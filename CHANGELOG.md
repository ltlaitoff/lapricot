# CHANGELOG

## v1.3.0 - 30.06

### User

- Changed website name from `Counter` to `Lapricot`
- Created new logotype
- Created custom color picker
- Created achiving categories
- Fixed display of the line for adding a new record to statistics on mobile devices

### Dev

- Migrated project to monorepo using nx
- Created libs for ui, client: data-access, store, and shared/types
- Removed Color collection from database and all colors on now saves in category.color in HEX format
- In categories add new field `achived`

## v1.2.0 - 11.06

### Backend

Millestone [v1.2.0](https://github.com/ltlaitoff/counter-backend/milestone/5)

- Create category for time [#63](https://github.com/ltlaitoff/counter-backend/issues/63)
- Created sessions [#67](https://github.com/ltlaitoff/counter-backend/pull/67)

### Frontend

Millestone [v1.2.0](https://github.com/ltlaitoff/counter-frontend/milestone/5)

- Create category for time [#96](https://github.com/ltlaitoff/counter-frontend/issues/96)
- Bug: Whet updating category color category-groups are reset [#113](https://github.com/ltlaitoff/counter-frontend/issues/113)
- Fix tab navigation in categories and statistic table [#105](https://github.com/ltlaitoff/counter-frontend/issues/105)
- Migrate to Angular 16 [#104](https://github.com/ltlaitoff/counter-frontend/issues/104)
- Create `Color` module and create `color-select` component [#101](https://github.com/ltlaitoff/counter-frontend/issues/101)
- Create statistic table controls [#117](https://github.com/ltlaitoff/counter-frontend/issues/117)
- Adaptive for all screens [#107](https://github.com/ltlaitoff/counter-frontend/issues/107)
- Create stubs for not-data: category-table, category-select, statistic-page... [#114](https://github.com/ltlaitoff/counter-frontend/issues/114)

## v1.1.0 - 06.05

### Backend

Millestone [v1.1.0](https://github.com/ltlaitoff/counter-backend/milestone/4)

- Fix change statistic bug [#62](https://github.com/ltlaitoff/counter-backend/issues/62)
- Create category-groups [#57](https://github.com/ltlaitoff/counter-backend/issues/57)
- Add dimension field to category [#52](https://github.com/ltlaitoff/counter-backend/issues/52)
- Remove populating data [#51](https://github.com/ltlaitoff/counter-backend/issues/51)
- Create categories reorder route [#31](https://github.com/ltlaitoff/counter-backend/issues/31)

### Frontend

Millestone [v1.1.0](https://github.com/ltlaitoff/counter-frontend/milestone/4)

- Change `exhaustMap` to `mergeMap` in all effects [#99](https://github.com/ltlaitoff/counter-frontend/issues/99)
- Bug: On add statistic item and go to statistic page, page show only 1 item [#94](https://github.com/ltlaitoff/counter-frontend/issues/94)
- On home page form create additional flag for show datetime input [#93](https://github.com/ltlaitoff/counter-frontend/issues/93)
- Fix reorder in category-select [#90](https://github.com/ltlaitoff/counter-frontend/issues/90)
- Fix change statistic bug [#87](https://github.com/ltlaitoff/counter-frontend/issues/87)
- Changelog: Add v1.0.1 and v1.0.2 to changelog [#86](https://github.com/ltlaitoff/counter-frontend/issues/86)
- Change footer [#84](https://github.com/ltlaitoff/counter-frontend/issues/84)
- Create category-groups [#75](https://github.com/ltlaitoff/counter-frontend/issues/75)
- Create checkbox for not-clean main form after submit [#74](https://github.com/ltlaitoff/counter-frontend/issues/74)
- Remove no-category from category-select [#73](https://github.com/ltlaitoff/counter-frontend/issues/73)
- Add dimension field to category [#64](https://github.com/ltlaitoff/counter-frontend/issues/64)
- Remove populating data from store [#62](https://github.com/ltlaitoff/counter-frontend/issues/62)
- Create categories reorder [#20](https://github.com/ltlaitoff/counter-frontend/issues/20)

## v1.0.2 - 07.04 - Hox fix

Changed in production api-link from dev version to prod
Pull request [#83](https://github.com/ltlaitoff/counter-frontend/pull/83)

## v1.0.1 - 03.04 - Hot fix

Removed 'no category' from category select
Pull request [#78](https://github.com/ltlaitoff/counter-frontend/pull/78)

## v1.0.0 - 03.04

### Backend

Millestone [v1.0.0](https://github.com/ltlaitoff/counter-backend/milestone/3)

- Create versioning by requests to api route [#55](https://github.com/ltlaitoff/counter-backend/issues/55)
- Create statistic edit data route [#34](https://github.com/ltlaitoff/counter-backend/issues/34)
- Create categories edit data route [#32](https://github.com/ltlaitoff/counter-backend/issues/32)
- Create logout route [#45](https://github.com/ltlaitoff/counter-backend/issues/45)
- Fix: Fix add statistic error. If body has \_id - error [#47](https://github.com/ltlaitoff/counter-backend/issues/47)
- Create route for initialize colors [#44](https://github.com/ltlaitoff/counter-backend/issues/44)
- Migrate to nest.js [#42](https://github.com/ltlaitoff/counter-backend/issues/42)

Millestone [v1.0.0](https://github.com/ltlaitoff/counter-frontend/milestone/3)

### Frontend

- Create versioning by requests to api [#69](https://github.com/ltlaitoff/counter-frontend/issues/69)
- Create pick category with find and scroll(ex Toggle) [#49](https://github.com/ltlaitoff/counter-frontend/issues/49)
- Create statistic edit data [#27](https://github.com/ltlaitoff/counter-frontend/issues/27)
- Fix add statistic record bugs [#65](https://github.com/ltlaitoff/counter-frontend/issues/65)
- Fix update-category-form bugs [#61](https://github.com/ltlaitoff/counter-frontend/issues/61)
- Create categories edit data [#21](https://github.com/ltlaitoff/counter-frontend/issues/21)
- Create not-sync icon in categories page [#40](https://github.com/ltlaitoff/counter-frontend/issues/40)
- Create buttons for force update categories and statistic [#48](https://github.com/ltlaitoff/counter-frontend/issues/48)
- Refactor and append functional header/user-panel [#56](https://github.com/ltlaitoff/counter-frontend/issues/56)
- Clean statistic and categories add form after submit [#51](https://github.com/ltlaitoff/counter-frontend/issues/51)
- Bug: On delete category and not delete statistic record -> error on frontend [#50](https://github.com/ltlaitoff/counter-frontend/issues/50)
- Fix bugs of adding statistic records [#41](https://github.com/ltlaitoff/counter-frontend/issues/41)
- Create usefull interface on home page for add statistic records [#42](https://github.com/ltlaitoff/counter-frontend/issues/42)
- Create logout button [#43](https://github.com/ltlaitoff/counter-frontend/issues/43)
- Create not-sync store statistic [#44](https://github.com/ltlaitoff/counter-frontend/issues/44)

## v0.0.2 - 12.03

### Backend

Millestone [v0.0.2](https://github.com/ltlaitoff/counter-backend/milestone/2)

- Create developing(local) database [#30](https://github.com/ltlaitoff/counter-backend/issues/30)
- Create routes for sync data with frontend [#33](https://github.com/ltlaitoff/counter-backend/issues/33)
- Create statistic delete item route [#35](https://github.com/ltlaitoff/counter-backend/issues/35)
- Fix on add statistic record only the first one is added [#36](https://github.com/ltlaitoff/counter-backend/issues/36)
- Fix allow empty comments for add categories and statistic [#37](https://github.com/ltlaitoff/counter-backend/issues/37)

### Frontend

Millestone [v0.0.2](https://github.com/ltlaitoff/counter-frontend/milestone/2)

- Create statistic beautiful table [#22](https://github.com/ltlaitoff/counter-frontend/issues/22)
- Pre-fetch [#23](https://github.com/ltlaitoff/counter-frontend/issues/23)
- Think and create new store for sync data with sync icons [#24](https://github.com/ltlaitoff/counter-frontend/issues/24)
- Fix footer version in production [#26](https://github.com/ltlaitoff/counter-frontend/issues/26)
- Create statistic delete item [#28](https://github.com/ltlaitoff/counter-frontend/issues/28)
- Fix on add statistic record only the first one is added [#32](https://github.com/ltlaitoff/counter-frontend/issues/32)
- Fix allow empty comments for add categories and statistic [#33](https://github.com/ltlaitoff/counter-frontend/issues/33)

## v0.0.1 - 05.03

### Backend

Millestone [v0.0.1](https://github.com/ltlaitoff/counter-backend/milestone/1)

- Fix production and develop https and cookie settings [#26](https://github.com/ltlaitoff/counter-backend/issues/26)

### Frontend

Millestone [v0.0.1](https://github.com/ltlaitoff/counter-frontend/milestone/1)

- Fix authorization [#1](https://github.com/ltlaitoff/counter-frontend/issues/1)
- Move API host variables to env [#2](https://github.com/ltlaitoff/counter-frontend/issues/2)
- Create gh-actions for deploy and check commits/pull requests [#3](https://github.com/ltlaitoff/counter-frontend/issues/3)
- Create global store NgRx [#5](https://github.com/ltlaitoff/counter-frontend/issues/5)
- Fix router in github.io [#6](https://github.com/ltlaitoff/counter-frontend/issues/6)
- Rework header [#9](https://github.com/ltlaitoff/counter-frontend/issues/9)
- Create footer [#10](https://github.com/ltlaitoff/counter-frontend/issues/10)
- Create temp certs for developing [#11](https://github.com/ltlaitoff/counter-frontend/issues/11)
- Fix statistic graph load [#14](https://github.com/ltlaitoff/counter-frontend/issues/14)

## v0.0.0 - 27.02

### Backend

Created working api

- Authorization with google identity
- All requests using identity user by cookie
- Initialize request for check is user authorized
- Categories and Statistic requests for basic application functional

### Frontend

- Created authorization with google identity
- Created add statistic record form
- Created categories table with add and remove opportunity
- Created statistic chart
- Created basic statistic table
