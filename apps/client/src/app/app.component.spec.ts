import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AppComponent } from './app.component'

/*
	App component should be

	In app la-header with userInfo prop = null on authGuard.authGuardData value = {authorized: false} should be
	In app la-header with userInfo prop = User... on authGuard.authGuardData value = {authorized: true, ...} should be

	In app main with router-outlet should be

	In app la-footer with version = environment.version and with type = environment.type should be

	In app console.info should called with output "Current version: ${environment.version}" on component init
	In app title of page should changes from "Lapricot" to "[D] Lapricot" on environment.type = "dev"
*/

describe('AppComponent', () => {
	it.todo('Create tests')
	// let component: AppComponent;
	// let fixture: ComponentFixture<AppComponent>;
	// beforeEach(async(() => {
	//     TestBed.configureTestingModule({
	//         declarations: [AppComponent]
	//     })
	//         .compileComponents();
	// }));
	// beforeEach(() => {
	//     fixture = TestBed.createComponent(AppComponent);
	//     component = fixture.componentInstance;
	//     fixture.detectChanges();
	// });
	// it('should create', () => {
	//     expect(component).toBeTruthy();
	// });
})
