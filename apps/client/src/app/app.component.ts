import { Component, OnInit } from '@angular/core'
import { environment } from '../environments/environment'
import { User } from '@lapricot/shared/types'
import { AuthGuardService } from '@lapricot/client-lib/auth'
import { Title } from '@angular/platform-browser'

import { HeaderComponent, FooterComponent } from '@lapricot/ui'
import { RouterOutlet } from '@angular/router'
import { CommonModule } from '@angular/common'

@Component({
	selector: 'la-root',
	standalone: true,
	imports: [CommonModule, RouterOutlet, HeaderComponent, FooterComponent],
	templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
	userData: User | null = null
	environment = environment

	ngOnInit() {
		console.info(`Current version: ${environment.version}`)

		this.authGuard.authGuardData.subscribe(newUserData => {
			if (newUserData.authorized) {
				this.userData = newUserData
				return
			}

			this.userData = null
		})
	}

	constructor(
		private authGuard: AuthGuardService,
		private titleService: Title
	) {
		if (environment.type === 'dev') {
			this.titleService.setTitle(`[D] ${this.titleService.getTitle()}`)
		}
	}
}
