import { Routes } from '@angular/router'

import {
	categoriesResolver,
	statisticResolver,
	categoryGroupsResolver,
	seanceResolver
} from '@lapricot/client-lib/resolvers'

export const APP_ROUTES: Routes = [
	{
		path: 'categories',
		loadComponent: () =>
			import('@lapricot/pages/categories').then(m => m.CategoriesPageComponent),
		resolve: [categoryGroupsResolver, categoriesResolver]
	},
	{
		path: 'trash',
		loadComponent: () =>
			import('@lapricot/pages/trash').then(m => m.TrashPageComponent),
		resolve: [categoriesResolver, statisticResolver]
	},
	{
		path: 'statistic',
		loadComponent: () =>
			import('@lapricot/pages/statistic').then(m => m.StatisticComponent),
		resolve: [categoryGroupsResolver, categoriesResolver, statisticResolver]
	},
	{
		path: 'statistic/report',
		loadComponent: () =>
			import('@lapricot/pages/report').then(m => m.ReportPageComponent),
		resolve: [categoriesResolver, statisticResolver]
	},
	{
		path: 'seance',
		loadComponent: () =>
			import('@lapricot/pages/seance').then(m => m.SeancePageComponent),
		resolve: [categoriesResolver, seanceResolver]
	},
	{
		path: 'seance/form',
		loadComponent: () =>
			import('@lapricot/pages/seance').then(m => m.SeanceFormPageComponent),
		resolve: [categoriesResolver, seanceResolver]
	},
	{
		path: 'sessions',
		loadComponent: () =>
			import('@lapricot/pages/sessions').then(m => m.SessionPageComponent)
	},
	{
		path: 'authorization',
		loadComponent: () =>
			import('@lapricot/pages/authorization').then(
				m => m.AuthorizationComponent
			)
	},
	{
		path: 'settings',
		loadComponent: () =>
			import('@lapricot/pages/settings').then(m => m.SettingsPageComponent)
	},
	{
		path: 'profile/:nickname',
		loadComponent: () =>
			import('@lapricot/pages/profile').then(m => m.ProfilePageComponent)
	},
	{
		path: '',
		loadComponent: () =>
			import('@lapricot/pages/home').then(m => m.HomePageComponent),
		resolve: [categoryGroupsResolver, categoriesResolver]
	},
	{
		path: '',
		redirectTo: '/',
		pathMatch: 'full'
	}
]
