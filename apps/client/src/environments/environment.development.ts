import { ClientEnvironment } from '@lapricot/shared/types'

export const environment: ClientEnvironment = {
	version: '1.4.0',
	type: 'dev',
	googleClientId:
		'446806438760-0h3fblc15rjdl05rqtui4ki0lkpqlr08.apps.googleusercontent.com',
	API_HOST: 'https://localhost:8000'
}
