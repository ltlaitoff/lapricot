import { ClientEnvironment } from '@lapricot/shared/types'

export const environment: ClientEnvironment = {
	version: '1.3.0',
	type: 'production',
	googleClientId:
		'446806438760-0h3fblc15rjdl05rqtui4ki0lkpqlr08.apps.googleusercontent.com',
	API_HOST: 'https://lapricot-api.vercel.app'
}
