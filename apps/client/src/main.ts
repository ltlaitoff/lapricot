import { importProvidersFrom, isDevMode } from '@angular/core'
import { bootstrapApplication } from '@angular/platform-browser'
import { StoreReducers, StoreEffects } from '@lapricot/client-lib/store'
import { EffectsModule } from '@ngrx/effects'
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { AppComponent } from './app/app.component'
import { localStorageSync } from 'ngrx-store-localstorage'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { ServiceWorkerModule } from '@angular/service-worker'
import { LottieModule } from 'ngx-lottie'
import { HttpClientModule } from '@angular/common/http'
import {
	GoogleLoginProvider,
	SocialAuthServiceConfig
} from '@abacritt/angularx-social-login'
import { APP_CONFIG } from '@lapricot/client-lib/config'
import { environment } from './environments/environment'
import { provideRouter, RouterModule, withDebugTracing } from '@angular/router'
import { APP_ROUTES } from './app/app.routes'
import { provideAnimations } from '@angular/platform-browser/animations'

export function localStorageSyncReducer(
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	reducer: ActionReducer<any>
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
): ActionReducer<any> {
	return localStorageSync({
		keys: [
			'categories',
			'statistic',
			'seance',
			'notSyncCategories',
			'notSyncStatistic',
			'notSyncCategoryGroups',
			'notSyncSeance',
			'syncCategoryGroups',
			'seanceActive'
		],
		rehydrate: true
	})(reducer)
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer]

// Factory funtion needed ngx-lottie
export function playerFactory() {
	return import(/* webpackChunkName: 'lottie-web' */ 'lottie-web')
}

bootstrapApplication(AppComponent, {
	providers: [
		provideAnimations(),
		importProvidersFrom(
			AngularSvgIconModule.forRoot(),
			StoreModule.forRoot(StoreReducers, { metaReducers }),
			EffectsModule.forRoot(StoreEffects),
			StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
			ServiceWorkerModule.register('ngsw-worker.js', {
				enabled: !isDevMode(),
				// Register the ServiceWorker as soon as the application is stable
				// or after 30 seconds (whichever comes first).
				registrationStrategy: 'registerWhenStable:30000'
			}),
			LottieModule.forRoot({ player: playerFactory })
		),
		importProvidersFrom(HttpClientModule),
		provideRouter(APP_ROUTES, withDebugTracing()),
		{ provide: APP_CONFIG, useValue: environment },
		{
			provide: 'SocialAuthServiceConfig',
			useValue: {
				autoLogin: false,
				providers: [
					{
						id: GoogleLoginProvider.PROVIDER_ID,
						provider: new GoogleLoginProvider(environment.googleClientId, {
							oneTapEnabled: false
						})
					}
				],
				onError: err => {
					console.error(err)
				}
			} as SocialAuthServiceConfig
		}
	]
}).catch(err => console.error(err))
