const { createGlobPatternsForDependencies } = require('@nx/angular/tailwind')
const { join } = require('path')

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		join(__dirname, 'src/**/!(*.stories|*.spec).{ts,html}'),
		join(__dirname, '../../libs/**/!(*.stories|*.spec).{ts,html}'),
		...createGlobPatternsForDependencies(__dirname)
	],
	theme: {
		extend: {
			animation: {
				fade: 'fadeOut 0.1s ease-in'
			},

			keyframes: theme => ({
				fadeOut: {
					'0%': { backgroundColor: 'transparent' },
					'100%': { backgroundColor: theme('colors.slate.600') }
				}
			})
		}
	},
	plugins: []
}
