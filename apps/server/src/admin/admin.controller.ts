import { Controller, HttpStatus, Post, Res, Headers } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import type { Response } from 'express'
import { CategoryService } from '../category/category.service'
import { ApiTags } from '@nestjs/swagger'
import { AdminService } from './admin.service'
import { StatisticService } from '../statistic/statistic.service'
import { UserService } from '../user/user.service'

@Controller('admin')
export class AdminController {
	constructor(
		private adminService: AdminService,
		private configService: ConfigService,
		private categoryService: CategoryService,
		private statisticSevice: StatisticService,
		private userService: UserService
	) {}

	@ApiTags('Admin')
	@Post('updatestatisticfields')
	async updateStatisticFields(
		@Headers('authorization') authorization: undefined | string,
		@Res() res: Response
	) {
		if (!authorization) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'UNAUTHORIZED' })
			return
		}

		const INITIALIZE_LOGIN = this.configService.get<string>('INITIALIZE_LOGIN')
		const INITIALIZE_PASSWORD = this.configService.get<string>(
			'INITIALIZE_PASSWORD'
		)

		if (authorization !== `${INITIALIZE_LOGIN}:${INITIALIZE_PASSWORD}`) {
			res
				.status(HttpStatus.UNAUTHORIZED)
				.json({ message: 'The login or password is incorrect' })
			return
		}

		return res
			.status(HttpStatus.OK)
			.json(await this.statisticSevice.__updateStatisticFields())
	}

	@ApiTags('Admin')
	@Post('updateCategoryFields')
	async updateCategoryFields(
		@Headers('authorization') authorization: undefined | string,
		@Res() res: Response
	) {
		if (!authorization) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'UNAUTHORIZED' })
			return
		}

		const INITIALIZE_LOGIN = this.configService.get<string>('INITIALIZE_LOGIN')
		const INITIALIZE_PASSWORD = this.configService.get<string>(
			'INITIALIZE_PASSWORD'
		)

		if (authorization !== `${INITIALIZE_LOGIN}:${INITIALIZE_PASSWORD}`) {
			res
				.status(HttpStatus.UNAUTHORIZED)
				.json({ message: 'The login or password is incorrect' })
			return
		}

		return res
			.status(HttpStatus.OK)
			.json(await this.categoryService.updateCategoryFields())
	}

	@ApiTags('Admin')
	@Post('removeUnusedStatisticFields')
	async removeUnusedStatisticFields(
		@Headers('authorization') authorization: undefined | string,
		@Res() res: Response
	) {
		if (!authorization) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'UNAUTHORIZED' })
			return
		}

		const INITIALIZE_LOGIN = this.configService.get<string>('INITIALIZE_LOGIN')
		const INITIALIZE_PASSWORD = this.configService.get<string>(
			'INITIALIZE_PASSWORD'
		)

		if (authorization !== `${INITIALIZE_LOGIN}:${INITIALIZE_PASSWORD}`) {
			res
				.status(HttpStatus.UNAUTHORIZED)
				.json({ message: 'The login or password is incorrect' })
			return
		}

		return res
			.status(HttpStatus.OK)
			.json(await this.adminService.removeUnusedStatisticFields())
	}

	@ApiTags('Admin')
	@Post('usersNickname')
	async usersNickname(
		@Headers('authorization') authorization: undefined | string,
		@Res() res: Response
	) {
		if (!authorization) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'UNAUTHORIZED' })
			return
		}

		const INITIALIZE_LOGIN = this.configService.get<string>('INITIALIZE_LOGIN')
		const INITIALIZE_PASSWORD = this.configService.get<string>(
			'INITIALIZE_PASSWORD'
		)

		if (authorization !== `${INITIALIZE_LOGIN}:${INITIALIZE_PASSWORD}`) {
			res
				.status(HttpStatus.UNAUTHORIZED)
				.json({ message: 'The login or password is incorrect' })
			return
		}

		return res
			.status(HttpStatus.OK)
			.json(await this.userService.__addToAllUsersNickname())
	}
}
