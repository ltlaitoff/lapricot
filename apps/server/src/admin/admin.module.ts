import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { StatisticModule } from '../statistic/statistic.module'
import { CategoryGroupModule } from '../category-group/category-group.module'
import { CategoryModule } from '../category/category.module'
import { AdminController } from './admin.controller'
import { AdminService } from './admin.service'
import { UserModule } from '../user/user.module'

@Module({
	imports: [
		ConfigModule,
		CategoryModule,
		CategoryGroupModule,
		StatisticModule,
		UserModule
	],
	controllers: [AdminController],
	providers: [AdminService]
})
export class AdminModule {}
