import { Injectable } from '@nestjs/common'
import { StatisticService } from '../statistic/statistic.service'
// import { CategoryGroupService } from '../category-group/category-group.service'
import { CategoryService } from '../category/category.service'
import { ObjectId } from 'mongoose'

@Injectable()
export class AdminService {
	constructor(
		private categoryService: CategoryService,
		private statisticService: StatisticService
	) {}

	async removeUnusedStatisticFields() {
		const allCategories = await this.categoryService.__getAllCategories()
		const allStatistic = await this.statisticService.__getAllStatistic()

		const allCategoriesIds = allCategories.map(
			// @ts-expect-error ADMIN
			item => item._id.toString()
		)

		console.log(`StatisticID | CategoryId | UserId | Count | Comment | Date`)
		return allStatistic
			.map(statistic => {
				if (
					!allCategoriesIds.includes(
						(statistic.category as ObjectId).toString()
					)
				) {
					console.log(
						// @ts-expect-error ADMIN
						`${statistic._id} | ${statistic.category} | ${statistic.user} | `,
						`${statistic.count} | ${
							statistic.comment
						} | ${statistic.date.toString()}`
					)

					// @ts-expect-error ADMIN
					return this.statisticService.delete(statistic._id, statistic.user)
				}

				return null
			})
			.filter(item => item !== null)
	}
}
