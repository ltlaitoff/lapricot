import { Injectable, Session } from '@nestjs/common'
import type { Request } from 'express'
import { JwtPayloadUser } from './auth.interface'
import { UserService } from '../user/user.service'
import { SessionData } from 'express-session'
import { AuthDataDto } from './dto/auth-data.dto'
import { User } from '@lapricot/shared/types'

@Injectable()
export class AuthService {
	constructor(private userService: UserService) {}

	async signIn(
		body: AuthDataDto,
		@Session() session: SessionData,
		user: JwtPayloadUser,
		req: Request
	): Promise<User> {
		const userDocument = await this.userService.findOrCreate(user)

		session.auth = {
			authorized: true,
			userId: userDocument._id,
			browserName: body.browserName,
			browserVersion: body.browserVersion,
			osName: body.osName,
			osVersion: body.osVersion,
			osVersionName: body.osVersionName,
			userAgent: body.userAgent,
			platformType: body.platformType,
			dateOfCreate: body.dateOfCreate
		}

		return {
			...userDocument,
			_id: userDocument._id.toString(),
			sessionId: req.sessionID,
			authorized: true
		}
	}
}
