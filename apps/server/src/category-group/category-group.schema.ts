import { CategoryGroup } from '@lapricot/shared/types'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import mongoose, { ObjectId } from 'mongoose'
import { User } from '../user/user.schema'

interface CategoriesGroupSchemaType
	extends Omit<CategoryGroup, '_id' | 'color'> {
	color: string
}

@Schema()
export class Group implements CategoriesGroupSchemaType {
	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		ref: User.name,
		required: true
	})
	user: ObjectId

	@Prop({ required: true })
	name: string

	@Prop({ required: true })
	color: string

	@Prop({ required: true })
	order: number
}

export const GroupSchema = SchemaFactory.createForClass(Group)
