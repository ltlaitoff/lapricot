import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateCategoryGroupData } from '@lapricot/shared/types'

export class CreateCategoryGroupDto implements CreateCategoryGroupData {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	name: string

	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	color: string
}
