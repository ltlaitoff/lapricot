import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString, IsNumber } from 'class-validator'

import { ReorderCategoryGroupData } from '@lapricot/shared/types'

export class ReorderCategoryGroupDto implements ReorderCategoryGroupData {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	categoryGroupId: string

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	previousIndex: number

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	currentIndex: number
}
