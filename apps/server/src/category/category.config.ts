import { CreateCategoryDto } from './dto/create-category.dto'

export const DEFAULT_CATEGORIES: CreateCategoryDto[] = [
	{
		name: 'Pull-ups',
		comment: 'Pull ups count',
		color: '#ef4444',
		group: []
	},
	{
		name: 'Push-ups',
		comment: 'Push ups count',
		color: '#10b981',
		group: []
	},
	{
		name: 'Callories',
		comment: 'Callories',
		color: '#3b82f6',
		dimension: 'kkal',
		group: []
	}
]

export const PROJECTIONS = `-__v -createdAt -updatedAt`
