import { Module } from '@nestjs/common'
import { CategoryController } from './category.controller'
import { CategoryService } from './category.service'
import { MongooseModule } from '@nestjs/mongoose'
import { Category, CategorySchema } from './category.schema'
import { StatisticModule } from '../statistic/statistic.module'

@Module({
	imports: [
		MongooseModule.forFeature([
			{
				name: Category.name,
				schema: CategorySchema
			}
		]),
		StatisticModule
	],
	controllers: [CategoryController],
	providers: [CategoryService],
	exports: [CategoryService]
})
export class CategoryModule {}
