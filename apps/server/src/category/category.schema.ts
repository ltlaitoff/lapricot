import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import mongoose, { ObjectId } from 'mongoose'

import { User } from '../user/user.schema'
import { Group } from '../category-group/category-group.schema'
import { Category as CategoryMain } from '@lapricot/shared/types'

interface CategorySchemaType extends Omit<CategoryMain, '_id' | 'group'> {
	group: ObjectId[]
}

@Schema()
export class Category implements CategorySchemaType {
	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		ref: User.name,
		required: true
	})
	user: ObjectId

	@Prop({ required: true, default: 'number' })
	mode: 'number' | 'time'

	@Prop({ required: true })
	name: string

	@Prop({ default: '' })
	comment: string

	@Prop({ required: true })
	color: string

	@Prop({ required: true })
	order: number

	@Prop()
	dimension?: string

	@Prop({ required: true })
	archived: boolean

	@Prop({
		type: [mongoose.Schema.Types.ObjectId],
		ref: Group.name,
		required: true,
		default: []
	})
	group: ObjectId[]

	@Prop({ required: true })
	trash: boolean

	@Prop({ required: true, default: -1 })
	trash_expires: number
}

export const CategorySchema = SchemaFactory.createForClass(Category)
