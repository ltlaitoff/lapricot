import { Body, Injectable } from '@nestjs/common'
import { Category } from './category.schema'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { ICategory } from './category.interface'
import { CreateCategoryDto } from './dto/create-category.dto'
import { DEFAULT_CATEGORIES, PROJECTIONS } from './category.config'
import { UserIdSession } from '../app.interfaces'
import { UpdateCategoryDto } from './dto/update-category.dto'
import { ReorderCategoryDto } from './dto/reorder-category.dto'
import { TRASH_MAX_AGE } from '../config/constants.config'
import { StatisticService } from '../statistic/statistic.service'

@Injectable()
export class CategoryService {
	constructor(
		@InjectModel(Category.name) private categoryModel: Model<ICategory>,
		private statisticService: StatisticService
	) {}

	async add(
		@Body() createCategoryDto: CreateCategoryDto,

		userId: UserIdSession
	) {
		const lastOrderId = await this.getLastOrder(userId)

		const dataForAdd = {
			user: userId,
			name: createCategoryDto.name,
			mode: createCategoryDto.mode || 'number',
			color: createCategoryDto.color,
			comment: createCategoryDto.comment,
			dimension: createCategoryDto.dimension,
			order: lastOrderId === null ? 0 : lastOrderId + 1,
			archived: false,
			trash: false,
			trash_expires: -1
		}

		const newCategory = await new this.categoryModel(dataForAdd)

		const newCategoryDocument = await newCategory.save()

		return newCategoryDocument
	}

	async getAll(userId: UserIdSession): Promise<Category[]> {
		const userAllCategories = this.categoryModel.find(
			{ user: userId },
			PROJECTIONS
		)

		return await userAllCategories.lean()
	}

	async delete(id: string, userId: UserIdSession): Promise<unknown> {
		const result = await this.categoryModel.deleteOne({
			_id: id,
			user: userId
		})

		const deletedStatistic =
			await this.statisticService.deleteAllRecordsByCategory(id, userId)

		return [result, deletedStatistic]
	}

	async edit(id: string, body: UpdateCategoryDto, userId: UserIdSession) {
		const updatedCategory = await this.categoryModel.findByIdAndUpdate(
			{ _id: id, user: userId },
			body,
			{ new: true }
		)

		if (!updatedCategory) {
			return null
		}

		const updatedCategoryDocument = await updatedCategory.save()

		return updatedCategoryDocument
	}

	private async getLastOrder(userId: UserIdSession) {
		const allUserCategories = await this.getAll(userId)

		const result = allUserCategories.reduce(
			(prev, category) => (category.order > prev ? category.order : prev),
			-1
		)

		return result === -1 ? null : result
	}

	async intializeUserDefaultCategories(userId: UserIdSession) {
		return DEFAULT_CATEGORIES.map(defaultCategory => {
			return this.add(defaultCategory, userId)
		})
	}

	/* archive */
	async archive(id: string, userId: UserIdSession): Promise<Category | null> {
		const category = await this.categoryModel.findOne({
			_id: id,
			user: userId
		})

		if (category === null) return null

		category.archived = !category.archived

		return category.save()
	}

	/* trash */
	async moveToTrash(
		id: string,
		userId: UserIdSession
	): Promise<Category | null> {
		const category = await this.categoryModel.findOne({
			_id: id,
			user: userId
		})

		if (category === null) return null
		if (category.trash) return category

		const trash_expires = new Date(Date.now()).getTime() + TRASH_MAX_AGE

		category.trash = true
		category.trash_expires = trash_expires

		this.statisticService.moveAllStatisticByCategoryToTrash(
			category._id.toString(),
			userId,
			trash_expires
		)

		return category.save()
	}

	async moveFromTrash(
		id: string,
		userId: UserIdSession
	): Promise<Category | null> {
		const category = await this.categoryModel.findOne({
			_id: id,
			user: userId
		})

		if (category === null) return null
		if (category.trash === false) return category

		const timestamp = category.trash_expires

		category.trash = false
		category.trash_expires = -1

		this.statisticService.moveAllStatisticByCategoryFromTrash(
			category._id.toString(),
			userId,
			timestamp
		)

		return category.save()
	}

	/* reorder */
	async reorder(
		body: ReorderCategoryDto,
		userId: UserIdSession
	): Promise<ReorderCategoryDto[] | null> {
		if (body.currentIndex === body.previousIndex) return null

		const categoriesForUpdateOrder = await this.getCategoriesByOrderInRange(
			userId,
			body.previousIndex,
			body.currentIndex
		)

		const updatedCategoriesForUpdateOrder = await this.changeCategoriesOrders(
			categoriesForUpdateOrder,
			body.previousIndex
		)

		const result: ReorderCategoryDto[] = await this.transformToReorderResult(
			updatedCategoriesForUpdateOrder
		)

		await this.updateOrders(result, userId)

		return result
	}

	private async getCategoriesByOrderInRange(
		userId: UserIdSession,
		previousIndex: number,
		currentIndex: number
	): Promise<ICategory[]> {
		const order =
			previousIndex < currentIndex
				? { $gte: previousIndex, $lte: currentIndex }
				: { $gte: currentIndex, $lte: previousIndex }

		const sort = {
			order: previousIndex < currentIndex ? 1 : -1
		}

		return await this.categoryModel
			.find(
				{
					order: order,
					user: userId
				},
				{},
				{
					sort: sort
				}
			)
			.lean()
	}

	private async changeCategoriesOrders(
		categories: (ICategory & { previousIndex?: number })[],
		previousIndex: number
	) {
		const lastIndex = categories.at(-1)?.order

		if (lastIndex === undefined) {
			throw new Error('ERR')
		}

		for (let i = categories.length - 2; i >= 0; i--) {
			const currentElement = categories.at(i)

			const indexNextElement = i + 1 > categories.length - 1 ? 0 : i + 1
			const nextElement = categories.at(indexNextElement)

			if (currentElement === undefined || nextElement === undefined) {
				throw new Error(
					`currentElement || nextElement is undefined in reorder. categories = ${JSON.stringify(
						categories
					)}`
				)
			}

			nextElement.previousIndex = nextElement.order
			nextElement.order = currentElement.order
		}

		categories[0].previousIndex = previousIndex
		categories[0].order = lastIndex

		return categories
	}

	private async transformToReorderResult(
		categories: (ICategory & { previousIndex?: number })[]
	): Promise<ReorderCategoryDto[]> {
		return categories.map(item => {
			if (item.previousIndex == undefined) {
				throw new Error(
					`item.previousIndex is undefined in transformToReorderResult. categories = ${JSON.stringify(
						categories
					)}`
				)
			}

			return {
				categoryId: item._id.toString(),
				currentIndex: item.order,
				previousIndex: item.previousIndex
			}
		})
	}

	private async updateOrders(
		categories: ReorderCategoryDto[],
		userId: UserIdSession
	) {
		await Promise.all(
			categories.map(async item => {
				const updatedCategory = await this.categoryModel
					.findByIdAndUpdate(
						{ _id: item.categoryId, user: userId },
						{
							order: item.currentIndex
						},
						{ new: true }
					)
					.lean()

				if (!updatedCategory) {
					throw new Error(
						`Error update categories reorder. Category = ${JSON.stringify(
							item
						)}`
					)
				}

				return updatedCategory
			})
		)
	}

	/* reorder end */
	/* Admins for migration */
	async updateCategoryFields(): Promise<unknown> {
		const result: unknown[] = []

		/* Group */
		result.push({
			group: await this.categoryModel.updateMany(
				{ group: { $exists: false } },
				{ $set: { group: [] } }
			)
		})

		/* dimension */
		result.push({
			dimension: await this.categoryModel.updateMany(
				{ dimension: { $exists: false } },
				{ $set: { dimension: '' } }
			)
		})

		/* mode */
		result.push({
			dimension: await this.categoryModel.updateMany(
				{ mode: { $exists: false } },
				{ $set: { mode: 'number' } }
			)
		})

		/* archived */
		result.push({
			dimension: await this.categoryModel.updateMany(
				{ archived: { $exists: false } },
				{ $set: { archived: false } }
			)
		})

		/* trash */
		result.push({
			trash: await this.categoryModel.updateMany(
				{ trash: { $exists: false } },
				{ $set: { trash: false } }
			)
		})

		/* trash_expires */
		result.push({
			trash_expires: await this.categoryModel.updateMany(
				{ trash_expires: { $exists: false } },
				{ $set: { trash_expires: -1 } }
			)
		})

		return result
	}

	async __getAllCategories(): Promise<Category[]> {
		const allCategories = this.categoryModel.find()

		return await allCategories.lean()
	}
}
