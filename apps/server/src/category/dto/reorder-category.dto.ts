import { ReorderCategoryData } from '@lapricot/shared/types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString, IsNumber } from 'class-validator'

export class ReorderCategoryDto implements ReorderCategoryData {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	categoryId: string

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	previousIndex: number

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	currentIndex: number
}
