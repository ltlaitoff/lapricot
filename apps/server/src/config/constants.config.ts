// 2 years in miliseconds
export const COOKIE_MAX_AGE = 2 * 360 * 24 * 60 * 60 * 1000
// 30 days in miliseconds
export const TRASH_MAX_AGE = 30 * 24 * 60 * 60 * 1000
