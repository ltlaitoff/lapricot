import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty, IsString } from 'class-validator'
import { CreateSeance, CreateSeanceRecord } from '@lapricot/shared/types'

export class CreateSeanceDto implements CreateSeance {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	name: string

	@ApiProperty()
	@IsString()
	comment: string

	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	color: string

	@ApiProperty()
	@IsArray()
	records: CreateSeanceRecord[]
}
