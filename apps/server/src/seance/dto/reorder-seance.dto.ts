import { ReorderSeanceData } from '@lapricot/shared/types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString, IsNumber } from 'class-validator'

export class ReorderSeanceDto implements ReorderSeanceData {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	id: string

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	previousIndex: number

	@ApiProperty()
	@IsNotEmpty()
	@IsNumber()
	currentIndex: number
}
