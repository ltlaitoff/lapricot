import { SeanceRecord, UpdateSeance } from '@lapricot/shared/types'
import { ApiProperty, PartialType } from '@nestjs/swagger'
import { IsArray, IsBoolean, IsNumber } from 'class-validator'
import { CreateSeanceDto } from './create-seance.dto'

export class UpdateSeanceDto
	extends PartialType(CreateSeanceDto)
	implements UpdateSeance
{
	@ApiProperty()
	@IsBoolean()
	trash: boolean

	@ApiProperty()
	@IsNumber()
	trash_expires: number

	@ApiProperty()
	@IsBoolean()
	archived: boolean

	@ApiProperty()
	@IsArray()
	records: SeanceRecord[]
}
