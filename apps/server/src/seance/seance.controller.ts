import {
	Body,
	Controller,
	Delete,
	Get,
	HttpStatus,
	Param,
	Post,
	Put,
	Res,
	Session
} from '@nestjs/common'
import type { Response } from 'express'
import { ApiTags, ApiResponse } from '@nestjs/swagger'
import { SessionData } from 'express-session'
import { SeanceService } from './seance.service'
import { CreateSeanceDto } from './dto/create-seance.dto'
import { UpdateSeanceDto } from './dto/update-seance.dto'
import { ReorderSeanceDto } from './dto/reorder-seance.dto'

@Controller('seance')
export class SeanceController {
	constructor(private seanceService: SeanceService) {}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({
		status: 200,
		description: 'Get all user seances'
	})
	@Get('all')
	async getAll(@Session() session: SessionData, @Res() res: Response) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.seanceService.getAll(session.auth.userId))
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Get all user seances' })
	@Post('add')
	async addNew(
		@Session() session: SessionData,
		@Res() res: Response,
		@Body() body: CreateSeanceDto
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.seanceService.add(body, session.auth.userId))
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Category has beed deleted' })
	@Delete(':id')
	async delete(
		@Param('id') id: string,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.seanceService.moveToTrash(id, session.auth.userId))
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Category has beed deleted' })
	@Delete(':id/permanent')
	async deletePermanent(
		@Param('id') id: string,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.seanceService.delete(id, session.auth.userId))
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Category has beed deleted' })
	@Post(':id/restore')
	async restoreCategory(
		@Param('id') id: string,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.seanceService.moveFromTrash(id, session.auth.userId))
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Categories has beed reorder' })
	@Put('reorder')
	async reorder(
		@Body() body: ReorderSeanceDto,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		const result = await this.seanceService.reorder(body, session.auth.userId)

		if (result === null) {
			return res
				.status(HttpStatus.BAD_REQUEST)
				.json({ message: 'Something gonna wrong' })
		}

		res.status(HttpStatus.OK).json(result)
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({ status: 200, description: 'Category has beed changes' })
	@Put(':id')
	async put(
		@Param('id') id: string,
		@Body() body: UpdateSeanceDto,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		const result = await this.seanceService.edit(id, body, session.auth.userId)

		if (result === null) {
			return res
				.status(HttpStatus.BAD_REQUEST)
				.json({ message: 'Something gonna wrong' })
		}

		res.status(HttpStatus.OK).json(result)
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({
		status: 200,
		description: 'The archive field in the seance was switched'
	})
	@Put('archive/:id')
	async archive(
		@Param('id') id: string,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		const result = await this.seanceService.archive(id, session.auth.userId)

		if (result === null) {
			return res
				.status(HttpStatus.BAD_REQUEST)
				.json({ message: 'Something gonna wrong' })
		}

		res.status(HttpStatus.OK).json(result)
	}

	@ApiTags('Seance')
	@ApiResponse({ status: 401, description: 'Unauthorized' })
	@ApiResponse({
		status: 200,
		description: 'Categot'
	})
	@Put('fromtrash/:id')
	async trash(
		@Param('id') id: string,
		@Session() session: SessionData,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		const result = await this.seanceService.moveFromTrash(
			id,
			session.auth.userId
		)

		if (result === null) {
			return res
				.status(HttpStatus.BAD_REQUEST)
				.json({ message: 'Something gonna wrong' })
		}

		res.status(HttpStatus.OK).json(result)
	}
}
