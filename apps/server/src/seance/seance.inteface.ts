import { HydratedDocument } from 'mongoose'
import { Seance } from './seance.schema'

export type ISeance = HydratedDocument<Seance>
