import { Module } from '@nestjs/common'
import { SeanceService } from './seance.service'
import { SeanceController } from './seance.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { Seance, SeanceSchema } from './seance.schema'

@Module({
	imports: [
		MongooseModule.forFeature([
			{
				name: Seance.name,
				schema: SeanceSchema
			}
		])
	],
	providers: [SeanceService],
	controllers: [SeanceController]
})
export class SeanceModule {}
