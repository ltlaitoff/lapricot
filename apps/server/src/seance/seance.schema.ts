import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import mongoose, { ObjectId } from 'mongoose'

import { User } from '../user/user.schema'
import { Seance as SeanceMain, SeanceRecord } from '@lapricot/shared/types'

type SeanceSchemaType = Omit<SeanceMain, '_id'>

@Schema()
export class Seance implements SeanceSchemaType {
	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		ref: User.name,
		required: true
	})
	user: ObjectId

	@Prop({ required: true })
	name: string

	@Prop({ default: '' })
	comment: string

	@Prop({ required: true })
	color: string

	@Prop({ required: true })
	order: number

	@Prop({ required: true })
	records: Array<SeanceRecord>

	@Prop({ required: true })
	archived: boolean

	@Prop({ required: true })
	trash: boolean

	@Prop({ required: true, default: -1 })
	trash_expires: number
}

export const SeanceSchema = SchemaFactory.createForClass(Seance)
