import { SeanceRecord } from '@lapricot/shared/types'
import { Body, Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { UserIdSession } from '../app.interfaces'
import { TRASH_MAX_AGE } from '../config/constants.config'
import { CreateSeanceDto } from './dto/create-seance.dto'
import { ReorderSeanceDto } from './dto/reorder-seance.dto'
import { UpdateSeanceDto } from './dto/update-seance.dto'
import { PROJECTIONS } from './seance.config'
import { ISeance } from './seance.inteface'
import { Seance } from './seance.schema'

@Injectable()
export class SeanceService {
	constructor(
		@InjectModel(Seance.name) private seanceModel: Model<ISeance> // private statisticService: StatisticService
	) {}

	async add(@Body() createSeanceDto: CreateSeanceDto, userId: UserIdSession) {
		const lastOrderId = await this.getLastOrder(userId)

		const records: SeanceRecord[] = createSeanceDto.records.map(item => ({
			...item,
			trash: false,
			trash_expires: -1
		}))

		const dataForAdd: Seance = {
			// @ts-expect-error Inner database field
			user: userId,
			name: createSeanceDto.name,
			color: createSeanceDto.color,
			comment: createSeanceDto.comment,
			records: records,
			order: lastOrderId === null ? 0 : lastOrderId + 1,
			archived: false,
			trash: false,
			trash_expires: -1
		}

		const newSeance = await new this.seanceModel(dataForAdd)

		const newSeanceDocument = await newSeance.save()

		return newSeanceDocument
	}

	async getAll(userId: UserIdSession): Promise<Seance[]> {
		const userAllCategories = this.seanceModel.find(
			{ user: userId },
			PROJECTIONS
		)

		return await userAllCategories.lean()
	}

	async delete(id: string, userId: UserIdSession): Promise<unknown> {
		return await this.seanceModel.deleteOne({
			_id: id,
			user: userId
		})
	}

	async edit(id: string, body: UpdateSeanceDto, userId: UserIdSession) {
		const updated = await this.seanceModel.findByIdAndUpdate(
			{ _id: id, user: userId },
			body,
			{ new: true }
		)

		if (!updated) return null

		return await updated.save()
	}

	private async getLastOrder(userId: UserIdSession) {
		const records = await this.getAll(userId)

		const lastOrder = records.reduce(
			(prev, item) => (item.order > prev ? item.order : prev),
			-1
		)

		return lastOrder === -1 ? null : lastOrder
	}

	/* archive */
	async archive(id: string, userId: UserIdSession): Promise<Seance | null> {
		const document = await this.seanceModel.findOne({
			_id: id,
			user: userId
		})

		if (document === null) return null

		document.archived = !document.archived

		return await document.save()
	}

	/* trash */
	async moveToTrash(id: string, userId: UserIdSession): Promise<Seance | null> {
		const document = await this.seanceModel.findOne({
			_id: id,
			user: userId
		})

		if (document === null) return null
		if (document.trash) return document

		const trash_expires = new Date(Date.now()).getTime() + TRASH_MAX_AGE

		document.trash = true
		document.trash_expires = trash_expires

		return await document.save()
	}

	async moveFromTrash(
		id: string,
		userId: UserIdSession
	): Promise<Seance | null> {
		const document = await this.seanceModel.findOne({
			_id: id,
			user: userId
		})

		if (document === null) return null
		if (document.trash === false) return document

		document.trash = false
		document.trash_expires = -1

		return await document.save()
	}

	/* reorder */
	async reorder(
		body: ReorderSeanceDto,
		userId: UserIdSession
	): Promise<ReorderSeanceDto[] | null> {
		if (body.currentIndex === body.previousIndex) return null

		const forUpdateOrder = await this.getByOrderInRange(
			userId,
			body.previousIndex,
			body.currentIndex
		)

		const updatedForUpdateOrder = await this.changeOrders(
			forUpdateOrder,
			body.previousIndex
		)

		const result: ReorderSeanceDto[] = await this.transformToReorderResult(
			updatedForUpdateOrder
		)

		await this.updateOrders(result, userId)

		return result
	}

	private async getByOrderInRange(
		userId: UserIdSession,
		previousIndex: number,
		currentIndex: number
	): Promise<ISeance[]> {
		const order =
			previousIndex < currentIndex
				? { $gte: previousIndex, $lte: currentIndex }
				: { $gte: currentIndex, $lte: previousIndex }

		const sort = {
			order: previousIndex < currentIndex ? 1 : -1
		}

		return await this.seanceModel
			.find(
				{
					order: order,
					user: userId
				},
				{},
				{
					sort: sort
				}
			)
			.lean()
	}

	private async changeOrders(
		data: (ISeance & { previousIndex?: number })[],
		previousIndex: number
	) {
		const lastIndex = data.at(-1)?.order

		if (lastIndex === undefined) {
			throw new Error('ERR')
		}

		for (let i = data.length - 2; i >= 0; i--) {
			const currentElement = data.at(i)

			const indexNextElement = i + 1 > data.length - 1 ? 0 : i + 1
			const nextElement = data.at(indexNextElement)

			if (currentElement === undefined || nextElement === undefined) {
				throw new Error(
					`currentElement || nextElement is undefined in reorder. data = ${JSON.stringify(
						data
					)}`
				)
			}

			nextElement.previousIndex = nextElement.order
			nextElement.order = currentElement.order
		}

		data[0].previousIndex = previousIndex
		data[0].order = lastIndex

		return data
	}

	private async transformToReorderResult(
		data: (ISeance & { previousIndex?: number })[]
	): Promise<ReorderSeanceDto[]> {
		return data.map(item => {
			if (item.previousIndex == undefined) {
				throw new Error(
					`item.previousIndex is undefined in transformToReorderResult. data = ${JSON.stringify(
						data
					)}`
				)
			}

			return {
				id: item._id.toString(),
				currentIndex: item.order,
				previousIndex: item.previousIndex
			}
		})
	}

	private async updateOrders(data: ReorderSeanceDto[], userId: UserIdSession) {
		await Promise.all(
			data.map(async item => {
				const updatedCategory = await this.seanceModel
					.findByIdAndUpdate(
						{ _id: item.id, user: userId },
						{
							order: item.currentIndex
						},
						{ new: true }
					)
					.lean()

				if (!updatedCategory) {
					throw new Error(
						`Error update data reorder. Category = ${JSON.stringify(item)}`
					)
				}

				return updatedCategory
			})
		)
	}

	/* reorder end */
}
