import { HydratedDocument } from 'mongoose'
import { Sessions } from './session.schema'
import { UserSession } from '@lapricot/shared/types'

export type ISessions = HydratedDocument<Sessions>
export type SessionWithParsedString = {
	_id: string
	expires: Date
	cookie: {
		expires: Date
	}
	auth: UserSession
}
