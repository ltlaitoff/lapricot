import { CreateStatisticDate } from '@lapricot/shared/types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CreateStatisticDto implements CreateStatisticDate {
	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	date: string

	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	count: number

	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	comment: string

	@ApiProperty()
	@IsNotEmpty()
	@IsString()
	category: string
}
