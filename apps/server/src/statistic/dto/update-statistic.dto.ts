import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsNumber } from 'class-validator'
import { UpdateStatisticData } from '@lapricot/shared/types'
import { CreateStatisticDto } from './create-statistic.dto'

export class UpdateStatisticDto
	extends PartialType(CreateStatisticDto)
	implements UpdateStatisticData
{
	@ApiProperty()
	@IsBoolean()
	trash: boolean

	@ApiProperty()
	@IsNumber()
	trash_expires: number
}
