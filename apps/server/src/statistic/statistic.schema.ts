import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import mongoose, { ObjectId } from 'mongoose'

import { Category } from '../category/category.schema'
import { User } from '../user/user.schema'

import { Statistic as StatisticType } from '@lapricot/shared/types'

interface StatisticSchemaType
	extends Omit<StatisticType, '_id' | 'date' | 'category'> {
	date: Date
	category: ObjectId
}

@Schema()
export class Statistic implements StatisticSchemaType {
	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		ref: User.name,
		required: true
	})
	user: ObjectId

	@Prop({ required: true })
	date: Date

	@Prop({ required: true })
	count: number

	@Prop()
	comment: string

	@Prop({
		type: mongoose.Schema.Types.ObjectId,
		ref: Category.name,
		required: true
	})
	category: ObjectId

	@Prop({ required: true })
	trash: boolean

	@Prop({ required: true, default: -1 })
	trash_expires: number
}

export const StatisticSchema = SchemaFactory.createForClass(Statistic)
