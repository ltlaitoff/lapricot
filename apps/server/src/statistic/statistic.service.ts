import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { TRASH_MAX_AGE } from '../config/constants.config'
import { UserIdSession } from '../app.interfaces'
import { CreateStatisticDto } from './dto/create-statistic.dto'
import { UpdateStatisticDto } from './dto/update-statistic.dto'
import { PROJECTIONS } from './statistic.config'
import { IStatistic } from './statistic.interface'
import { Statistic } from './statistic.schema'

@Injectable()
export class StatisticService {
	constructor(
		@InjectModel(Statistic.name) private statisticModel: Model<IStatistic>
	) {}

	async getAll(userId: UserIdSession) {
		const userAllStastistic = this.statisticModel
			.find({ user: userId }, PROJECTIONS)
			.lean()

		return await userAllStastistic.lean()
	}

	async getById(id: string, userId: UserIdSession) {
		return await this.statisticModel
			.find({ _id: id, user: userId }, PROJECTIONS)
			.lean()
	}

	async add(body: CreateStatisticDto, userId: UserIdSession) {
		const dataForAdd = {
			user: userId,
			date: body.date,
			count: body.count,
			comment: body.comment,
			category: body.category,
			trash: false,
			trash_expires: -1
		}

		const newStatistic = await new this.statisticModel(dataForAdd)

		const newStatisticDocument = await newStatistic.save()

		return newStatisticDocument
	}

	async delete(id: string, userId: UserIdSession): Promise<unknown> {
		return await this.statisticModel.deleteOne({
			_id: id,
			user: userId
		})
	}

	async deleteAllRecordsByCategory(
		categoryId: string,
		userId: UserIdSession
	): Promise<unknown> {
		return await this.statisticModel.deleteMany({
			category: categoryId,
			user: userId
		})
	}

	async edit(id: string, body: UpdateStatisticDto, userId: UserIdSession) {
		const updatedStatistic = await this.statisticModel.findByIdAndUpdate(
			{ _id: id, user: userId },
			body,
			{ new: true }
		)

		if (!updatedStatistic) {
			return null
		}

		const updatedStatisticDocument = await updatedStatistic.save()

		return updatedStatisticDocument
	}

	async __getAllStatistic(): Promise<Statistic[]> {
		const allStatistic = this.statisticModel.find()

		return await allStatistic.lean()
	}

	/* Trash */
	async moveToTrash(
		id: string,
		userId: UserIdSession
	): Promise<Statistic | null> {
		const statistic = await this.statisticModel.findOne({
			_id: id,
			user: userId
		})

		if (statistic === null) return null
		if (statistic.trash) return statistic

		const trash_expires = new Date(Date.now()).getTime() + TRASH_MAX_AGE

		statistic.trash = true
		statistic.trash_expires = trash_expires

		return statistic.save()
	}

	async moveFromTrash(
		id: string,
		userId: UserIdSession
	): Promise<Statistic | null> {
		const statistic = await this.statisticModel.findOne({
			_id: id,
			user: userId
		})

		if (statistic === null) return null
		if (statistic.trash === false) return statistic

		statistic.trash = false
		statistic.trash_expires = -1

		return statistic.save()
	}

	async moveAllStatisticByCategoryToTrash(
		categoryId: string,
		userId: UserIdSession,
		trash_expires: number
	) {
		const allStatisticByUser = await this.getAll(userId)

		const allStatisticFilteredByCategory = allStatisticByUser.filter(
			statistic => {
				return statistic.category.toString() === categoryId
			}
		)

		allStatisticFilteredByCategory.map(statistic => {
			this.edit(
				statistic._id.toString(),
				{
					trash: true,
					trash_expires: trash_expires
				},
				userId
			)
		})

		return
	}

	async moveAllStatisticByCategoryFromTrash(
		categoryId: string,
		userId: UserIdSession,
		timestamp: number
	) {
		const allStatisticByUser = await this.getAll(userId)

		const filteredStatistic = allStatisticByUser.filter(statistic => {
			return (
				statistic.category.toString() === categoryId &&
				statistic.trash &&
				statistic.trash_expires === timestamp
			)
		})

		filteredStatistic.map(statistic => {
			this.edit(
				statistic._id.toString(),
				{
					trash: false,
					trash_expires: -1
				},
				userId
			)
		})

		return
	}

	async __updateStatisticFields() {
		const result: unknown[] = []

		// Remove `summ` field
		result.push(
			await this.statisticModel.updateMany(
				{
					summ: { $exists: true }
				},
				{ $unset: { summ: 1 } },
				{ strict: '' }
			)
		)

		// Add 'trash' field
		result.push({
			add_trash: await this.statisticModel.updateMany(
				{ trash: { $exists: false } },
				{ $set: { trash: false } }
			)
		})

		// Add 'trash_expires' field
		result.push({
			add_trash_expires: await this.statisticModel.updateMany(
				{ trash_expires: { $exists: false } },
				{ $set: { trash_expires: -1 } }
			)
		})

		return result
	}
}
