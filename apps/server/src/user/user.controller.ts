import {
	Body,
	Controller,
	Get,
	HttpStatus,
	Param,
	Put,
	Res,
	Session
} from '@nestjs/common'
import type { Response } from 'express'
import { ApiTags } from '@nestjs/swagger'
import { UserService } from './user.service'
import { SessionData } from 'express-session'
import { UpdateUserDto } from './dto/update-user.dto'

@Controller('user')
export class userController {
	constructor(private userService: UserService) {}

	@ApiTags('user')
	@Get(':nickname')
	async all(@Param('nickname') nickname: string, @Res() res: Response) {
		res
			.status(HttpStatus.OK)
			.json(await this.userService.getUserDataWithStatisticByNickname(nickname))
	}

	@ApiTags('user')
	@Put()
	async update(
		@Session() session: SessionData,
		@Body() newData: UpdateUserDto,
		@Res() res: Response
	) {
		if (session.auth === undefined || !session.auth.authorized) {
			res.status(HttpStatus.UNAUTHORIZED).json({ message: 'Unauthorized' })
			return
		}

		res
			.status(HttpStatus.OK)
			.json(await this.userService.updateUser(session.auth.userId, newData))
	}
}
