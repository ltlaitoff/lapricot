import { Module } from '@nestjs/common'
import { UserService } from './user.service'
import { MongooseModule } from '@nestjs/mongoose'
import { User, UserSchema } from './user.schema'
import { CategoryModule } from '../category/category.module'
import { StatisticModule } from '../statistic/statistic.module'
import { userController } from './user.controller'

@Module({
	imports: [
		CategoryModule,
		StatisticModule,
		MongooseModule.forFeature([
			{
				name: User.name,
				schema: UserSchema
			}
		])
	],
	controllers: [userController],
	providers: [UserService],
	exports: [UserService]
})
export class UserModule {}
