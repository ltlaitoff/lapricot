import { UserBase } from '@lapricot/shared/types'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { now } from 'mongoose'

@Schema({ timestamps: true })
export class User implements Omit<UserBase, '_id'> {
	@Prop({ required: true })
	name: string

	@Prop({ required: true })
	nickname: string

	@Prop({ required: true })
	picture: string

	@Prop({ required: true })
	email: string

	@Prop({ required: true })
	email_verified: boolean

	@Prop({ required: true })
	given_name: string

	@Prop({ required: true })
	family_name: string

	@Prop({ default: now() })
	createdAt: Date

	@Prop({ default: now() })
	updatedAt: Date
}

export const UserSchema = SchemaFactory.createForClass(User)
