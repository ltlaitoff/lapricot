import { Body, Injectable } from '@nestjs/common'

import { InjectModel } from '@nestjs/mongoose'
import mongoose, { Model, ObjectId } from 'mongoose'
import { IUser } from './user.interface'
import { User } from './user.schema'
import { CreateUserDto } from './dto/create-user.dto'
import { CategoryService } from '../category/category.service'
import { StatisticService } from '../statistic/statistic.service'
import { UserProfile } from '@lapricot/shared/types'
import { UserIdSession } from '../app.interfaces'
import { UpdateUserDto } from './dto/update-user.dto'

@Injectable()
export class UserService {
	constructor(
		@InjectModel(User.name) private userModel: Model<IUser>,
		private categoryService: CategoryService,
		private statisticService: StatisticService
	) {}

	async create(
		@Body() createUserDto: CreateUserDto,
		options: {
			intializeDefaultCategories: boolean
		} = { intializeDefaultCategories: false }
	): Promise<IUser> {
		const newUser = await new this.userModel(createUserDto)

		if (options.intializeDefaultCategories) {
			this.categoryService.intializeUserDefaultCategories(newUser._id)
		}

		return newUser.save()
	}

	async find(
		data: Partial<User & { _id: string | ObjectId | mongoose.Types.ObjectId }>
	): Promise<IUser | null> {
		return await this.userModel.findOne(data).lean()
	}

	async findOrCreate(data: CreateUserDto): Promise<IUser> {
		/*
		 * ! SECURITY: On user change email or name system create new user account !
		 */

		/*
			Google and update picture link to avatar
			findUser must be only by email and name
		*/

		const dataForFind = {
			name: data.name,
			email: data.email,
			email_verified: data.email_verified,
			given_name: data.given_name,
			family_name: data.family_name
		}

		const findedUser = await this.find(dataForFind)

		if (findedUser !== null) {
			return findedUser
		}

		// @ts-expect-error Add nickname field for creating user
		dataForFind.nickname = data.email.split('@')[0]

		return await this.create(data)
	}

	async getUserDataWithStatisticByNickname(
		nickname: string
	): Promise<UserProfile | null> {
		const userData = await this.userModel.findOne({ nickname: nickname }).lean()

		if (userData === null) return null

		const userCategories = await this.categoryService.getAll(userData._id)
		const userStatistic = await this.statisticService.getAll(userData._id)

		return {
			...userData,
			_id: String(userData._id),
			// @ts-expect-error ObjectId is auto trasform to string
			categories: userCategories,
			// @ts-expect-error ObjectId is auto trasform to string
			statistic: userStatistic
		}
	}

	async updateUser(id: UserIdSession, body: UpdateUserDto) {
		const updatedUser = await this.userModel.findByIdAndUpdate(
			{ _id: id },
			body,
			{ new: true }
		)

		if (!updatedUser) return null

		return await updatedUser.save()
	}

	async __addToAllUsersNickname() {
		const result: unknown[] = []

		const users = await this.userModel.find({})
		const nowDate = new Date(Date.now())

		users.map(user => {
			if (!user.nickname) {
				user.nickname = user.email.split('@')[0] || String(user._id)
			}

			if (!user.createdAt) {
				user.createdAt = nowDate
			}

			user.save()
		})

		result.push(await this.userModel.find({}))

		return result
	}
}
