describe('ui', () => {
	beforeEach(() =>
		cy.visit('/iframe.html?id=footercomponent--primary&args=version;type:dev;')
	)
	it('should render the component', () => {
		cy.get('la-footer').should('exist')
	})
})
