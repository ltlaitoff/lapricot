describe('ui', () => {
	beforeEach(() =>
		cy.visit('/iframe.html?id=usedsourcelinkcomponent--primary&args=link;icon;')
	)
	it('should render the component', () => {
		cy.get('la-used-source-link').should('exist')
	})
})
