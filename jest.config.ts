import { getJestProjects } from '@nx/jest'

export default {
	projects: getJestProjects(),
	coverageReporters: ['text']
}
