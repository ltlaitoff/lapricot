const nxPreset = require('@nx/jest/preset').default

module.exports = {
	...nxPreset,
	coverageReporters: ['json', 'cobertura'],
	setupFilesAfterEnv: ['./tests/check-assertions-number.ts']
}
