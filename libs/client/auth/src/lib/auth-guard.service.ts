import { Injectable } from '@angular/core'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { ReplaySubject, filter, firstValueFrom } from 'rxjs'

import { ApiService } from '@lapricot/client-lib/data-access'
import { InitializeFailed, User } from '@lapricot/shared/types'
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login'
import * as Bowser from 'bowser'
import { UserAuthHardware } from '@lapricot/shared/types'

@Injectable({
	providedIn: 'root'
})
export class AuthGuardService {
	public authGuardData = new ReplaySubject<InitializeFailed | User>(1)
	private socialAuthServiceUser: SocialUser | null = null
	private routerEventsSubject = new ReplaySubject<NavigationEnd>(1)

	constructor(
		private router: Router,
		private apiService: ApiService,
		private socialAuthService: SocialAuthService
	) {
		router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe((event: NavigationEnd) => this.routerEventsSubject.next(event))

		this.authGuardData.subscribe(async value => {
			const routerEvent: NavigationEnd = await firstValueFrom(
				this.routerEventsSubject
			)

			if (value.authorized === false) {
				if (!routerEvent.url.includes('/profile/')) {
					this.router.navigate(['/authorization'])
				}
			}
		})

		this.initialize()

		this.socialAuthService.authState.subscribe(user => {
			this.socialAuthServiceUser = user

			if (user === null) return

			this.apiService
				.authorization(user.idToken, this.getUserHardware())
				.subscribe(authorizationValue => {
					this.authorize(authorizationValue)
				})
		})
	}

	initialize() {
		this.apiService.initialize().subscribe(value => {
			this.authGuardData.next(value)
		})
	}

	authorize(data: User) {
		this.authGuardData.next(data)
		this.router.navigate(['/'])
	}

	unauthorize() {
		this.apiService.logout().subscribe(value => {
			if (value.status !== 'ok') {
				throw new Error(`Log out error! Response /logout: ${value}`)
			}

			if (this.socialAuthServiceUser) {
				this.socialAuthService.signOut()
			}

			this.authGuardData.next({ authorized: false })
			this.router.navigate(['/authorization'])
		})
	}

	private getUserHardware(): UserAuthHardware {
		const userAgent = window.navigator.userAgent

		const hardware = Bowser.parse(window.navigator.userAgent)

		return {
			browserName: hardware.browser.name || 'Unknown',
			browserVersion: hardware.browser.version || '',
			osName: hardware.os.name || 'Unknown',
			osVersion: hardware.os.version || '',
			osVersionName: hardware.os.versionName || '',
			userAgent: userAgent,
			platformType: hardware.platform.type || 'Unknown',
			dateOfCreate: new Date(Date.now()).getTime()
		}
	}
}
