import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import {
	CategoriesBasicSet,
	ReorderCategoryData,
	ReorderCategoryGroupData,
	ReorderCategoryGroupReturnData,
	ReorderCategoryReturnData,
	User,
	CreateCategoryGroupData,
	CreateStatisticDate,
	UserAuthHardware,
	UserSession,
	InitializeFailed,
	InitializeSuccess,
	ClientEnvironment,
	Seance,
	ReorderSeanceData,
	CreateSeance,
	UpdateSeance,
	ReorderSeanceReturnData,
	UserProfile
} from '@lapricot/shared/types'

import { Category, Statistic, CategoryGroup } from '@lapricot/shared/types'

import { APP_CONFIG } from '@lapricot/client-lib/config'

@Injectable({
	providedIn: 'root'
})
export class ApiService {
	API_BASE_URL: string

	initialize() {
		const initialize = this.http.get<InitializeFailed | InitializeSuccess>(
			`${this.API_BASE_URL}/initialize`,
			{
				withCredentials: true,
				responseType: 'json'
			}
		)

		return initialize
	}

	/* User */

	getUserDataByNickname$(nickname: string) {
		return this.http.get<UserProfile>(`${this.API_BASE_URL}/user/${nickname}`, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	updateUserData$(data: { name: string; nickname: string }) {
		return this.http.put(`${this.API_BASE_URL}/user/`, data, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	/* Authorization */

	authorization(
		JWTGoogleAuthorizationToken: string,
		userData: UserAuthHardware
	) {
		return this.http.post<User>(
			`${this.API_BASE_URL}/authorization`,
			userData,
			{
				withCredentials: true,
				responseType: 'json',
				headers: new HttpHeaders({
					Authorization: JWTGoogleAuthorizationToken
				})
			}
		)
	}

	logout() {
		return this.http.post<{ status: 'ok' | unknown }>(
			`${this.API_BASE_URL}/authorization/logout`,
			null,
			{
				withCredentials: true,
				responseType: 'json'
			}
		)
	}

	/* Statistic */

	getAllStatisticRecords() {
		return this.http.get<Statistic[]>(`${this.API_BASE_URL}/statistic/all`, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	addStatisticRecord(data: CreateStatisticDate) {
		return this.http.post<Statistic>(
			`${this.API_BASE_URL}/statistic/add`,
			data,
			{
				withCredentials: true
			}
		)
	}

	deleteStatistic(id: string) {
		return this.http.delete<Statistic>(`${this.API_BASE_URL}/statistic/${id}`, {
			withCredentials: true
		})
	}

	deleteStatisticPermanent(id: string) {
		return this.http.delete<unknown>(
			`${this.API_BASE_URL}/statistic/${id}/permanent`,
			{
				withCredentials: true
			}
		)
	}

	updateStatistic(id: string, data: CreateStatisticDate) {
		return this.http.put<Statistic>(
			`${this.API_BASE_URL}/statistic/${id}`,
			data,
			{
				withCredentials: true
			}
		)
	}

	/* Category */

	getAllCategories() {
		return this.http.get<Category[]>(`${this.API_BASE_URL}/category/all`, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	addCategory(data: CategoriesBasicSet) {
		return this.http.post<Category>(`${this.API_BASE_URL}/category/add`, data, {
			withCredentials: true
		})
	}

	deleteCategory(id: string) {
		return this.http.delete<Category>(`${this.API_BASE_URL}/category/${id}`, {
			withCredentials: true
		})
	}

	deleteCategoryPermanent(id: string) {
		return this.http.delete<unknown>(
			`${this.API_BASE_URL}/category/${id}/permanent`,
			{
				withCredentials: true
			}
		)
	}

	restoreCategory(id: string) {
		return this.http.post<Category>(
			`${this.API_BASE_URL}/category/${id}/restore`,
			null,
			{
				withCredentials: true
			}
		)
	}

	updateCategory(id: string, data: CategoriesBasicSet) {
		return this.http.put<Category>(
			`${this.API_BASE_URL}/category/${id}`,
			data,
			{
				withCredentials: true
			}
		)
	}

	archiveCategory(id: string) {
		return this.http.put<Category>(
			`${this.API_BASE_URL}/category/archive/${id}`,
			null,
			{
				withCredentials: true
			}
		)
	}

	moveCategoryFromTrash$(id: string) {
		return this.http.put<Category>(
			`${this.API_BASE_URL}/category/fromtrash/${id}`,
			null,
			{
				withCredentials: true
			}
		)
	}

	reorderCategory(data: ReorderCategoryData) {
		return this.http.put<ReorderCategoryReturnData>(
			`${this.API_BASE_URL}/category/reorder`,
			data,
			{
				withCredentials: true
			}
		)
	}

	/* Category Groups */

	getAllCategoryGroups() {
		return this.http.get<CategoryGroup[]>(`${this.API_BASE_URL}/group/all`, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	addCategoryGroups(data: CreateCategoryGroupData) {
		return this.http.post<CategoryGroup>(
			`${this.API_BASE_URL}/group/add`,
			data,
			{
				withCredentials: true
			}
		)
	}

	deleteCategoryGroups(id: string) {
		return this.http.delete(`${this.API_BASE_URL}/group/${id}`, {
			withCredentials: true
		})
	}

	updateCategoryGroups(id: string, data: CreateCategoryGroupData) {
		return this.http.put<CategoryGroup>(
			`${this.API_BASE_URL}/group/${id}`,
			data,
			{
				withCredentials: true
			}
		)
	}

	reorderCategoryGroups(data: ReorderCategoryGroupData) {
		return this.http.put<ReorderCategoryGroupReturnData>(
			`${this.API_BASE_URL}/group/reorder`,
			data,
			{
				withCredentials: true
			}
		)
	}

	/* Sessions */
	getUserSessions() {
		return this.http.get<UserSession[]>(`${this.API_BASE_URL}/session`, {
			withCredentials: true
		})
	}

	deleteUserSession(id: string) {
		return this.http.delete<{ _id: string }>(
			`${this.API_BASE_URL}/session/${id}`,
			{
				withCredentials: true
			}
		)
	}

	/* Seance */
	getAllSeances$() {
		return this.http.get<Seance[]>(`${this.API_BASE_URL}/seance/all`, {
			withCredentials: true,
			responseType: 'json'
		})
	}

	addSeance$(data: CreateSeance) {
		return this.http.post<Seance>(`${this.API_BASE_URL}/seance/add`, data, {
			withCredentials: true
		})
	}

	deleteSeance$(id: string) {
		return this.http.delete<Seance>(`${this.API_BASE_URL}/seance/${id}`, {
			withCredentials: true
		})
	}

	deleteSeancePermanent$(id: string) {
		return this.http.delete<unknown>(
			`${this.API_BASE_URL}/seance/${id}/permanent`,
			{
				withCredentials: true
			}
		)
	}

	restoreSeance$(id: string) {
		return this.http.post<Seance>(
			`${this.API_BASE_URL}/seance/${id}/restore`,
			null,
			{
				withCredentials: true
			}
		)
	}

	updateSeance$(id: string, data: UpdateSeance) {
		return this.http.put<Seance>(`${this.API_BASE_URL}/seance/${id}`, data, {
			withCredentials: true
		})
	}

	archiveSeance$(id: string) {
		return this.http.put<Seance>(
			`${this.API_BASE_URL}/seance/archive/${id}`,
			null,
			{
				withCredentials: true
			}
		)
	}

	moveSeanceFromTrash$(id: string) {
		return this.http.put<Seance>(
			`${this.API_BASE_URL}/seance/fromtrash/${id}`,
			null,
			{
				withCredentials: true
			}
		)
	}

	reorderSeance$(data: ReorderSeanceData) {
		return this.http.put<ReorderSeanceReturnData>(
			`${this.API_BASE_URL}/seance/reorder`,
			data,
			{
				withCredentials: true
			}
		)
	}

	constructor(
		private http: HttpClient,
		@Inject(APP_CONFIG) private appConfig: ClientEnvironment
	) {
		this.API_BASE_URL = appConfig.API_HOST
	}
}
