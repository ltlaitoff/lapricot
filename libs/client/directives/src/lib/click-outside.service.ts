import { Injectable, OnDestroy } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable({
	providedIn: 'root'
})
export class ClickOutsideService implements OnDestroy {
	constructor() {
		document.addEventListener('click', this.newClick)
	}

	public click$: Subject<MouseEvent> = new Subject()

	ngOnDestroy() {
		document.removeEventListener('click', this.newClick)
	}

	private newClick = (e: MouseEvent) => {
		this.click$.next(e)
	}
}
