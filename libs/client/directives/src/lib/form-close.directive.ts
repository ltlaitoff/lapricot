import {
	Directive,
	ElementRef,
	EventEmitter,
	Input,
	OnDestroy,
	Output
} from '@angular/core'
import { Subscription } from 'rxjs'
import { ClickOutsideService } from './click-outside.service'
import { KeydownService } from './keydown.service'

@Directive({
	selector: '[laFormClose]',
	standalone: true
})
export class FormCloseDirective implements OnDestroy {
	constructor(
		private el: ElementRef,
		private clickOutsideService: ClickOutsideService,
		private keydownService: KeydownService
	) {
		this.clickOutsideSubscription = clickOutsideService.click$.subscribe(
			(e: MouseEvent) => {
				if (this.formCloseActive) this.onClick(e.target)
			}
		)

		this.keydownEscapeSubscription = keydownService.keydownEscape$.subscribe(
			() => {
				if (this.formCloseActive) this.onEscape()
			}
		)
	}

	private clickOutsideSubscription: Subscription
	private keydownEscapeSubscription: Subscription

	@Input() formCloseActive = true
	@Output() formClose = new EventEmitter()

	ngOnDestroy() {
		this.clickOutsideSubscription.unsubscribe()
	}

	private onClick(target: EventTarget | null) {
		if (!this.formCloseActive) return

		const clickedInside = this.el.nativeElement.contains(target)

		if (clickedInside) return

		this.formClose.emit()
	}

	private onEscape() {
		if (!this.formCloseActive) return

		this.formClose.emit()
	}
}
