import { Injectable, OnDestroy } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable({
	providedIn: 'root'
})
export class KeydownService implements OnDestroy {
	constructor() {
		document.addEventListener('keydown', this.newKeydown)
	}

	// public keydown$: Subject<KeyboardEvent> = new Subject()
	public keydownEscape$: Subject<unknown> = new Subject()

	ngOnDestroy() {
		document.removeEventListener('keydown', this.newKeydown)
	}

	private newKeydown = (e: KeyboardEvent) => {
		// this.keydown$.next(e)

		if (e.key === 'Escape') {
			this.keydownEscape$.next(true)
		}
	}
}
