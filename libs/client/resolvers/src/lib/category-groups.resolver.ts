import { ResolveFn } from '@angular/router'
import { inject } from '@angular/core'
import { Store } from '@ngrx/store'
import { RootState, CategoryGroupsActions } from '@lapricot/client-lib/store'

export const categoryGroupsResolver: ResolveFn<boolean> = () => {
	const store: Store<RootState> = inject(Store)

	store.dispatch(CategoryGroupsActions.load())

	return true
}
