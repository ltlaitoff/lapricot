import { ResolveFn } from '@angular/router'
import { inject } from '@angular/core'
import { Store } from '@ngrx/store'
import {
	RootState,
	LoadStatus,
	CategoriesActions,
	selectCategoriesStatus
} from '@lapricot/client-lib/store'
import { firstValueFrom } from 'rxjs'

export const categoriesResolver: ResolveFn<boolean> = async () => {
	const store: Store<RootState> = inject(Store)

	await firstValueFrom(store.select(selectCategoriesStatus)).then(value => {
		if (value === LoadStatus.NOT_SYNCHRONIZED) {
			store.dispatch(CategoriesActions.load({ force: true }))
		}

		store.dispatch(CategoriesActions.load())
	})

	return true
}
