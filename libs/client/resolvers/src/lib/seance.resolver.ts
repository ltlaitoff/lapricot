import { ResolveFn } from '@angular/router'
import { inject } from '@angular/core'
import { Store } from '@ngrx/store'
import {
	RootState,
	LoadStatus,
	SeanceActions,
	selectSeanceStatus
} from '@lapricot/client-lib/store'
import { firstValueFrom } from 'rxjs'

export const seanceResolver: ResolveFn<boolean> = async () => {
	const store: Store<RootState> = inject(Store)

	await firstValueFrom(store.select(selectSeanceStatus)).then(value => {
		if (value === LoadStatus.NOT_SYNCHRONIZED) {
			store.dispatch(SeanceActions.load({ force: true }))
		}

		store.dispatch(SeanceActions.load())
	})

	return true
}
