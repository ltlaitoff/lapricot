import { ResolveFn } from '@angular/router'
import { inject } from '@angular/core'
import { Store } from '@ngrx/store'
import {
	LoadStatus,
	RootState,
	StatisticActions,
	selectStatisticStatus
} from '@lapricot/client-lib/store'
import { firstValueFrom } from 'rxjs'

export const statisticResolver: ResolveFn<boolean> = async () => {
	const store: Store<RootState> = inject(Store)

	await firstValueFrom(store.select(selectStatisticStatus)).then(value => {
		if (value === LoadStatus.NOT_SYNCHRONIZED) {
			store.dispatch(StatisticActions.load({ force: true }))
		}

		store.dispatch(StatisticActions.load())
	})

	return true
}
