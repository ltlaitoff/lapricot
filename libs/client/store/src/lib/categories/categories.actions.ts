import { createActionGroup, props } from '@ngrx/store'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { CategoryStateItem, CategorySyncStateItem } from './categories.types'

export const CategoriesActions = createActionGroup({
	source: 'Categories',
	events: {
		load: (props: { force: boolean } = { force: false }) => props,

		add: (category: CategoriesBasicSet) => category,
		delete: (category: CategoryStateItem) => category,
		deletePermanent: (category: CategoryStateItem) => category,
		restore: (category: CategoryStateItem) => category,
		update: props<{
			oldCategory: CategoryStateItem
			dataForUpdate: CategoriesBasicSet
		}>(),
		reorder: props<{
			category: CategorySyncStateItem
			previousIndex: number
			currentIndex: number
		}>(),
		archiveToggle: (category: CategoryStateItem) => category
	}
})
