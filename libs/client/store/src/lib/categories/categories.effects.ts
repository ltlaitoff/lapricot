import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { select, Store } from '@ngrx/store'
import { EMPTY, merge, of } from 'rxjs'
import {
	switchMap,
	catchError,
	withLatestFrom,
	mergeMap,
	map,
	filter
} from 'rxjs/operators'
import { ApiService } from '@lapricot/client-lib/data-access'
import { CategoriesActions } from './categories.actions'
import { CategoriesNotSyncActions } from './not-sync/categories-not-sync.actions'
import { CategoriesSyncActions } from './sync/categories-sync.actions'

import { RootState } from '../rootTypes'
import {
	changeAddCategoryValueToStoreItem,
	changeUpdateCategoryValueToStoreItem,
	changeDeleteCategoryValueToStoreItem
} from './not-sync'
import { CategoriesStatusActions } from './status'
import {
	CategoryStateItem,
	CategoryNotSyncStateItem,
	CategorySyncStateItem
} from './categories.types'
import { NotSyncStatus } from '../store.types'
import { StatisticActions } from '../statistic'
import { CategoriesBasicSet } from '@lapricot/shared/types'

const defaultError$ = (notSyncCategory: CategoryNotSyncStateItem) => {
	return of(
		CategoriesNotSyncActions.changeStatus({
			status: NotSyncStatus.ERROR,
			category: notSyncCategory
		}),
		CategoriesStatusActions.setError()
	)
}

const filterByCategoryIsSync = (
	category: CategoryStateItem
): category is CategorySyncStateItem => {
	return category.status === undefined
}

const moveCategoryToNotSync$ = (notSyncCategory: CategoryNotSyncStateItem) => {
	return of(
		CategoriesNotSyncActions.add(notSyncCategory),
		CategoriesSyncActions.delete({
			categoryId: notSyncCategory._id
		}),
		CategoriesStatusActions.setSynchronization()
	)
}

@Injectable()
export class CategoriesEffects {
	add$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.add),
			map(data => changeAddCategoryValueToStoreItem(data)),
			mergeMap((category: CategoryNotSyncStateItem) =>
				merge(
					of(
						CategoriesNotSyncActions.add(category),
						CategoriesStatusActions.setSynchronization()
					),

					this.api.addCategory(category).pipe(
						switchMap(resultCategory => [
							CategoriesSyncActions.add({
								category: resultCategory
							}),

							CategoriesStatusActions.setSynchronized(),

							CategoriesNotSyncActions.delete(category)
						]),
						catchError(() => defaultError$(category))
					)
				)
			)
		)
	)

	update$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.update),
			filter(function (data: {
				oldCategory: CategoryStateItem
				dataForUpdate: CategoriesBasicSet
			}): data is {
				oldCategory: CategorySyncStateItem
				dataForUpdate: CategoriesBasicSet
			} {
				return data.oldCategory.status === undefined
			}),
			map(({ oldCategory, dataForUpdate }) =>
				changeUpdateCategoryValueToStoreItem(oldCategory, dataForUpdate)
			),
			mergeMap(notSyncCategory =>
				merge(
					moveCategoryToNotSync$(notSyncCategory),

					this.api.updateCategory(notSyncCategory._id, notSyncCategory).pipe(
						switchMap(resultCategory =>
							of(
								CategoriesSyncActions.add({
									category: resultCategory
								}),
								CategoriesNotSyncActions.delete(notSyncCategory),
								CategoriesStatusActions.setSynchronized()
							)
						),
						catchError(() => defaultError$(notSyncCategory))
					)
				)
			)
		)
	)

	restore$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.restore),
			filter(filterByCategoryIsSync),
			map(category => changeUpdateCategoryValueToStoreItem(category)),
			mergeMap(notSyncCategory =>
				merge(
					moveCategoryToNotSync$(notSyncCategory),

					this.api.restoreCategory(notSyncCategory._id).pipe(
						switchMap(resultCategory =>
							of(
								CategoriesSyncActions.add({
									category: resultCategory
								}),

								CategoriesStatusActions.setSynchronized(),

								CategoriesNotSyncActions.delete(notSyncCategory),
								StatisticActions.load({ force: true })
							)
						),
						catchError(() => defaultError$(notSyncCategory))
					)
				)
			)
		)
	)

	reorder$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.reorder),
			map(({ category, previousIndex, currentIndex }) => ({
				syncCategory: category,
				notSyncCategory: {
					...changeUpdateCategoryValueToStoreItem(category, {}),
					order: previousIndex < currentIndex ? currentIndex + 1 : currentIndex
				},
				previousIndex,
				currentIndex
			})),
			mergeMap(
				({ syncCategory, notSyncCategory, previousIndex, currentIndex }) =>
					merge(
						moveCategoryToNotSync$(notSyncCategory),
						this.api
							.reorderCategory({
								categoryId: notSyncCategory._id,
								previousIndex,
								currentIndex
							})
							.pipe(
								switchMap(resultValue =>
									of(
										...resultValue.map(value => {
											if (value.categoryId === notSyncCategory._id) {
												return CategoriesSyncActions.add({
													category: {
														...syncCategory,
														order: value.currentIndex
													}
												})
											}

											return CategoriesSyncActions.orderUpdate({
												data: value
											})
										}),
										CategoriesStatusActions.setSynchronized(),
										CategoriesNotSyncActions.delete(notSyncCategory)
									)
								),
								catchError(() => defaultError$(notSyncCategory))
							)
					)
			)
		)
	)

	delete$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.delete),
			filter(filterByCategoryIsSync),
			map(category => changeDeleteCategoryValueToStoreItem(category)),
			mergeMap(notSyncCategory =>
				merge(
					moveCategoryToNotSync$(notSyncCategory),

					this.api.deleteCategory(notSyncCategory._id).pipe(
						switchMap(resultCategory => [
							CategoriesSyncActions.add({
								category: resultCategory
							}),

							CategoriesStatusActions.setSynchronized(),
							CategoriesNotSyncActions.delete(notSyncCategory),
							StatisticActions.load({ force: true })
						]),
						catchError(() => defaultError$(notSyncCategory))
					)
				)
			)
		)
	)

	deletePermanent$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.deletePermanent),
			filter(filterByCategoryIsSync),
			map(category => changeDeleteCategoryValueToStoreItem(category)),
			mergeMap(notSyncCategory =>
				merge(
					moveCategoryToNotSync$(notSyncCategory),

					this.api.deleteCategoryPermanent(notSyncCategory._id).pipe(
						switchMap(() =>
							of(
								CategoriesStatusActions.setSynchronized(),
								CategoriesNotSyncActions.delete(notSyncCategory),
								StatisticActions.load({ force: true })
							)
						),
						catchError(() => defaultError$(notSyncCategory))
					)
				)
			)
		)
	)

	archiveToggle$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.archiveToggle),
			filter(filterByCategoryIsSync),
			map(category => changeDeleteCategoryValueToStoreItem(category)),
			mergeMap(notSyncCategory =>
				merge(
					moveCategoryToNotSync$(notSyncCategory),

					this.api.archiveCategory(notSyncCategory._id).pipe(
						switchMap(resultCategory =>
							of(
								CategoriesSyncActions.add({
									category: resultCategory
								}),

								CategoriesStatusActions.setSynchronized(),

								CategoriesNotSyncActions.delete(notSyncCategory)
							)
						),
						catchError(() => defaultError$(notSyncCategory))
					)
				)
			)
		)
	)

	loadCategories$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoriesActions.load),
			withLatestFrom(this.store.pipe(select('categories'))),
			mergeMap(([params, categoriesValue]) => {
				if (categoriesValue.length === 0 || params.force) {
					return merge(
						of(CategoriesStatusActions.setSynchronization()),

						this.api.getAllCategories().pipe(
							mergeMap(value => [
								CategoriesSyncActions.set({ categories: value }),
								CategoriesStatusActions.setSynchronized()
							]),
							catchError(() => of(CategoriesStatusActions.setError()))
						)
					)
				}

				return EMPTY
			})
		)
	)

	constructor(
		private actions$: Actions,
		private api: ApiService,
		private store: Store<RootState>
	) {}
}
