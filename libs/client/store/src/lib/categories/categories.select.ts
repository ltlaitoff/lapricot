import { RootState } from '../rootTypes'
import { CategoryStateItem } from './categories.types'
import { filterByTrash } from '../helpers/filter-by-trash.helper'
import { CategoriesStatusTypes } from './status'

export const selectCategoriesTrashed = (
	state: RootState
): CategoryStateItem[] => {
	return filterByTrash([...state.categories, ...state.notSyncCategories], true)
}

export const selectCategories = (state: RootState): CategoryStateItem[] => {
	return filterByTrash([...state.categories, ...state.notSyncCategories])
}

export const selectCategoriesAll = (state: RootState): CategoryStateItem[] => {
	return [...state.categories, ...state.notSyncCategories]
}

export const selectCategoriesStatus = (
	state: RootState
): CategoriesStatusTypes.StatusState => {
	return state.categoriesStatus
}
