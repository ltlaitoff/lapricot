import { Category } from '@lapricot/shared/types'
import { NotSyncStateItemBase, SyncStateItemBase } from '../store.types'

export type CategoryNotSyncStateItem = NotSyncStateItemBase & Category
export type CategoryNotSyncState = CategoryNotSyncStateItem[]

export type CategorySyncStateItem = SyncStateItemBase & Category
export type CategorySyncState = CategorySyncStateItem[]

export type CategoryStateItem = CategorySyncStateItem | CategoryNotSyncStateItem
