import { createActionGroup, props } from '@ngrx/store'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { NotSyncStatus } from '../../store.types'
import {
	CategoryNotSyncState,
	CategoryNotSyncStateItem
} from '../categories.types'

export const CategoriesNotSyncActions = createActionGroup({
	source: 'Categories not sync',
	events: {
		set: props<{ categories: CategoryNotSyncState }>(),
		add: props<CategoryNotSyncStateItem>(),
		changeStatus: props<{
			status: NotSyncStatus
			category: CategoryNotSyncStateItem
		}>(),
		delete: props<CategoryNotSyncStateItem>(),
		update: props<{
			oldCategory: CategoryNotSyncStateItem
			dataForUpdate: CategoriesBasicSet
		}>()
	}
})
