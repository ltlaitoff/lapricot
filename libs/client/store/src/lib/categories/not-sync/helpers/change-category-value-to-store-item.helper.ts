import { NotSyncStatus, NotSyncAction } from '../../../store.types'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import {
	CategoryNotSyncStateItem,
	CategorySyncStateItem
} from '../../categories.types'
import { generateNotSyncCategoryId } from './generate-not-sync-category-id.helper'

export function changeAddCategoryValueToStoreItem(
	data: CategoriesBasicSet
): CategoryNotSyncStateItem {
	const forAdd: CategoryNotSyncStateItem = {
		...data,
		_id: generateNotSyncCategoryId(),
		status: NotSyncStatus.NOT_SYNCHRONIZED,
		action: NotSyncAction.CREATED,
		order: 999,
		mode: data.mode || 'number',
		archived: false,
		trash: false,
		trash_expires: -1
	}

	return forAdd
}

export function changeDeleteCategoryValueToStoreItem(
	data: CategorySyncStateItem
): CategoryNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.DELETED
	}
}

export function changeArchivedCategoryValueToStoreItem(
	data: CategorySyncStateItem
): CategoryNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CHANGED,
		archived: !data.archived
	}
}

export function changeUpdateCategoryValueToStoreItem(
	oldCategoryData: CategorySyncStateItem,
	newCategoryData: Partial<CategoriesBasicSet> = {}
): CategoryNotSyncStateItem {
	return {
		...oldCategoryData,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CHANGED,
		...newCategoryData
	}
}
