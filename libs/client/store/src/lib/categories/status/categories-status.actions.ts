import { createActionGroup, emptyProps } from '@ngrx/store'

export const CategoriesStatusActions = createActionGroup({
	source: 'Categories status',
	events: {
		setNotSynchronized: emptyProps(),
		setSynchronization: emptyProps(),
		setError: emptyProps(),
		setSynchronized: emptyProps()
	}
})
