import { props, createActionGroup } from '@ngrx/store'
import { ReorderCategoryData } from '@lapricot/shared/types'
import { CategorySyncState, CategorySyncStateItem } from '../categories.types'

export const CategoriesSyncActions = createActionGroup({
	source: 'Categories sync',
	events: {
		set: props<{ categories: CategorySyncState }>(),
		add: props<{ category: CategorySyncStateItem }>(),
		delete: props<{ categoryId: string }>(),
		update: props<{ category: CategorySyncStateItem }>(),
		orderUpdate: props<{ data: ReorderCategoryData }>()
	}
})
