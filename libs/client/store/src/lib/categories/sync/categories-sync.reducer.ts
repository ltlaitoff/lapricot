import { createReducer, on } from '@ngrx/store'
import { CategoriesSyncActions } from './categories-sync.actions'
import { CategorySyncState } from '../categories.types'

export const initialState: CategorySyncState = []

export const categoriesReducer = createReducer(
	initialState,
	on(CategoriesSyncActions.set, (state, { categories }) => [...categories]),
	on(CategoriesSyncActions.add, (state, { category }) => [...state, category]),
	on(CategoriesSyncActions.delete, (state, category) => {
		return [...state.filter(item => item._id !== category.categoryId)]
	}),
	on(CategoriesSyncActions.orderUpdate, (state, { data }) => {
		return [
			...state.map(item => {
				if (item._id === data.categoryId) {
					return {
						...item,
						order: data.currentIndex
					}
				}

				return item
			})
		]
	})
)
