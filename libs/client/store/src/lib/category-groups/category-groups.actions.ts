import { createActionGroup, props } from '@ngrx/store'

import { CreateCategoryGroupData } from '@lapricot/shared/types'
import {
	CategoryGroupsStateItem,
	CategoryGroupsSyncStateItem
} from './category-groups.types'

export const CategoryGroupsActions = createActionGroup({
	source: 'CategoryGroups',
	events: {
		load: (props: { force: boolean } = { force: false }) => props,

		add: (props: CreateCategoryGroupData) => props,
		delete: (props: CategoryGroupsStateItem) => props,
		update: props<{
			old: CategoryGroupsStateItem
			dataForUpdate: CreateCategoryGroupData
		}>(),
		reorder: props<{
			props: CategoryGroupsSyncStateItem
			previousIndex: number
			currentIndex: number
		}>()
	}
})
