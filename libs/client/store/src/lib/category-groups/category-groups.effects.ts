import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { select, Store } from '@ngrx/store'
import { EMPTY, of, merge } from 'rxjs'
import {
	switchMap,
	catchError,
	withLatestFrom,
	mergeMap,
	map,
	filter
} from 'rxjs/operators'
import { ApiService } from '@lapricot/client-lib/data-access'
import { CategoryGroupsActions } from './category-groups.actions'
import { CategoryGroupsNotSyncActions } from './not-sync/category-groups-not-sync.actions'
import { CategoryGroupsSyncActions } from './sync/category-groups-sync.actions'

import { RootState } from '../rootTypes'
import { NotSyncHelpers } from './not-sync'
import {
	CategoryGroupsNotSyncStateItem,
	CategoryGroupsStateItem,
	CategoryGroupsSyncStateItem
} from './category-groups.types'
import { NotSyncStatus } from '../store.types'
import { CreateCategoryGroupData } from '@lapricot/shared/types'

const defaultCatchError$ = (payload: CategoryGroupsNotSyncStateItem) =>
	of(
		CategoryGroupsNotSyncActions.changeStatus({
			status: NotSyncStatus.ERROR,
			payload: payload
		})
	)

@Injectable()
export class CategoryGroupsEffects {
	add$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoryGroupsActions.add),
			map(payload =>
				NotSyncHelpers.changeAddCategoryGroupValueToStoreItem(payload)
			),
			mergeMap(payload =>
				merge(
					of(CategoryGroupsNotSyncActions.add(payload)),

					this.api.addCategoryGroups(payload).pipe(
						switchMap(resultCategory =>
							of(
								CategoryGroupsSyncActions.add({
									payload: resultCategory
								}),
								CategoryGroupsNotSyncActions.delete(payload)
							)
						),
						catchError(() => defaultCatchError$(payload))
					)
				)
			)
		)
	)

	update$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoryGroupsActions.update),
			filter(
				(data: {
					old: CategoryGroupsStateItem
					dataForUpdate: CreateCategoryGroupData
				}): data is {
					old: CategoryGroupsSyncStateItem
					dataForUpdate: CreateCategoryGroupData
				} => {
					return data.old.status === undefined
				}
			),
			map(({ old, dataForUpdate }) =>
				NotSyncHelpers.changeUpdateCategoryGroupValueToStoreItem(
					old,
					dataForUpdate
				)
			),
			mergeMap(payload =>
				merge(
					of(
						CategoryGroupsNotSyncActions.add(payload),
						CategoryGroupsSyncActions.delete({
							id: payload._id
						})
					),
					this.api.updateCategoryGroups(payload._id, payload).pipe(
						switchMap(resultCategory =>
							of(
								CategoryGroupsSyncActions.add({
									payload: resultCategory
								}),

								CategoryGroupsNotSyncActions.delete(payload)
							)
						),
						catchError(() => defaultCatchError$(payload))
					)
				)
			)
		)
	)

	reorder$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoryGroupsActions.reorder),
			map(({ props, previousIndex, currentIndex }) => ({
				category: {
					...NotSyncHelpers.changeUpdateCategoryGroupValueToStoreItem(
						props,
						{}
					),
					order: previousIndex < currentIndex ? currentIndex + 1 : currentIndex
				},
				previousIndex,
				currentIndex
			})),
			switchMap(({ category, previousIndex, currentIndex }) =>
				merge(
					of(
						CategoryGroupsNotSyncActions.add(category),
						CategoryGroupsSyncActions.delete({
							id: category._id
						})
					),
					this.api
						.reorderCategoryGroups({
							categoryGroupId: category._id,
							previousIndex,
							currentIndex
						})
						.pipe(
							switchMap(resultValue =>
								of(
									...resultValue.map(value => {
										if (value.categoryGroupId === category._id) {
											return CategoryGroupsSyncActions.add({
												payload: {
													...category,
													order: value.currentIndex,
													status: undefined,
													action: undefined
												} as CategoryGroupsSyncStateItem
											})
										}

										return CategoryGroupsSyncActions.orderUpdate({
											payload: value
										})
									}),

									CategoryGroupsNotSyncActions.delete(category)
								)
							),
							catchError(() => defaultCatchError$(category))
						)
				)
			)
		)
	)

	delete$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoryGroupsActions.delete),
			filter(
				(
					payload: CategoryGroupsStateItem
				): payload is CategoryGroupsSyncStateItem =>
					payload.status === undefined
			),
			map(payload =>
				NotSyncHelpers.changeDeleteCategoryGroupValueToStoreItem(payload)
			),
			switchMap(payload =>
				merge(
					of(
						CategoryGroupsNotSyncActions.add(payload),
						CategoryGroupsSyncActions.delete({
							id: payload._id
						})
					),

					this.api.deleteCategoryGroups(payload._id).pipe(
						switchMap(() => of(CategoryGroupsNotSyncActions.delete(payload))),
						catchError(() => defaultCatchError$(payload))
					)
				)
			)
		)
	)

	loadCategoryGroups$ = createEffect(() =>
		this.actions$.pipe(
			ofType(CategoryGroupsActions.load),
			// TODO: Change categories after set in root
			withLatestFrom(this.store.pipe(select('categories'))),
			switchMap(([params, categoriesValue]) => {
				if (categoriesValue.length === 0 || params.force) {
					return this.api.getAllCategoryGroups().pipe(
						mergeMap(value =>
							of(CategoryGroupsSyncActions.set({ payload: value }))
						),
						catchError(() => EMPTY)
					)
				}

				return EMPTY
			})
		)
	)

	constructor(
		private actions$: Actions,
		private api: ApiService,
		private store: Store<RootState>
	) {}
}
