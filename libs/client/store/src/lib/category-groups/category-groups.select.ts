import { RootState } from '../rootTypes'
import { CategoryGroupsStateItem } from './category-groups.types'

export const selectCategoryGroups = (
	state: RootState
): CategoryGroupsStateItem[] => {
	return [...state.syncCategoryGroups, ...state.notSyncCategoryGroups]
}
