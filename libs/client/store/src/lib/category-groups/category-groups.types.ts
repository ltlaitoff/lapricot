import { NotSyncStateItemBase, SyncStateItemBase } from '../store.types'

import { CategoryGroup } from '@lapricot/shared/types'

export type CategoryGroupsNotSyncStateItem = NotSyncStateItemBase &
	CategoryGroup
export type CategoryGroupsNotSyncState = CategoryGroupsNotSyncStateItem[]

export type CategoryGroupsSyncStateItem = SyncStateItemBase & CategoryGroup
export type CategoryGroupsSyncState = CategoryGroupsSyncStateItem[]

export type CategoryGroupsStateItem =
	| CategoryGroupsSyncStateItem
	| CategoryGroupsNotSyncStateItem
