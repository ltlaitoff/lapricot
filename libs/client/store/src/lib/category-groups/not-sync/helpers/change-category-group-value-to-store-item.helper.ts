import { NotSyncStatus, NotSyncAction } from '../../../store.types'
import { CreateCategoryGroupData } from '@lapricot/shared/types'
import {
	CategoryGroupsNotSyncStateItem,
	CategoryGroupsSyncStateItem
} from '../../category-groups.types'
import { generateNotSyncCategoryGroupId } from './generate-not-sync-category-group-id.helper'

export function changeAddCategoryGroupValueToStoreItem(
	data: CreateCategoryGroupData
): CategoryGroupsNotSyncStateItem {
	const forAdd = {
		...data,
		_id: generateNotSyncCategoryGroupId(),
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CREATED,
		order: 999
	}

	return forAdd
}

export function changeDeleteCategoryGroupValueToStoreItem(
	data: CategoryGroupsSyncStateItem
): CategoryGroupsNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.DELETED
	}
}

export function changeUpdateCategoryGroupValueToStoreItem(
	oldCategoryData: CategoryGroupsSyncStateItem,
	newCategoryData: Partial<CreateCategoryGroupData>
): CategoryGroupsNotSyncStateItem {
	return {
		...oldCategoryData,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CHANGED,
		...newCategoryData
	}
}
