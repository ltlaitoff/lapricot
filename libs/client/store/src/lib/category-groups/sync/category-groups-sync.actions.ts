import { ReorderCategoryGroupData } from '@lapricot/shared/types'
import { props, createActionGroup } from '@ngrx/store'
import {
	CategoryGroupsSyncState,
	CategoryGroupsSyncStateItem
} from '../category-groups.types'

export const CategoryGroupsSyncActions = createActionGroup({
	source: 'CategoryGroupsSync',
	events: {
		set: props<{ payload: CategoryGroupsSyncState }>(),
		add: props<{ payload: CategoryGroupsSyncStateItem }>(),
		delete: props<{ id: string }>(),
		update: props<{ payload: CategoryGroupsSyncStateItem }>(),
		orderUpdate: props<{ payload: ReorderCategoryGroupData }>()
	}
})
