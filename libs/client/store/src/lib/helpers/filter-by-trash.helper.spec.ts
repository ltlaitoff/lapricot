import { filterByTrash } from './filter-by-trash.helper'

type TestValue = { id: number; trash: boolean }[]

const TEST_CASES: Array<[TestValue, boolean, TestValue]> = [
	[[], false, []],
	[[], true, []],
	[
		[
			{ id: 0, trash: false },
			{ id: 1, trash: false }
		],
		false,
		[
			{ id: 0, trash: false },
			{ id: 1, trash: false }
		]
	],
	[
		[
			{ id: 0, trash: true },
			{ id: 1, trash: false }
		],
		false,
		[{ id: 1, trash: false }]
	],
	[
		[
			{ id: 0, trash: true },
			{ id: 1, trash: true }
		],
		false,
		[]
	],
	[
		[
			{ id: 0, trash: true },
			{ id: 1, trash: true }
		],
		true,
		[
			{ id: 0, trash: true },
			{ id: 1, trash: true }
		]
	],
	[
		[
			{ id: 0, trash: true },
			{ id: 1, trash: false }
		],
		true,
		[{ id: 0, trash: true }]
	],
	[
		[
			{ id: 0, trash: false },
			{ id: 1, trash: false }
		],
		true,
		[]
	]
]

describe('store/categories/filter-by-trash.helper', () => {
	it.each(TEST_CASES)(
		'filterByTrash on array = %j and trash = %p should return %j',
		(array, trash, arrayResult) => {
			const result = filterByTrash(array, trash)

			expect(result.length).toBe(arrayResult.length)

			result.map((item, index) => {
				expect(result[index].id).toBe(arrayResult[index].id)
				expect(result[index].trash).toBe(arrayResult[index].trash)
			})
		}
	)
})
