export function filterByTrash<T extends { trash: boolean }>(
	array: T[],
	trash = false
) {
	return array.filter(item => item.trash === trash)
}
