import { CategoriesEffects } from './categories/categories.effects'
import { CategoryGroupsEffects } from './category-groups/category-groups.effects'
import { SeanceEffects } from './seance/seance.effects'
import { StatisticEffects } from './statistic/statistic.effects'

const effects = [
	CategoriesEffects,
	StatisticEffects,
	CategoryGroupsEffects,
	SeanceEffects
]

export default effects
