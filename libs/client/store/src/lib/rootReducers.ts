import { ActionReducerMap } from '@ngrx/store'
import { RootState } from './rootTypes'

import { categoriesReducer } from './categories/sync'
import { categoriesNotSyncReducer } from './categories/not-sync'
import { statisticReducer } from './statistic/sync'
import { statisticNotSyncReducer } from './statistic/not-sync'
import { categoriesStatusReducer } from './categories/status'
import { statisticStatusReducer } from './statistic/status'
import { categoryGroupsNotSyncReducer } from './category-groups/not-sync'
import { categoryGroupsSyncReducer } from './category-groups/sync'
import { seanceReducer } from './seance/sync'
import { seanceNotSyncReducer } from './seance/not-sync'
import { seanceStatusReducer } from './seance/status'
import { seanceActiveReducer } from './seance/active'

const reducers: ActionReducerMap<RootState> = {
	categories: categoriesReducer,
	statistic: statisticReducer,
	seance: seanceReducer,
	notSyncCategories: categoriesNotSyncReducer,
	notSyncStatistic: statisticNotSyncReducer,
	notSyncSeance: seanceNotSyncReducer,
	categoriesStatus: categoriesStatusReducer,
	statisticStatus: statisticStatusReducer,
	seanceStatus: seanceStatusReducer,
	notSyncCategoryGroups: categoryGroupsNotSyncReducer,
	syncCategoryGroups: categoryGroupsSyncReducer,
	seanceActive: seanceActiveReducer
}

export default reducers
