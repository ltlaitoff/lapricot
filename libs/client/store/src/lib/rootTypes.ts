import { StatisticSyncState, StatisticNotSyncState } from './statistic'
import { CategorySyncState, CategoryNotSyncState } from './categories'
import {
	CategoryGroupsNotSyncState,
	CategoryGroupsSyncState
} from './category-groups'
import { LoadStatus } from '.'
import { SeanceNotSyncState, SeanceSyncState } from './seance'
import { SeanceActiveState } from './seance/active/seance-active.intefaces'

export interface RootState {
	categories: CategorySyncState
	statistic: StatisticSyncState
	seance: SeanceSyncState
	notSyncCategories: CategoryNotSyncState
	notSyncStatistic: StatisticNotSyncState
	notSyncSeance: SeanceNotSyncState
	categoriesStatus: LoadStatus
	statisticStatus: LoadStatus
	seanceStatus: LoadStatus
	notSyncCategoryGroups: CategoryGroupsNotSyncState
	syncCategoryGroups: CategoryGroupsSyncState
	seanceActive: SeanceActiveState
}
