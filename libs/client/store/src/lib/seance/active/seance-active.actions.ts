import { createActionGroup, emptyProps, props } from '@ngrx/store'
import { SeanceStateItemWithCategory } from '../seance.types'

export const SeanceActiveActions = createActionGroup({
	source: 'Seance active',
	events: {
		startSeance: (data: SeanceStateItemWithCategory) => data,
		endSeance: emptyProps(),
		setApproach: props<{ data: number }>(),
		setRecord: props<{ data: number }>()
	}
})
