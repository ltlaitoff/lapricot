import { SeanceStateItem } from '../seance.types'

export interface SeanceActiveStateEnabled {
	currentSeance: SeanceStateItem
	currentRecord: number
	currentApproach: number
}

export interface SeanceActiveStateDisabled {
	currentSeance: null
	currentRecord: null
	currentApproach: null
}

export type SeanceActiveState =
	| SeanceActiveStateEnabled
	| SeanceActiveStateDisabled
