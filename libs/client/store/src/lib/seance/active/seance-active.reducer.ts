import { createReducer, on } from '@ngrx/store'
import { SeanceActiveActions } from './seance-active.actions'
import { SeanceActiveState } from './seance-active.intefaces'

export const initialState: SeanceActiveState = {
	currentSeance: null,
	currentRecord: null,
	currentApproach: null
} as SeanceActiveState

export const seanceActiveReducer = createReducer(
	initialState,

	on(SeanceActiveActions.startSeance, (_, data) => {
		const newData = {
			...data,
			records: data.records.map(item => {
				return {
					...item,
					category: item.category._id
				}
			})
		}

		return {
			currentSeance: newData,
			currentRecord: 0,
			currentApproach: 0
		}
	}),
	on(SeanceActiveActions.endSeance, () => {
		return {
			currentSeance: null,
			currentRecord: null,
			currentApproach: null
		}
	}),
	on(SeanceActiveActions.setApproach, (state, { data }) => {
		if (state.currentApproach === null) return state

		return {
			...state,
			currentApproach: data
		}
	}),
	on(SeanceActiveActions.setRecord, (state, { data }) => {
		if (state.currentApproach === null) return state

		return {
			...state,
			currentRecord: data
		}
	})
)
