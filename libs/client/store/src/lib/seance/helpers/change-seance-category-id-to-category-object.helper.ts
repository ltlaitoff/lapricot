import { CategoryStateItem } from '../../categories/categories.types'
import { SeanceItemToWithCategory, SeanceStateItem } from '../seance.types'

export function changeSeanceCategoryIdToCategoryObject<
	T extends SeanceStateItem,
	Y = SeanceItemToWithCategory<T>
>(data: SeanceStateItem[], categories: CategoryStateItem[]): Y[] {
	if (categories.length === 0) return []

	const categoriesObject: Record<CategoryStateItem['_id'], CategoryStateItem> =
		categories.reduce((acc, item) => {
			return {
				...acc,
				[item._id]: item
			}
		}, {})

	return data
		.map(seanceRecord => {
			const newRecords = seanceRecord.records
				.map(item => {
					return {
						...item,
						category: categoriesObject[item.category]
					}
				})
				.filter(item => item.category !== undefined)

			return {
				...seanceRecord,
				records: newRecords
			} as Y
		})
		.filter(item => item !== null)
}
