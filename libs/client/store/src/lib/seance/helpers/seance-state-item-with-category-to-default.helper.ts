import { SeanceStateItemWithCategory, SeanceStateItem } from '../seance.types'

export function seanceStateItemWithCategoryToDefault(
	state: SeanceStateItemWithCategory
): SeanceStateItem {
	const clonedState = structuredClone(state)

	const newRecords = clonedState.records.map(item => ({
		...item,
		category: item.category._id
	}))

	return {
		...clonedState,
		records: newRecords
	}
}
