import { NotSyncStatus, NotSyncAction } from '../../../store.types'
import {
	CreateSeance,
	SeanceRecord,
	UpdateSeance
} from '@lapricot/shared/types'
import { SeanceNotSyncStateItem, SeanceSyncStateItem } from '../../seance.types'
import { generateNotSyncId } from './generate-not-sync-id.helper'

export function changeAddSeanceValueToStoreItem(
	data: CreateSeance
): SeanceNotSyncStateItem {
	const clonedData: CreateSeance = structuredClone(data)

	const records: SeanceRecord[] = clonedData.records.map(item => ({
		...item,
		trash: false,
		trash_expires: -1
	}))

	const forAdd: SeanceNotSyncStateItem = {
		...clonedData,
		records: records,
		_id: generateNotSyncId(),
		status: NotSyncStatus.NOT_SYNCHRONIZED,
		action: NotSyncAction.CREATED,
		order: 999,
		archived: false,
		trash: false,
		trash_expires: -1
	}

	return forAdd
}

export function changeDeleteSeanceValueToStoreItem(
	data: SeanceSyncStateItem
): SeanceNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.DELETED
	}
}

export function changeArchivedSeanceValueToStoreItem(
	data: SeanceSyncStateItem
): SeanceNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CHANGED,
		archived: !data.archived
	}
}

export function changeUpdateSeanceValueToStoreItem(
	oldSeanceData: SeanceSyncStateItem,
	newSeanceData: UpdateSeance = {}
): SeanceNotSyncStateItem {
	return {
		...oldSeanceData,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CHANGED,
		...newSeanceData
	}
}
