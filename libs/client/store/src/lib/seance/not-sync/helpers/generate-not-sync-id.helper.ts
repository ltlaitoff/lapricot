import { NOT_SYNC_ID_TAG } from '../../seance.config'

export const generateNotSyncId = () => {
	return `${NOT_SYNC_ID_TAG}-${new Date(Date.now()).getTime()}`
}
