import { createActionGroup, props } from '@ngrx/store'
import { UpdateSeance } from '@lapricot/shared/types'
import { NotSyncStatus } from '../../store.types'
import { SeanceNotSyncState, SeanceNotSyncStateItem } from '../seance.types'

export const SeanceNotSyncActions = createActionGroup({
	source: 'Seance not sync',
	events: {
		set: props<{ data: SeanceNotSyncState }>(),
		add: props<SeanceNotSyncStateItem>(),
		changeStatus: props<{
			status: NotSyncStatus
			data: SeanceNotSyncStateItem
		}>(),
		delete: props<SeanceNotSyncStateItem>(),
		update: props<{
			old: SeanceNotSyncStateItem
			dataForUpdate: UpdateSeance
		}>()
	}
})
