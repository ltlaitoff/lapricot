import { createReducer, on } from '@ngrx/store'
import { SeanceNotSyncState } from '../seance.types'
import { SeanceNotSyncActions } from './seance-not-sync.actions'

export const initialState: SeanceNotSyncState = []

export const seanceNotSyncReducer = createReducer(
	initialState,

	on(SeanceNotSyncActions.add, (state, payload) => {
		return [...state, payload]
	}),

	on(SeanceNotSyncActions.changeStatus, (state, { status, data }) => {
		return [
			...state.map(item => {
				if (item._id === data._id) {
					return {
						...item,
						status: status
					}
				}

				return item
			})
		]
	}),
	on(SeanceNotSyncActions.delete, (state, payload) => {
		return [...state.filter(item => item._id !== payload._id)]
	})
)
