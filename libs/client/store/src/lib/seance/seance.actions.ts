import { createActionGroup, props } from '@ngrx/store'
import { CreateSeance, UpdateSeance } from '@lapricot/shared/types'
import {
	SeanceStateItemWithCategory,
	SeanceSyncStateItem
} from './seance.types'

export const SeanceActions = createActionGroup({
	source: 'Seance',
	events: {
		load: (props: { force: boolean } = { force: false }) => props,

		add: (data: CreateSeance) => data,
		delete: (data: SeanceStateItemWithCategory) => data,
		deletePermanent: (data: SeanceStateItemWithCategory) => data,
		restore: (data: SeanceStateItemWithCategory) => data,
		update: props<{
			old: SeanceStateItemWithCategory
			dataForUpdate: UpdateSeance
		}>(),
		reorder: props<{
			data: SeanceStateItemWithCategory
			previousIndex: number
			currentIndex: number
		}>(),
		archiveToggle: (data: SeanceStateItemWithCategory) => data
	}
})
