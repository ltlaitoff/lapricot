import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { select, Store } from '@ngrx/store'
import { EMPTY, merge, of } from 'rxjs'
import {
	switchMap,
	catchError,
	withLatestFrom,
	mergeMap,
	map,
	filter
} from 'rxjs/operators'
import { ApiService } from '@lapricot/client-lib/data-access'
import { SeanceActions } from './seance.actions'
import {
	SeanceStateItem,
	SeanceNotSyncStateItem,
	SeanceSyncStateItem
} from './seance.types'
import { SeanceNotSyncActions } from './not-sync/seance-not-sync.actions'
import { SeanceSyncActions } from './sync/seance-sync.actions'

import { RootState } from '../rootTypes'
import {
	changeAddSeanceValueToStoreItem,
	changeUpdateSeanceValueToStoreItem,
	changeDeleteSeanceValueToStoreItem
} from './not-sync'
import { SeanceStatusActions } from './status'

import { NotSyncStatus } from '../store.types'
import { StatisticActions } from '../statistic'
import { UpdateSeance } from '@lapricot/shared/types'
import { seanceStateItemWithCategoryToDefault } from './helpers/seance-state-item-with-category-to-default.helper'

const defaultError$ = (notSyncSeance: SeanceNotSyncStateItem) => {
	return of(
		SeanceNotSyncActions.changeStatus({
			status: NotSyncStatus.ERROR,
			data: notSyncSeance
		}),
		SeanceStatusActions.setError()
	)
}

const filterBySeanceIsSync = (
	seance: SeanceStateItem
): seance is SeanceSyncStateItem => {
	return seance.status === undefined
}

const moveSeanceToNotSync$ = (notSyncSeance: SeanceNotSyncStateItem) => {
	return of(
		SeanceNotSyncActions.add(notSyncSeance),
		SeanceSyncActions.delete({
			id: notSyncSeance._id
		}),
		SeanceStatusActions.setSynchronization()
	)
}

@Injectable()
export class SeanceEffects {
	add$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.add),
			map(data => changeAddSeanceValueToStoreItem(data)),
			mergeMap((data: SeanceNotSyncStateItem) =>
				merge(
					of(
						SeanceNotSyncActions.add(data),
						SeanceStatusActions.setSynchronization()
					),

					this.api.addSeance$(data).pipe(
						switchMap(resultSeance => [
							SeanceSyncActions.add({
								data: resultSeance
							}),

							SeanceStatusActions.setSynchronized(),

							SeanceNotSyncActions.delete(data)
						]),
						catchError(() => defaultError$(data))
					)
				)
			)
		)
	)

	update$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.update),
			map(data => ({
				old: seanceStateItemWithCategoryToDefault(data.old),
				dataForUpdate: data.dataForUpdate
			})),
			filter(function (data: {
				old: SeanceStateItem
				dataForUpdate: UpdateSeance
			}): data is {
				old: SeanceSyncStateItem
				dataForUpdate: UpdateSeance
			} {
				return data.old.status === undefined
			}),
			map(({ old, dataForUpdate }) =>
				changeUpdateSeanceValueToStoreItem(old, dataForUpdate)
			),
			mergeMap(notSyncSeance =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),

					this.api.updateSeance$(notSyncSeance._id, notSyncSeance).pipe(
						switchMap(resultSeance =>
							of(
								SeanceSyncActions.add({
									data: resultSeance
								}),
								SeanceNotSyncActions.delete(notSyncSeance),
								SeanceStatusActions.setSynchronized()
							)
						),
						catchError(() => defaultError$(notSyncSeance))
					)
				)
			)
		)
	)

	restore$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.restore),
			map(seanceStateItemWithCategoryToDefault),
			filter(filterBySeanceIsSync),
			map(seance => changeUpdateSeanceValueToStoreItem(seance)),
			mergeMap(notSyncSeance =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),

					this.api.restoreSeance$(notSyncSeance._id).pipe(
						switchMap(resultSeance =>
							of(
								SeanceSyncActions.add({
									data: resultSeance
								}),

								SeanceStatusActions.setSynchronized(),

								SeanceNotSyncActions.delete(notSyncSeance),
								StatisticActions.load({ force: true })
							)
						),
						catchError(() => defaultError$(notSyncSeance))
					)
				)
			)
		)
	)

	reorder$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.reorder),
			map(data => {
				return {
					...data,
					data: seanceStateItemWithCategoryToDefault(data.data)
				}
			}),
			filter(function (data: {
				data: SeanceStateItem
				previousIndex: number
				currentIndex: number
			}): data is {
				data: SeanceSyncStateItem
				previousIndex: number
				currentIndex: number
			} {
				return data.data.status === undefined
			}),
			map(({ data, previousIndex, currentIndex }) => ({
				syncSeance: data,
				notSyncSeance: {
					...changeUpdateSeanceValueToStoreItem(data, {}),
					order: previousIndex < currentIndex ? currentIndex + 1 : currentIndex
				},
				previousIndex,
				currentIndex
			})),
			mergeMap(({ syncSeance, notSyncSeance, previousIndex, currentIndex }) =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),
					this.api
						.reorderSeance$({
							id: notSyncSeance._id,
							previousIndex,
							currentIndex
						})
						.pipe(
							switchMap(resultValue =>
								of(
									...resultValue.map(value => {
										if (value.id === notSyncSeance._id) {
											return SeanceSyncActions.add({
												data: {
													...syncSeance,
													order: value.currentIndex
												}
											})
										}

										return SeanceSyncActions.orderUpdate({
											data: value
										})
									}),
									SeanceStatusActions.setSynchronized(),
									SeanceNotSyncActions.delete(notSyncSeance)
								)
							),
							catchError(() => defaultError$(notSyncSeance))
						)
				)
			)
		)
	)

	delete$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.delete),
			map(seanceStateItemWithCategoryToDefault),
			filter(filterBySeanceIsSync),
			map(seance => changeDeleteSeanceValueToStoreItem(seance)),
			mergeMap(notSyncSeance =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),

					this.api.deleteSeance$(notSyncSeance._id).pipe(
						switchMap(resultSeance => [
							SeanceSyncActions.add({
								data: resultSeance
							}),

							SeanceStatusActions.setSynchronized(),
							SeanceNotSyncActions.delete(notSyncSeance),
							StatisticActions.load({ force: true })
						]),
						catchError(() => defaultError$(notSyncSeance))
					)
				)
			)
		)
	)

	deletePermanent$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.deletePermanent),
			map(seanceStateItemWithCategoryToDefault),
			filter(filterBySeanceIsSync),
			map(seance => changeDeleteSeanceValueToStoreItem(seance)),
			mergeMap(notSyncSeance =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),

					this.api.deleteSeancePermanent$(notSyncSeance._id).pipe(
						switchMap(() =>
							of(
								SeanceStatusActions.setSynchronized(),
								SeanceNotSyncActions.delete(notSyncSeance),
								StatisticActions.load({ force: true })
							)
						),
						catchError(() => defaultError$(notSyncSeance))
					)
				)
			)
		)
	)

	archiveToggle$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.archiveToggle),
			map(seanceStateItemWithCategoryToDefault),
			filter(filterBySeanceIsSync),
			map(seance => changeDeleteSeanceValueToStoreItem(seance)),
			mergeMap(notSyncSeance =>
				merge(
					moveSeanceToNotSync$(notSyncSeance),

					this.api.archiveSeance$(notSyncSeance._id).pipe(
						switchMap(resultSeance =>
							of(
								SeanceSyncActions.add({
									data: resultSeance
								}),

								SeanceStatusActions.setSynchronized(),

								SeanceNotSyncActions.delete(notSyncSeance)
							)
						),
						catchError(() => defaultError$(notSyncSeance))
					)
				)
			)
		)
	)

	loadSeance$ = createEffect(() =>
		this.actions$.pipe(
			ofType(SeanceActions.load),
			withLatestFrom(this.store.pipe(select('seance'))),
			mergeMap(([params, seanceValues]) => {
				if (seanceValues.length === 0 || params.force) {
					return merge(
						of(SeanceStatusActions.setSynchronization()),

						this.api.getAllSeances$().pipe(
							mergeMap(value => [
								SeanceSyncActions.set({ data: value }),
								SeanceStatusActions.setSynchronized()
							]),
							catchError(() => of(SeanceStatusActions.setError()))
						)
					)
				}

				return EMPTY
			})
		)
	)

	constructor(
		private actions$: Actions,
		private api: ApiService,
		private store: Store<RootState>
	) {}
}
