import { RootState } from '../rootTypes'
import {
	SeanceNotSyncState,
	SeanceNotSyncStateItem,
	SeanceNotSyncStateItemWithCategory,
	SeanceStateItemWithCategory,
	SeanceSyncState
} from './seance.types'
import { filterByTrash } from '../helpers/filter-by-trash.helper'
import { LoadStatus } from '../store.types'
import {
	CategoryStateItem,
	selectCategories,
	selectCategoriesAll
} from '../categories'
import { changeSeanceCategoryIdToCategoryObject } from './helpers/change-seance-category-id-to-category-object.helper'
import { createSelector } from '@ngrx/store'
import { SeanceActiveState } from './active/seance-active.intefaces'

const selectNotSyncSeance = (state: RootState) => {
	return state.notSyncSeance
}

const selectSyncSeance = (state: RootState) => {
	return state.seance
}

export const selectNotSyncSeanceWithCategory = createSelector(
	selectNotSyncSeance,
	selectCategories,
	(
		notSyncSeance: SeanceNotSyncState,
		categories: CategoryStateItem[]
	): SeanceNotSyncStateItemWithCategory[] => {
		return changeSeanceCategoryIdToCategoryObject<SeanceNotSyncStateItem>(
			notSyncSeance,
			categories
		) as SeanceNotSyncStateItemWithCategory[]
	}
)

export const selectSeance = createSelector(
	selectSyncSeance,
	selectNotSyncSeance,
	selectCategoriesAll,
	(
		syncSeance: SeanceSyncState,
		notSyncSeance: SeanceNotSyncState,
		categories: CategoryStateItem[]
	): SeanceStateItemWithCategory[] => {
		return changeSeanceCategoryIdToCategoryObject(
			filterByTrash([...syncSeance, ...notSyncSeance]),
			categories
		)
	}
)

export const selectSeanceTrashed = createSelector(
	selectSyncSeance,
	selectNotSyncSeance,
	selectCategoriesAll,
	(
		syncSeance: SeanceSyncState,
		notSyncSeance: SeanceNotSyncState,
		categories: CategoryStateItem[]
	): SeanceStateItemWithCategory[] => {
		return changeSeanceCategoryIdToCategoryObject(
			filterByTrash([...syncSeance, ...notSyncSeance], true),
			categories
		)
	}
)

export const selectSeanceStatus = (state: RootState): LoadStatus => {
	return state.seanceStatus
}

export const selectSeanceActive = (state: RootState): SeanceActiveState => {
	return state.seanceActive
}
