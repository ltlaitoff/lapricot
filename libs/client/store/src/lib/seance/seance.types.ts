import { Category, Seance } from '@lapricot/shared/types'
import { NotSyncStateItemBase, SyncStateItemBase } from '../store.types'

type ArrayElement<ArrayType extends readonly unknown[]> =
	ArrayType extends readonly (infer ElementType)[] ? ElementType : never

export type SeanceItemToWithCategory<
	T extends { records: { category: string }[] }
> = Omit<T, 'records'> & {
	records: (Omit<ArrayElement<T['records']>, 'category'> & {
		category: Category
	})[]
}

export type SeanceNotSyncStateItem = NotSyncStateItemBase & Seance
export type SeanceNotSyncState = SeanceNotSyncStateItem[]
export type SeanceNotSyncStateItemWithCategory =
	SeanceItemToWithCategory<SeanceNotSyncStateItem>

export type SeanceSyncStateItem = SyncStateItemBase & Seance
export type SeanceSyncState = SeanceSyncStateItem[]
export type SeanceSyncStateItemWithCategory =
	SeanceItemToWithCategory<SeanceSyncStateItem>

export type SeanceStateItem = SeanceNotSyncStateItem | SeanceSyncStateItem
export type SeanceStateItemWithCategory =
	| SeanceNotSyncStateItemWithCategory
	| SeanceSyncStateItemWithCategory
