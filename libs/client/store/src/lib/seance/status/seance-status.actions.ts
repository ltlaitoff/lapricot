import { createActionGroup, emptyProps } from '@ngrx/store'

export const SeanceStatusActions = createActionGroup({
	source: 'Seance status',
	events: {
		setNotSynchronized: emptyProps(),
		setSynchronization: emptyProps(),
		setError: emptyProps(),
		setSynchronized: emptyProps()
	}
})
