import { createReducer, on } from '@ngrx/store'
import { SeanceStatusActions } from './seance-status.actions'
import { LoadStatus } from '../../store.types'

export const initialState: LoadStatus =
	LoadStatus.NOT_SYNCHRONIZED as LoadStatus

export const seanceStatusReducer = createReducer(
	initialState,

	on(SeanceStatusActions.setNotSynchronized, () => LoadStatus.NOT_SYNCHRONIZED),
	on(SeanceStatusActions.setSynchronization, () => LoadStatus.SYNCHRONIZATION),
	on(SeanceStatusActions.setError, () => LoadStatus.ERROR),
	on(SeanceStatusActions.setSynchronized, () => LoadStatus.SYNCHRONIZED)
)
