import { props, createActionGroup } from '@ngrx/store'
import { ReorderSeanceData } from '@lapricot/shared/types'
import { SeanceSyncState, SeanceSyncStateItem } from '../seance.types'

export const SeanceSyncActions = createActionGroup({
	source: 'Seance sync',
	events: {
		set: props<{ data: SeanceSyncState }>(),
		add: props<{ data: SeanceSyncStateItem }>(),
		delete: props<{ id: string }>(),
		update: props<{ data: SeanceSyncStateItem }>(),
		orderUpdate: props<{ data: ReorderSeanceData }>()
	}
})
