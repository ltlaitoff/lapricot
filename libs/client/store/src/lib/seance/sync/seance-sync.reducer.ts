import { createReducer, on } from '@ngrx/store'
import { SeanceSyncActions } from './seance-sync.actions'
import { SeanceSyncState } from '../seance.types'

export const initialState: SeanceSyncState = []

export const seanceReducer = createReducer(
	initialState,
	on(SeanceSyncActions.set, (state, { data }) => [...data]),
	on(SeanceSyncActions.add, (state, { data }) => [...state, data]),
	on(SeanceSyncActions.delete, (state, data) => {
		return [...state.filter(item => item._id !== data.id)]
	}),
	on(SeanceSyncActions.orderUpdate, (state, { data }) => {
		return [
			...state.map(item => {
				if (item._id === data.id) {
					return {
						...item,
						order: data.currentIndex
					}
				}

				return item
			})
		]
	})
)
