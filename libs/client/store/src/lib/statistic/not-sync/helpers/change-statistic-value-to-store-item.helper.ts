import { generateNotSyncStatisticId } from './generate-not-sync-statistic-id.helper'
import {
	CreateStatisticDate,
	UpdateStatisticData
} from '@lapricot/shared/types'
import {
	StatisticNotSyncStateItem,
	StatisticSyncStateItem
} from '../../statistic.types'
import { NotSyncStatus, NotSyncAction } from '../../../store.types'

export function changeAddStatisticValueToStoreItem(
	data: CreateStatisticDate
): StatisticNotSyncStateItem {
	const forAdd = {
		...data,
		_id: generateNotSyncStatisticId(),
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.CREATED,
		trash: false,
		trash_expires: -1
	}

	return forAdd
}

export function changeDeleteStatisticValueToStoreItem(
	data: StatisticSyncStateItem
): StatisticNotSyncStateItem {
	return {
		...data,
		date: new Date(data.date).toISOString(),
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.DELETED
	}
}

export function changeUpdateStatisticValueToStoreItem(
	data: StatisticSyncStateItem,
	newData: UpdateStatisticData
): StatisticNotSyncStateItem {
	return {
		...data,
		status: NotSyncStatus.SYNCHRONIZATION,
		action: NotSyncAction.DELETED,
		...newData
	}
}
