import { createActionGroup, props } from '@ngrx/store'
import { UpdateStatisticData } from '@lapricot/shared/types'
import { NotSyncStatus } from '../../store.types'
import {
	StatisticNotSyncState,
	StatisticNotSyncStateItem
} from '../statistic.types'

export const StatisticNotSyncActions = createActionGroup({
	source: 'Statistic not sync',
	events: {
		set: props<{ statistic: StatisticNotSyncState }>(),
		add: props<StatisticNotSyncStateItem>(),
		changeStatus: props<{
			status: NotSyncStatus
			statistic: StatisticNotSyncStateItem
		}>(),
		delete: props<StatisticNotSyncStateItem>(),
		update: props<{
			oldStatistic: StatisticNotSyncStateItem
			dataForUpdate: UpdateStatisticData
		}>()
	}
})
