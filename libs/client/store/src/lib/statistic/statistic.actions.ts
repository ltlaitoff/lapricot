import { createActionGroup, props } from '@ngrx/store'
import {
	CreateStatisticDate,
	UpdateStatisticData
} from '@lapricot/shared/types'
import { StatisticStateItemWithCategory } from './statistic.types'

export const StatisticActions = createActionGroup({
	source: 'Stastistic',
	events: {
		load: (props: { force: boolean } = { force: false }) => props,
		add: (statistic: CreateStatisticDate) => statistic,
		delete: (statistic: StatisticStateItemWithCategory) => statistic,
		deletePermanent: (statistic: StatisticStateItemWithCategory) => statistic,
		update: props<{
			oldStatistic: StatisticStateItemWithCategory
			dataForUpdate: UpdateStatisticData
		}>()
	}
})
