import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { select, Store } from '@ngrx/store'
import { EMPTY, merge, of } from 'rxjs'
import {
	catchError,
	withLatestFrom,
	mergeMap,
	switchMap,
	map,
	filter
} from 'rxjs/operators'
import { ApiService } from '@lapricot/client-lib/data-access'
import { StatisticActions } from '.'
import { StatisticNotSyncActions } from './not-sync/statistic-not-sync.actions'
import { StatisticSyncActions } from './sync/statistic-sync.actions'

import { RootState } from '../rootTypes'
import { NotSyncHelpers } from './not-sync'
import { StatisticStatusActions } from './status'
import {
	StatisticNotSyncStateItem,
	StatisticStateItem,
	StatisticSyncStateItem
} from './statistic.types'
import { NotSyncStatus } from '../store.types'
import { statisticStateItemWithCategoryToDefault } from './helpers/statistic-state-item-with-category-to-default.helper'
import { UpdateStatisticData } from '@lapricot/shared/types'

const standartCathError$ = (statistic: StatisticNotSyncStateItem) =>
	of(
		StatisticNotSyncActions.changeStatus({
			status: NotSyncStatus.ERROR,
			statistic: statistic
		}),
		StatisticStatusActions.setError()
	)

@Injectable()
export class StatisticEffects {
	loadStatistic$ = createEffect(() =>
		this.actions$.pipe(
			ofType(StatisticActions.load),
			withLatestFrom(this.store.pipe(select('statistic'))),
			mergeMap(([params, statisticValue]) => {
				if (statisticValue.length === 0 || params.force) {
					return merge(
						of(StatisticStatusActions.setSynchronization()),

						this.api.getAllStatisticRecords().pipe(
							mergeMap(value =>
								of(
									StatisticStatusActions.setSynchronized(),
									StatisticSyncActions.set({ statistic: value })
								)
							),
							catchError(() => of(StatisticStatusActions.setError()))
						)
					)
				}

				return EMPTY
			})
		)
	)

	add$ = createEffect(() =>
		this.actions$.pipe(
			ofType(StatisticActions.add),
			map(statistic =>
				NotSyncHelpers.changeAddStatisticValueToStoreItem(statistic)
			),
			withLatestFrom(this.store.pipe(select('statistic'))),
			mergeMap(([notSyncStatistic, storeStatisticData]) =>
				merge(
					of(
						StatisticNotSyncActions.add(notSyncStatistic),
						StatisticStatusActions.setSynchronization()
					),

					this.api.addStatisticRecord(notSyncStatistic).pipe(
						switchMap(resultStatistic => {
							if (storeStatisticData.length === 0) {
								this.store.dispatch(StatisticActions.load())
							}

							return of(
								StatisticSyncActions.add({
									statistic: resultStatistic
								}),
								StatisticStatusActions.setSynchronized(),
								StatisticNotSyncActions.delete(notSyncStatistic)
							)
						}),

						catchError(() => standartCathError$(notSyncStatistic))
					)
				)
			)
		)
	)

	update$ = createEffect(() =>
		this.actions$.pipe(
			ofType(StatisticActions.update),
			map(statistic => ({
				...statistic,
				oldStatistic: statisticStateItemWithCategoryToDefault(
					statistic.oldStatistic
				)
			})),
			filter(
				(statistic: {
					oldStatistic: StatisticStateItem
					dataForUpdate: UpdateStatisticData
				}): statistic is {
					oldStatistic: StatisticSyncStateItem
					dataForUpdate: UpdateStatisticData
				} => {
					return statistic.oldStatistic.status === undefined
				}
			),
			map(({ oldStatistic, dataForUpdate }) =>
				NotSyncHelpers.changeUpdateStatisticValueToStoreItem(
					oldStatistic,
					dataForUpdate
				)
			),
			mergeMap(statistic =>
				merge(
					of(
						StatisticNotSyncActions.add(statistic),
						StatisticSyncActions.delete({
							id: statistic._id
						}),
						StatisticStatusActions.setSynchronization()
					),

					this.api.updateStatistic(statistic._id, statistic).pipe(
						switchMap(resultStatistic =>
							of(
								StatisticSyncActions.add({
									statistic: resultStatistic
								}),
								StatisticStatusActions.setSynchronized(),
								StatisticNotSyncActions.delete(statistic)
							)
						),
						catchError(() => standartCathError$(statistic))
					)
				)
			)
		)
	)

	delete$ = createEffect(() =>
		this.actions$.pipe(
			ofType(StatisticActions.delete),
			map(statistic => statisticStateItemWithCategoryToDefault(statistic)),
			filter((statistic): statistic is StatisticSyncStateItem => {
				return statistic.status === undefined
			}),
			map(statistic =>
				NotSyncHelpers.changeDeleteStatisticValueToStoreItem(statistic)
			),
			mergeMap(statistic =>
				merge(
					of(
						StatisticSyncActions.delete({
							id: statistic._id
						}),
						StatisticNotSyncActions.add(statistic),
						StatisticStatusActions.setSynchronization()
					),

					this.api.deleteStatistic(statistic._id).pipe(
						switchMap(resultStatistic =>
							of(
								StatisticSyncActions.add({
									statistic: resultStatistic
								}),
								StatisticStatusActions.setSynchronized(),
								StatisticNotSyncActions.delete(statistic)
							)
						),
						catchError(() => standartCathError$(statistic))
					)
				)
			)
		)
	)

	deletePermanent$ = createEffect(() =>
		this.actions$.pipe(
			ofType(StatisticActions.deletePermanent),
			map(statistic => statisticStateItemWithCategoryToDefault(statistic)),
			filter((statistic): statistic is StatisticSyncStateItem => {
				return statistic.status === undefined
			}),
			map(statistic =>
				NotSyncHelpers.changeDeleteStatisticValueToStoreItem(statistic)
			),
			mergeMap(statistic =>
				merge(
					of(
						StatisticSyncActions.delete({
							id: statistic._id
						}),
						StatisticNotSyncActions.add(statistic),
						StatisticStatusActions.setSynchronization()
					),

					this.api.deleteStatisticPermanent(statistic._id).pipe(
						switchMap(() =>
							of(
								StatisticStatusActions.setSynchronized(),
								StatisticNotSyncActions.delete(statistic)
							)
						),
						catchError(() => standartCathError$(statistic))
					)
				)
			)
		)
	)

	constructor(
		private actions$: Actions,
		private api: ApiService,
		private store: Store<RootState>
	) {}
}
