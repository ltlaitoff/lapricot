import { RootState } from '../rootTypes'
import {
	StatisticNotSyncState,
	StatisticStateItemWithCategory,
	StatisticSyncState
} from './statistic.types'
import { StatisticStatusTypes } from './status'
import { changeStatisticCategoryIdToCategoryObject } from './helpers/change-statistic-category-id-to-category-object.helper'
import { createSelector } from '@ngrx/store'

import {
	selectCategories,
	selectCategoriesAll
} from '../categories/categories.select'
import { CategoryStateItem } from '../categories/categories.types'
import {
	StatisticNotSyncStateItemWithCategory,
	StatisticNotSyncStateItem
} from './statistic.types'
import { filterByTrash } from '../helpers/filter-by-trash.helper'

const selectNotSyncStatistic = (state: RootState) => {
	return state.notSyncStatistic
}

const selectSyncStatistic = (state: RootState) => {
	return state.statistic
}

export const selectNotSyncStatisticWithCategory = createSelector(
	selectNotSyncStatistic,
	selectCategories,
	(
		notSyncStatistic: StatisticNotSyncState,
		categories: CategoryStateItem[]
	): StatisticNotSyncStateItemWithCategory[] => {
		return changeStatisticCategoryIdToCategoryObject<StatisticNotSyncStateItem>(
			notSyncStatistic,
			categories
		) as StatisticNotSyncStateItemWithCategory[]
	}
)

export const selectStatistic = createSelector(
	selectSyncStatistic,
	selectNotSyncStatistic,
	selectCategories,
	(
		syncStatistic: StatisticSyncState,
		notSyncStatistic: StatisticNotSyncState,
		categories: CategoryStateItem[]
	): StatisticStateItemWithCategory[] => {
		return changeStatisticCategoryIdToCategoryObject(
			filterByTrash([...syncStatistic, ...notSyncStatistic]),
			categories
		)
	}
)

export const selectStatisticTrashed = createSelector(
	selectSyncStatistic,
	selectNotSyncStatistic,
	selectCategoriesAll,
	(
		syncStatistic: StatisticSyncState,
		notSyncStatistic: StatisticNotSyncState,
		categories: CategoryStateItem[]
	): StatisticStateItemWithCategory[] => {
		return changeStatisticCategoryIdToCategoryObject(
			filterByTrash([...syncStatistic, ...notSyncStatistic], true),
			categories
		)
	}
)

export const selectStatisticStatus = (
	state: RootState
): StatisticStatusTypes.StatusState => {
	return state.statisticStatus
}
