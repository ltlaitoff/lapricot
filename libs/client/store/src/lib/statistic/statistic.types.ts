import { Statistic } from '@lapricot/shared/types'
import { NotSyncStateItemBase, SyncStateItemBase } from '../store.types'
import { CategoryStateItem } from '../categories/categories.types'

export type StatisticItemToWithCategory<T extends { category: string }> = Omit<
	T,
	'category'
> & {
	category: CategoryStateItem
}

export type StatisticNotSyncStateItem = NotSyncStateItemBase & Statistic
export type StatisticNotSyncState = StatisticNotSyncStateItem[]
export type StatisticNotSyncStateItemWithCategory =
	StatisticItemToWithCategory<StatisticNotSyncStateItem>

export type StatisticSyncStateItem = SyncStateItemBase & Statistic
export type StatisticSyncState = StatisticSyncStateItem[]
export type StatisticSyncStateItemWithCategory =
	StatisticItemToWithCategory<StatisticSyncStateItem>

export type StatisticStateItem =
	| StatisticNotSyncStateItem
	| StatisticSyncStateItem
export type StatisticStateItemWithCategory =
	| StatisticNotSyncStateItemWithCategory
	| StatisticSyncStateItemWithCategory
