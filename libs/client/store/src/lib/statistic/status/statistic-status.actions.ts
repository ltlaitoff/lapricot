import { createActionGroup, emptyProps, props } from '@ngrx/store'
import { StatisticStatusTypes } from '.'

export const StatisticStatusActions = createActionGroup({
	source: 'Statistic status',
	events: {
		set: props<{ status: StatisticStatusTypes.StatusState }>(),
		setNotSynchronized: emptyProps(),
		setSynchronization: emptyProps(),
		setError: emptyProps(),
		setSynchronized: emptyProps()
	}
})
