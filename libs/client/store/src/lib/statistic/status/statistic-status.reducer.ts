import { createReducer, on } from '@ngrx/store'
import { StatisticStatusActions } from './statistic-status.actions'
import { StatisticStatusTypes } from '.'

export const initialState: StatisticStatusTypes.StatusState =
	StatisticStatusTypes.StatusState
		.NOT_SYNCHRONIZED as StatisticStatusTypes.StatusState

export const statisticStatusReducer = createReducer(
	initialState,

	on(StatisticStatusActions.set, (state, payload) => {
		return payload.status
	}),
	on(StatisticStatusActions.setNotSynchronized, () => {
		return StatisticStatusTypes.StatusState.NOT_SYNCHRONIZED
	}),
	on(StatisticStatusActions.setSynchronization, () => {
		return StatisticStatusTypes.StatusState.SYNCHRONIZATION
	}),
	on(StatisticStatusActions.setError, () => {
		return StatisticStatusTypes.StatusState.ERROR
	}),
	on(StatisticStatusActions.setSynchronized, () => {
		return StatisticStatusTypes.StatusState.SYNCHRONIZED
	})
)
