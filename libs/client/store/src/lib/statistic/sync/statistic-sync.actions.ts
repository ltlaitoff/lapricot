import { props, createActionGroup } from '@ngrx/store'
import { StatisticSyncState, StatisticSyncStateItem } from '../statistic.types'

export const StatisticSyncActions = createActionGroup({
	source: 'Statistic sync',
	events: {
		set: props<{ statistic: StatisticSyncState }>(),
		add: props<{ statistic: StatisticSyncStateItem }>(),
		delete: props<{ id: string }>(),
		update: props<{ category: StatisticSyncStateItem }>()
	}
})
