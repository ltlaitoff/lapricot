import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'

import { AuthorizationComponent } from './authorization.component'

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [AuthorizationComponent],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(AuthorizationComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return { fixture, component }
}

describe('AuthorizationComponent', () => {
	it('Component la-authorization should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-authorization h2 with text = "Login" should be', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(By.css('h2'))

		expect(element).toBeTruthy()
		expect(element.nativeElement.textContent.trim()).toBe('Login')
	})

	it('In la-authorization asl-google-signin-button should be', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(
			By.css('asl-google-signin-button')
		)

		expect(element).toBeTruthy()
	})
})
