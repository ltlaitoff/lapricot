import { Component } from '@angular/core'

import { inject } from '@angular/core'
import { APP_CONFIG } from '@lapricot/client-lib/config'
import { ClientEnvironment } from '@lapricot/shared/types'

import {
	SocialLoginModule,
	SocialAuthServiceConfig,
	GoogleLoginProvider
} from '@abacritt/angularx-social-login'
import { CommonModule } from '@angular/common'

@Component({
	selector: 'la-authorization',
	imports: [CommonModule, SocialLoginModule],
	providers: [
		{
			provide: 'SocialAuthServiceConfig',
			useFactory: () => {
				const ENV = inject(APP_CONFIG) as ClientEnvironment

				return {
					autoLogin: false,
					providers: [
						{
							id: GoogleLoginProvider.PROVIDER_ID,
							provider: new GoogleLoginProvider(ENV.googleClientId, {
								oneTapEnabled: false
							})
						}
					],
					onError: err => {
						console.error(err)
					}
				} as SocialAuthServiceConfig
			}
		}
	],
	templateUrl: './authorization.component.html',
	standalone: true
})
export class AuthorizationComponent {}
