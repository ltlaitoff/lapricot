import {
	Component,
	OnInit,
	EventEmitter,
	Output,
	Input,
	OnDestroy
} from '@angular/core'
import { CommonModule } from '@angular/common'
import {
	FormGroup,
	FormControl,
	Validators,
	ReactiveFormsModule
} from '@angular/forms'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { Subscription } from 'rxjs'
import { ColorSelectComponent } from '@lapricot/ui'

@Component({
	selector: 'la-categories-form',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule, ColorSelectComponent],
	templateUrl: './categories-form.component.html',
	styleUrls: ['./categories-form.component.scss']
})
export class CategoriesFormComponent implements OnInit, OnDestroy {
	@Input() initialFormData: {
		name: string
		comment: string
		color: string
		dimension: string
		mode?: string
	} | null = null
	@Input() fromType: 'add' | 'edit' = 'add'

	@Output() formSubmit = new EventEmitter<CategoriesBasicSet>()

	errorMessage: string | null = null

	formData = new FormGroup({
		name: new FormControl<string>(this.initialFormData?.name || '', [
			Validators.required,
			Validators.maxLength(60)
		]),
		comment: new FormControl<string>(
			this.initialFormData?.comment || '',
			Validators.maxLength(100)
		),
		dimension: new FormControl<string>(
			this.initialFormData?.dimension || '',
			Validators.maxLength(30)
		),
		mode: new FormControl<string>(this.initialFormData?.mode || 'number', [
			Validators.required
		]),
		color: new FormControl<string | null>(this.initialFormData?.color || null, [
			Validators.required
		])
	})

	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		if (this.initialFormData) {
			this.formData.setValue({
				...this.initialFormData,
				mode: this.initialFormData?.mode || 'number'
			})
		}

		this.formDataValueChangesSubscription =
			this.formData.valueChanges.subscribe(() => {
				this.errorMessage = this.checkFormDataOnErrors()
			})
	}

	ngOnDestroy() {
		this.formDataValueChangesSubscription.unsubscribe()
	}

	checkFormDataOnErrors() {
		if (this.formData.valid) {
			return null
		}

		const name = this.formData.get('name')

		if (name && name.invalid && (name.dirty || name.touched)) {
			if (name.errors?.['required']) {
				return 'Name is a required field'
			}

			if (name.errors?.['maxlength']) {
				return 'Name must be no more than 60 characters long'
			}
		}

		const comment = this.formData.get('comment')

		if (comment && comment.invalid && (comment.dirty || comment.touched)) {
			if (comment.errors?.['maxlength']) {
				return 'Comment must be no more than 100 characters long'
			}
		}

		const dimension = this.formData.get('dimension')

		if (
			dimension &&
			dimension.invalid &&
			(dimension.dirty || dimension.touched)
		) {
			if (dimension.errors?.['maxlength']) {
				return 'Dimension must be no more than 30 characters long'
			}
		}

		return null
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			}
		})
	}

	onSubmitAddForm() {
		if (this.formData.invalid) {
			this.validateAllFormFields(this.formData)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		const valueForSend = this.prepareSubmitData(this.formData.value)

		if (valueForSend == null) {
			return
		}

		this.formSubmit.emit(valueForSend)

		this.formData.reset({
			name: '',
			comment: '',
			color: null,
			mode: this.formData.value.mode || 'number',
			dimension: ''
		})
	}

	private prepareSubmitData(value: typeof this.formData.value) {
		if (!value.name) {
			this.formData.get('name')?.setErrors({ required: 'required' })
		}

		if (
			!value.name ||
			value.comment == null ||
			!value.color ||
			value.dimension == null ||
			!value.mode
		) {
			return null
		}

		const mode: 'number' | 'time' =
			value.mode !== 'number' && value.mode !== 'time' ? 'number' : value.mode

		const valueForSend = {
			name: value.name,
			comment: value.comment,
			color: value.color,
			dimension: value.dimension,
			group: [],
			mode: mode
		}

		return valueForSend
	}

	get formTypeIsAdd() {
		return this.fromType === 'add'
	}

	get formTypeIsEdit() {
		return this.fromType === 'edit'
	}

	get formName() {
		return this.formData.get('name')
	}
}
