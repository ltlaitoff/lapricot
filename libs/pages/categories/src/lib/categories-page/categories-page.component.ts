import { Component, OnDestroy, OnInit } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Store } from '@ngrx/store'

import {
	RootState,
	CategoriesActions,
	selectCategoriesStatus,
	LoadStatus
} from '@lapricot/client-lib/store'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { CategoriesFormComponent } from '../categories-form-add-new/categories-form.component'
import { LoadStatusButtonComponent } from '@lapricot/ui'
import { CategoriesTableComponent } from '../categories-table/categories-table.component'
import { FormsModule } from '@angular/forms'
import { FormCloseDirective } from '@lapricot/client-lib/directives'

/*
TODO [x]: View as table with orde
TODO [x]: Add new category
TODO [x]: Delete category (idk what do with statistic.. -.-)
			Возле поля таблицы должна появляться кнопка удаления(вообще это будет набор кнопок) при нажатии на которую всплывёт модалка где нужно будет подтвердить удаление
TODO [ ]: Change category info
			При клике на текстовое поле появится инпут на его месте в который можно будет вписать новое значение, после закрытия инпута запрос в api на изменение
			При клике на цвет - на его месте появляется открытый select color
TODO [ ]: Drag-n-drop order
*/

@Component({
	selector: 'la-categories-page',
	standalone: true,
	imports: [
		CommonModule,
		FormCloseDirective,
		FormsModule,
		CategoriesFormComponent,
		LoadStatusButtonComponent,
		CategoriesTableComponent
	],
	templateUrl: './categories-page.component.html'
})
export class CategoriesPageComponent implements OnInit, OnDestroy {
	currentStatus: LoadStatus = LoadStatus.NOT_SYNCHRONIZED

	categoryTableType: 'normal' | 'archive' | 'all' = 'normal'

	public isAddFormOpened = false

	private selectCategoriesStatusSubscription: Subscription

	ngOnInit() {
		this.selectCategoriesStatusSubscription = this.store
			.select(selectCategoriesStatus)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.currentStatus = value
			})
	}

	ngOnDestroy() {
		this.selectCategoriesStatusSubscription.unsubscribe()
	}

	toggleAddForm() {
		this.isAddFormOpened = !this.isAddFormOpened
	}

	closeAddForm() {
		this.isAddFormOpened = false
	}

	closeWithCheck() {
		if (!this.isAddFormOpened) return

		this.closeAddForm()
	}

	reloadCategories(force: boolean) {
		this.store.dispatch(CategoriesActions.load({ force: force }))
	}

	addNewCategory(data: CategoriesBasicSet) {
		this.store.dispatch(CategoriesActions.add(data))
		this.closeWithCheck()
	}

	constructor(private store: Store<RootState>) {}
}
