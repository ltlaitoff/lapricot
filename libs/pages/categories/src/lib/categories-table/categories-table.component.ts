import { CdkDrag, CdkDragDrop, DragDropModule } from '@angular/cdk/drag-drop'
import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { Store } from '@ngrx/store'
import { sortedByOrder } from '../helpers/sorted-by-order.helper'
import {
	RootState,
	selectCategories,
	CategoriesActions,
	CategoryStateItem,
	CategorySyncStateItem,
	LoadStatus,
	selectCategoriesStatus,
	selectCategoryGroups,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'

import { distinctUntilChanged, Subscription } from 'rxjs'
import {
	CicleComponent,
	NotSyncStatusIconComponent,
	PanelFormComponent,
	PanelFormItemComponent,
	TableColorHeadItemComponent,
	TableControlButtonComponent
} from '@lapricot/ui'
import { CategoriesFormComponent } from '../categories-form-add-new/categories-form.component'
import { CategoryGroupsCellComponent } from '@lapricot/pages/category-groups'
import { CommonModule } from '@angular/common'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { FormCloseDirective } from '@lapricot/client-lib/directives'

@Component({
	selector: 'la-categories-table',
	standalone: true,
	imports: [
		CommonModule,
		DragDropModule,
		CdkDrag,
		AngularSvgIconModule,
		TableControlButtonComponent,
		FormCloseDirective,
		TableColorHeadItemComponent,
		PanelFormComponent,
		PanelFormItemComponent,
		CategoriesFormComponent,
		CicleComponent,
		NotSyncStatusIconComponent,
		CategoryGroupsCellComponent
	],
	templateUrl: './categories-table.component.html',
	styleUrls: ['./categories-table.component.scss']
})
export class CategoriesTableComponent implements OnInit, OnDestroy {
	@Input() type: 'normal' | 'archive' | 'all' = 'normal'

	categories: CategoryStateItem[] | null = null
	showMenu: string | null = null
	editCategoryId: string | null = null
	allCategoryGroups: CategoryGroupsStateItem[] = []
	currentStatus: LoadStatus = LoadStatus.NOT_SYNCHRONIZED

	private selectCategoriesSubscription: Subscription
	private selectCategoryGroupsSubscription: Subscription
	private selectCategoriesStatusSubscription: Subscription

	ngOnInit() {
		this.selectCategoriesSubscription = this.store
			.select(selectCategories)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				if (JSON.stringify(value) !== JSON.stringify(this.categories)) {
					this.categories = value
				}
			})

		this.selectCategoryGroupsSubscription = this.store
			.select(selectCategoryGroups)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				if (JSON.stringify(value) !== JSON.stringify(this.allCategoryGroups)) {
					this.allCategoryGroups = value
				}
			})

		this.selectCategoriesStatusSubscription = this.store
			.select(selectCategoriesStatus)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.currentStatus = value
			})
	}

	ngOnDestroy() {
		this.selectCategoriesSubscription.unsubscribe()
		this.selectCategoryGroupsSubscription.unsubscribe()
		this.selectCategoriesStatusSubscription.unsubscribe()
	}

	getCategoriesForSort() {
		if (this.categories === null) return this.categories

		if (this.type === 'all') return this.categories

		const currentCategoriesLocal = this.type !== 'normal'

		return this.categories.filter(item => {
			return item.archived === currentCategoriesLocal
		})
	}

	get sortedByOrderCategories() {
		const categoriesForSort = this.getCategoriesForSort()
		return sortedByOrder(categoriesForSort)
	}

	drop(
		event: CdkDragDrop<
			CategorySyncStateItem[],
			CategorySyncStateItem[],
			CategorySyncStateItem
		>
	) {
		if (this.sortedByOrderCategories === null) return
		if (event.previousIndex === event.currentIndex) return

		const categoryData = event.item.data
		const previousIndex = categoryData.order
		const currentIndex = this.sortedByOrderCategories[event.currentIndex].order

		this.store.dispatch(
			CategoriesActions.reorder({
				category: categoryData,
				previousIndex: previousIndex,
				currentIndex: currentIndex
			})
		)
	}

	onControlButtonClick(categoryId: string) {
		if (this.showMenu === null || this.showMenu !== categoryId) {
			this.showMenu = categoryId
			return
		}

		this.showMenu = null
	}

	onFormClickedOutside(categoryId: string) {
		if (this.showMenu !== categoryId) {
			return
		}

		this.showMenu = null
		this.editCategoryId = null
	}

	editCategory(
		currentValue: CategoryStateItem,
		editedValue: CategoriesBasicSet
	) {
		this.editCategoryId = null
		this.showMenu = null

		this.store.dispatch(
			CategoriesActions.update({
				oldCategory: currentValue,
				dataForUpdate: { ...editedValue, group: currentValue.group }
			})
		)
	}

	setEditCategoryId(newId: string) {
		if (this.editCategoryId === newId) {
			this.editCategoryId = null
			return
		}

		this.editCategoryId = newId
	}

	deleteCategory(category: CategoryStateItem) {
		this.store.dispatch(CategoriesActions.delete(category))
	}

	archiveCategory(category: CategoryStateItem) {
		this.store.dispatch(CategoriesActions.archiveToggle(category))
	}

	changeCategoryGroups(category: CategoryStateItem, categoryGroups: string[]) {
		this.store.dispatch(
			CategoriesActions.update({
				oldCategory: category,
				dataForUpdate: {
					...category,
					color: category.color,
					group: categoryGroups
				}
			})
		)
	}

	stopPropagation(event: Event) {
		event.stopPropagation()
	}

	constructor(private store: Store<RootState>) {}
}
