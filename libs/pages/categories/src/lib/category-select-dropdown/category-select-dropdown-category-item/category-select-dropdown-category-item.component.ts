import { Component, EventEmitter, Output, Input } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CategoryStateItem } from '@lapricot/client-lib/store'
import { CicleComponent } from '@lapricot/ui'
import { HighlightPipe } from '../highlight.pipe'

@Component({
	selector: 'la-category-select-dropdown-category-item',
	standalone: true,
	imports: [CommonModule, CicleComponent, HighlightPipe],
	templateUrl: './category-select-dropdown-category-item.component.html'
})
export class CategorySelectDropdownCategoryItemComponent {
	@Input() category: CategoryStateItem | null = null
	@Input() choiced = false
	@Input() multi = false
	@Input() searchValue = ''

	@Output() itemClick = new EventEmitter<string | null>()

	onItemClick(id: string | null) {
		this.itemClick.emit(id)
	}
}
