import { Component } from '@angular/core'

@Component({
	selector: 'la-category-select-dropdown-divider',
	templateUrl: './category-select-dropdown-divider.component.html',
	standalone: true
})
export class CategorySelectDropdownDividerComponent {}
