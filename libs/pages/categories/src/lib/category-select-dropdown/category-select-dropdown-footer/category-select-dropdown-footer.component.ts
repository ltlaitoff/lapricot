import { Component, EventEmitter, Input, Output } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Store } from '@ngrx/store'
import { RootState } from '@lapricot/client-lib/store'
import { CategoriesActions } from '@lapricot/client-lib/store'
import { CategoriesBasicSet } from '@lapricot/shared/types'
import { CategoriesFormComponent } from '../../categories-form-add-new/categories-form.component'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-category-select-dropdown-footer',
	standalone: true,
	imports: [CommonModule, AngularSvgIconModule, CategoriesFormComponent],
	templateUrl: './category-select-dropdown-footer.component.html'
})
export class CategorySelectDropdownFooterComponent {
	constructor(private store: Store<RootState>) {}

	@Input() isAddFormOpened = false
	@Output() isAddFormOpenedChange = new EventEmitter<boolean>()

	toggleAddForm(event: Event) {
		event.stopPropagation()

		this.isAddFormOpened = !this.isAddFormOpened

		this.isAddFormOpenedChange.emit(this.isAddFormOpened)
	}

	closeAddForm() {
		this.isAddFormOpened = false

		this.isAddFormOpenedChange.emit(this.isAddFormOpened)
	}

	addNewCategory(data: CategoriesBasicSet) {
		this.store.dispatch(CategoriesActions.add(data))
		this.closeAddForm()
	}

	stopPropagination(e: Event) {
		e.stopPropagation()
	}
}
