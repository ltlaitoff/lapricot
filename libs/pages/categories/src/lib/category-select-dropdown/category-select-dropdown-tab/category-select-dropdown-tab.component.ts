import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { CategorySelectTab } from '../category-select-dropdown.types'

@Component({
	selector: 'la-category-select-dropdown-tab',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './category-select-dropdown-tab.component.html',
	styleUrls: ['./category-select-dropdown-tab.component.scss']
})
export class CategorySelectDropdownTabComponent {
	@Input() tab: CategorySelectTab = 'category'
	@Input() active = false

	@Output() buttonClick = new EventEmitter<CategorySelectTab>()

	onClickInner() {
		this.buttonClick.emit(this.tab)
	}
}
