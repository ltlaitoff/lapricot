import {
	ChangeDetectionStrategy,
	Component,
	EventEmitter,
	Input,
	Output
} from '@angular/core'
import {
	CategoryStateItem,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { CategorySelectTab } from './category-select-dropdown.types'
import {
	TABS_DEFAULT,
	ACTIVE_TAB_DEFAULT
} from './category-select-dropdown.config'
import { CategorySelectDropdownDividerComponent } from './category-select-dropdown-divider/category-select-dropdown-divider.component'
import { CategorySelectDropdownTabComponent } from './category-select-dropdown-tab/category-select-dropdown-tab.component'
import { CategorySelectDropdownGroupTabComponent } from './tabs/category-select-dropdown-group-tab/category-select-dropdown-group-tab.component'
import { CategorySelectDropdownCategoryTabComponent } from './tabs/category-select-dropdown-category-tab/category-select-dropdown-category-tab.component'
import { CategorySelectDropdownFooterComponent } from './category-select-dropdown-footer/category-select-dropdown-footer.component'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

@Component({
	selector: 'la-category-select-dropdown',
	standalone: true,
	imports: [
		CommonModule,
		FormsModule,
		CategorySelectDropdownDividerComponent,
		CategorySelectDropdownTabComponent,
		CategorySelectDropdownGroupTabComponent,
		CategorySelectDropdownCategoryTabComponent,
		CategorySelectDropdownFooterComponent
	],
	templateUrl: './category-select-dropdown.component.html',
	styleUrls: ['./category-select-dropdown.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategorySelectDropdownComponent {
	@Input() categories: CategoryStateItem[] | null = null
	@Input() categoryGroups: CategoryGroupsStateItem[] | null = null
	@Input() currentCategory: string | string[] | null = null

	@Output() itemClick = new EventEmitter<string | null>()

	searchValue = ''
	tabs: CategorySelectTab[] = TABS_DEFAULT
	activeTab: CategorySelectTab = ACTIVE_TAB_DEFAULT
	isAddFormOpened = false

	onItemClick(id: string | null) {
		this.itemClick.emit(id)
	}

	setActiveTab(newActiveTab: CategorySelectTab) {
		this.activeTab = newActiveTab
	}

	stopPropagination(event: Event) {
		this.isAddFormOpened = false

		event.stopPropagation()
	}
}
