import { Component, EventEmitter, Input, Output } from '@angular/core'
import { sortedByOrder } from '../../../helpers/sorted-by-order.helper'
import { CategoryStateItem } from '@lapricot/client-lib/store'
import { filterCategoriesBySearch } from '../../helpers/filter-by-search.helper'
import { CategorySelectDropdownCategoryItemComponent } from '../../category-select-dropdown-category-item/category-select-dropdown-category-item.component'
import { CommonModule } from '@angular/common'

@Component({
	selector: 'la-category-select-dropdown-category-tab',
	standalone: true,
	imports: [CommonModule, CategorySelectDropdownCategoryItemComponent],
	templateUrl: './category-select-dropdown-category-tab.component.html'
})
export class CategorySelectDropdownCategoryTabComponent {
	@Input() categories: CategoryStateItem[] = []
	@Input() currentCategory: string | string[] | null = null
	@Input() searchValue = ''

	@Output() selectSubmit = new EventEmitter<string | null>()

	get categoriesList() {
		const searchedCategories = filterCategoriesBySearch(
			this.categories,
			this.searchValue
		)

		if (!searchedCategories) return null

		return sortedByOrder(searchedCategories)
	}

	onItemClick(value: string | null) {
		this.selectSubmit.emit(value)
	}

	checkIsItemChoised(id: string) {
		if (this.currentCategory === null) return false

		if (this.currentCategory instanceof Array) {
			return this.currentCategory.reduce((acc, item) => {
				return acc || item === id
			}, false)
		}

		return this.currentCategory === id
	}

	get isMulti() {
		return this.currentCategory instanceof Array
	}
}
