import {
	ChangeDetectionStrategy,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output
} from '@angular/core'
import { CommonModule } from '@angular/common'
import { sortedByOrder } from '../../../helpers/sorted-by-order.helper'
import {
	CategoryStateItem,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { filterCategoriesBySearch } from '../../helpers/filter-by-search.helper'
import { CategorySelectDropdownCategoryItemComponent } from '../../category-select-dropdown-category-item/category-select-dropdown-category-item.component'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-category-select-dropdown-group-tab',
	standalone: true,
	imports: [
		CommonModule,
		AngularSvgIconModule,
		CategorySelectDropdownCategoryItemComponent
	],
	templateUrl: './category-select-dropdown-group-tab.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategorySelectDropdownGroupTabComponent implements OnInit {
	@Input() categories: CategoryStateItem[] = []
	@Input() categoryGroups: CategoryGroupsStateItem[] = []
	@Input() currentCategory: string | string[] | null = null
	@Input() searchValue = ''

	@Output() selectSubmit = new EventEmitter<string | null>()

	categoriesAndGroupsForOutput: Record<string, CategoryStateItem[]> = {}

	openedGroupIds: string[] = []

	categoriesWithoutGroups: CategoryStateItem[] = []

	ngOnInit() {
		this.transformCategoriesAndGroupsForOutput()
	}

	get categoryGroupsForOutput() {
		const result = sortedByOrder(this.categoryGroups)

		if (result === null) {
			throw new Error('Something gonna wrong')
		}

		return result
	}

	get categoriesList() {
		const searchedCategories = filterCategoriesBySearch(
			this.categories,
			this.searchValue
		)

		if (!searchedCategories) return null

		return sortedByOrder(searchedCategories)
	}

	transformCategoriesAndGroupsForOutput() {
		this.categoriesAndGroupsForOutput = {}
		this.categoriesWithoutGroups = []

		sortedByOrder(this.categories)?.forEach(category => {
			if (category.group.length === 0) {
				this.categoriesWithoutGroups.push(category)

				return
			}

			category.group.forEach(groupId => {
				if (!this.categoriesAndGroupsForOutput[groupId]) {
					this.categoriesAndGroupsForOutput[groupId] = []
				}

				this.categoriesAndGroupsForOutput[groupId].push(category)
			})
		})
	}

	categoriesByGroupIdForOutput(groupId: string) {
		return filterCategoriesBySearch(
			this.categoriesAndGroupsForOutput[groupId],
			this.searchValue
		)
	}

	toggleGroupOpened(group: CategoryGroupsStateItem) {
		if (!this.openedGroupIds.includes(group._id)) {
			this.openedGroupIds.push(group._id)

			return
		}

		this.openedGroupIds = this.openedGroupIds.filter(item => item !== group._id)
	}

	onItemClick(value: string | null) {
		this.selectSubmit.emit(value)
	}

	checkIsItemChoised(id: string) {
		if (this.currentCategory === null) return false

		if (this.currentCategory instanceof Array) {
			return this.currentCategory.reduce((acc, item) => {
				return acc || item === id
			}, false)
		}

		return this.currentCategory === id
	}

	get isMulti() {
		return this.currentCategory instanceof Array
	}
}
