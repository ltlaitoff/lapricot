import { CommonModule } from '@angular/common'
import {
	Component,
	forwardRef,
	Input,
	OnChanges,
	SimpleChanges
} from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import { FormCloseDirective } from '@lapricot/client-lib/directives'
import {
	CategoryStateItem,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { CategorySelectDropdownComponent } from '../category-select-dropdown/category-select-dropdown.component'

@Component({
	selector: 'la-category-select-multi',
	standalone: true,
	imports: [CommonModule, FormCloseDirective, CategorySelectDropdownComponent],
	templateUrl: './category-select-multi.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => CategorySelectMultiComponent),
			multi: true
		}
	]
})
export class CategorySelectMultiComponent
	implements ControlValueAccessor, OnChanges
{
	@Input() categories: CategoryStateItem[] | null = null
	@Input() categoryGroups: CategoryGroupsStateItem[] | null = null
	@Input() buttonClass = ''
	// `value` is stub for sync formControlName work in adaptive
	@Input() value: string[] | null | undefined = null

	private onChange: null | ((_: string[]) => void) = null
	private onTouched: null | ((_: unknown) => void) = null

	ngOnChanges(changes: SimpleChanges) {
		if (changes['value'] && !changes['value'].firstChange) {
			this.currentId = changes['value'].currentValue
		}
	}

	isDropDownOpened = false

	disabled = false
	currentId: string[] = []

	onItemClick(id: string | null) {
		if (!this.onChange) return

		if (id === null) return

		if (this.currentId.includes(id)) {
			this.currentId = this.currentId.filter(item => item !== id)
		} else {
			this.currentId.push(id)
		}

		this.onChange(this.currentId)
	}

	writeValue(value: string[]): void {
		this.currentId = value
	}

	registerOnChange(callback: (_: unknown) => void): void {
		this.onChange = callback
	}

	registerOnTouched(callback: (_: unknown) => void): void {
		this.onTouched = callback
	}

	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}

	closeDropDown() {
		this.isDropDownOpened = false
	}

	toggleDropDown() {
		this.isDropDownOpened = !this.isDropDownOpened
	}

	getButtonClasses() {
		const result: string[] = []

		if (this.currentId.length > 0) {
			result.push('border-purple-300')
		}

		return result.join(' ')
	}
}
