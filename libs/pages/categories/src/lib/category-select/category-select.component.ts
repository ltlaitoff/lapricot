import {
	Component,
	forwardRef,
	Input,
	OnChanges,
	SimpleChanges
} from '@angular/core'
import { CommonModule } from '@angular/common'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import {
	CategoryStateItem,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { CicleComponent } from '@lapricot/ui'
import { CategorySelectDropdownComponent } from '../category-select-dropdown/category-select-dropdown.component'
import { FormCloseDirective } from '@lapricot/client-lib/directives'

@Component({
	selector: 'la-category-select',
	standalone: true,
	imports: [
		CommonModule,
		FormCloseDirective,
		CicleComponent,
		CategorySelectDropdownComponent
	],
	templateUrl: './category-select.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => CategorySelectComponent),
			multi: true
		}
	]
})
export class CategorySelectComponent
	implements ControlValueAccessor, OnChanges
{
	@Input({ required: true }) categories: CategoryStateItem[] | null = null
	@Input({ required: true }) categoryGroups: CategoryGroupsStateItem[] | null =
		null
	@Input() buttonClass = ''
	// `value` is stub for sync formControlName work in adaptive
	@Input() value: string | null | undefined = null

	private onChange: null | ((_: string | null) => void) = null
	private onTouched: null | ((_: unknown) => void) = null

	ngOnChanges(changes: SimpleChanges) {
		if (changes['value'] && !changes['value'].firstChange) {
			this.currentId = changes['value'].currentValue

			this.setCurrentCategory()
		}
	}

	isDropDownOpened = false

	disabled = false
	currentId: string | null = null
	currentCategory: CategoryStateItem | null = null

	onItemClick(id: string | null) {
		if (!this.onChange) return

		this.currentId = id

		this.onChange(id)
		this.setCurrentCategory()
		this.isDropDownOpened = false
	}

	writeValue(value: string): void {
		this.currentId = value
		this.setCurrentCategory()
	}

	registerOnChange(callback: (_: string | null) => void): void {
		this.onChange = callback
	}

	registerOnTouched(callback: (_: unknown) => void): void {
		this.onTouched = callback
	}

	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}

	setCurrentCategory() {
		if (this.categories) {
			this.currentCategory =
				this.categories.find(category => category._id === this.currentId) ||
				null
		}
	}

	closeDropDown() {
		this.isDropDownOpened = false
	}

	toggleDropDown() {
		this.isDropDownOpened = !this.isDropDownOpened
	}
}
