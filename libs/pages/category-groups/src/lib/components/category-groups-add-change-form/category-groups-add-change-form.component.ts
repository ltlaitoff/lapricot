import { CommonModule } from '@angular/common'
import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output
} from '@angular/core'
import {
	FormGroup,
	FormControl,
	Validators,
	ReactiveFormsModule
} from '@angular/forms'
import { CreateCategoryGroupData } from '@lapricot/shared/types'
import { Subscription } from 'rxjs'
import { ColorSelectComponent } from '@lapricot/ui'
@Component({
	selector: 'la-category-groups-add-change-form',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule, ColorSelectComponent],
	templateUrl: './category-groups-add-change-form.component.html',
	styleUrls: ['./category-groups-add-change-form.component.scss']
})
export class CategoryGroupsAddChangeFormComponent implements OnInit, OnDestroy {
	@Input() initialFormData: {
		name: string
		color: string
	} | null = null
	@Input() fromType: 'add' | 'edit' = 'add'

	@Output() addGroup = new EventEmitter<CreateCategoryGroupData>()
	@Output() deleteGroup = new EventEmitter()

	errorMessage: string | null = null

	formData = new FormGroup({
		name: new FormControl<string>(this.initialFormData?.name || '', [
			Validators.required,
			Validators.maxLength(30)
		]),
		color: new FormControl<string | null>(this.initialFormData?.color || null)
	})

	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		if (this.initialFormData) {
			this.formData.setValue(this.initialFormData)
		}

		this.formDataValueChangesSubscription =
			this.formData.valueChanges.subscribe(() => {
				this.errorMessage = this.checkFormDataOnErrors()
			})
	}

	ngOnDestroy() {
		this.formDataValueChangesSubscription.unsubscribe()
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			}
		})
	}

	checkFormDataOnErrors() {
		if (this.formData.valid) {
			return null
		}

		const name = this.formData.get('name')

		if (name && name.invalid && (name.dirty || name.touched)) {
			if (name.errors?.['required']) {
				return 'Name is a required field'
			}

			if (name.errors?.['maxlength']) {
				return 'Name must be no more than 30 characters long'
			}
		}

		return null
	}

	onSubmitAddForm(event: SubmitEvent) {
		if (this.formData.invalid) {
			this.validateAllFormFields(this.formData)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		event.stopPropagation()

		const valueForSend = this.prepareSubmitData(this.formData.value)

		if (valueForSend == null) {
			return
		}

		this.addGroup.emit(valueForSend)
		this.formData.reset()
	}

	private prepareSubmitData(value: typeof this.formData.value) {
		if (!value.name || !value.color) {
			return null
		}

		const valueForSend = {
			name: value.name,
			color: value.color
		}

		return valueForSend
	}

	get formTypeIsAdd() {
		return this.fromType === 'add'
	}

	get formTypeIsEdit() {
		return this.fromType === 'edit'
	}

	stopPropagation(event: Event) {
		event.stopPropagation()
	}

	deleteButtonClick() {
		this.deleteGroup.emit()
	}
}
