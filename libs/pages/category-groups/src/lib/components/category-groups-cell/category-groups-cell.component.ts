import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges
} from '@angular/core'
import { Store } from '@ngrx/store'
import {
	RootState,
	CategoryGroupsActions,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { CreateCategoryGroupData } from '@lapricot/shared/types'
import { arraysEqual } from '../../helpers/arrays-equal.helper'
import { CommonModule } from '@angular/common'
import { FormCloseDirective } from '@lapricot/client-lib/directives'
import { CategoryGroupsItemComponent } from '../category-groups-item/category-groups-item.component'
import { CategoryGroupsFormComponent } from '../category-groups-form/category-groups-form.component'

@Component({
	selector: 'la-category-groups-cell',
	standalone: true,
	imports: [
		CommonModule,
		FormCloseDirective,
		CategoryGroupsItemComponent,
		CategoryGroupsFormComponent
	],
	templateUrl: './category-groups-cell.component.html'
})
export class CategoryGroupsCellComponent implements OnInit, OnChanges {
	@Input() categoryGroups: Array<string> = []
	@Input() allCategoryGroups: CategoryGroupsStateItem[] = []

	@Output() changeCategoryGroups = new EventEmitter<string[]>()

	choicedCategoryGroups: CategoryGroupsStateItem[] = []
	isFormShowed = false

	ngOnInit() {
		this.updateChoicedCategoryGroups(
			this.categoryGroups,
			this.allCategoryGroups
		)
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['allCategoryGroups'].firstChange) return

		const allCategoryGroups: CategoryGroupsStateItem[] =
			changes['allCategoryGroups'].currentValue

		if (this.choicedCategoryGroups.length === 0) {
			this.updateChoicedCategoryGroups(this.categoryGroups, allCategoryGroups)
			return
		}

		const choicedCategoryGroupsAsStringArray = this.choicedCategoryGroups.map(
			item => item._id
		)

		this.updateChoicedCategoryGroups(
			choicedCategoryGroupsAsStringArray,
			allCategoryGroups
		)
	}

	closeForm() {
		if (!this.isFormShowed) return

		this.isFormShowed = false
		this.updateCategoryGroup()
	}

	showForm() {
		this.isFormShowed = true
	}

	addChoicedCategoryGroup(categoryGroup: CategoryGroupsStateItem) {
		this.choicedCategoryGroups = [...this.choicedCategoryGroups, categoryGroup]
	}

	deleteChoicedCategoryGroup(categoryGroup: CategoryGroupsStateItem) {
		this.choicedCategoryGroups = this.choicedCategoryGroups.filter(
			item => item._id !== categoryGroup._id
		)
	}

	editCategoryGroup([currentValue, editedValue]: [
		CategoryGroupsStateItem,
		CreateCategoryGroupData
	]) {
		this.store.dispatch(
			CategoryGroupsActions.update({
				old: currentValue,
				dataForUpdate: editedValue
			})
		)
	}

	deleteCategoryGroup(category: CategoryGroupsStateItem) {
		this.store.dispatch(CategoryGroupsActions.delete(category))
	}

	addNewCategoryGroup(categoryForAdd: CreateCategoryGroupData) {
		this.store.dispatch(CategoryGroupsActions.add(categoryForAdd))
	}

	private updateChoicedCategoryGroups(
		categoryGroups: string[],
		allCategoryGroups: CategoryGroupsStateItem[]
	) {
		this.choicedCategoryGroups = categoryGroups
			.map(value => allCategoryGroups.find(item => item._id === value))
			.filter(item => item != undefined) as CategoryGroupsStateItem[]
	}

	private updateCategoryGroup() {
		const choicedCategoryGroupsAsStringArray = this.choicedCategoryGroups.map(
			item => item._id
		)

		if (arraysEqual(this.categoryGroups, choicedCategoryGroupsAsStringArray)) {
			return
		}

		this.changeCategoryGroups.emit(choicedCategoryGroupsAsStringArray)
	}

	toggleFormOnKeyPress(event: KeyboardEvent) {
		switch (event.code) {
			case 'Space':
			case 'Enter': {
				event.preventDefault()
				this.toggleFormShowed()

				return
			}
		}
	}

	private toggleFormShowed() {
		if (this.isFormShowed) {
			this.closeForm()
			return
		}

		this.showForm()
		return
	}

	constructor(private store: Store<RootState>) {}
}
