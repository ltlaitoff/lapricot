import { CdkDrag, CdkDragDrop, DragDropModule } from '@angular/cdk/drag-drop'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { Store } from '@ngrx/store'
import { sortedByOrder } from '../../helpers/sorted-by-order.helper'
import { RootState } from '@lapricot/client-lib/store'
import {
	CategoryGroupsActions,
	CategoryGroupsStateItem,
	CategoryGroupsSyncStateItem
} from '@lapricot/client-lib/store'
import { CreateCategoryGroupData } from '@lapricot/shared/types'
import { CommonModule } from '@angular/common'
import { CategoryGroupsItemComponent } from '../category-groups-item/category-groups-item.component'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { CategoryGroupsAddChangeFormComponent } from '../category-groups-add-change-form/category-groups-add-change-form.component'

@Component({
	selector: 'la-category-groups-form',
	standalone: true,
	imports: [
		CommonModule,
		AngularSvgIconModule,
		CategoryGroupsItemComponent,
		DragDropModule,
		CdkDrag,
		CategoryGroupsAddChangeFormComponent
	],
	templateUrl: './category-groups-form.component.html',
	styleUrls: ['./category-groups-form.component.scss']
})
export class CategoryGroupsFormComponent {
	@Input() categoryGroups: CategoryGroupsStateItem[] | null = null
	@Input() choicedCategoryGroups: CategoryGroupsStateItem[] | null = null

	@Output() addChoicedCategoryGroup =
		new EventEmitter<CategoryGroupsStateItem>()
	@Output() deleteChoicedCategoryGroup =
		new EventEmitter<CategoryGroupsStateItem>()

	@Output() editCategoryGroup = new EventEmitter<
		[CategoryGroupsStateItem, CreateCategoryGroupData]
	>()
	@Output() deleteCategoryGroup = new EventEmitter<CategoryGroupsStateItem>()

	@Output() addNewCategoryGroup = new EventEmitter<CreateCategoryGroupData>()

	isEditCategoryGroupFormShowed: string | null = null
	isAddFormOpened = false

	get categoryGroupsList() {
		return sortedByOrder(this.categoryGroups)
	}

	get categoryGroupsChoiced() {
		return this.choicedCategoryGroups
	}

	onDeleteClick(categoryGroup: CategoryGroupsStateItem) {
		this.deleteChoicedCategoryGroup.emit(categoryGroup)
	}

	private addNewCheckedCategoryGroupInner(
		categoryGroup: CategoryGroupsStateItem
	) {
		if (this.checkIsCategoryGroupChecked(categoryGroup._id)) {
			return
		}

		this.addChoicedCategoryGroup.emit(categoryGroup)
	}

	addNewCheckedCategoryGroup(
		event: MouseEvent | KeyboardEvent,
		categoryGroup: CategoryGroupsStateItem
	) {
		event.preventDefault()

		if (event instanceof KeyboardEvent) {
			switch (event.code) {
				case 'Space':
				case 'Enter':
					return this.addNewCheckedCategoryGroupInner(categoryGroup)
			}
		}

		this.addNewCheckedCategoryGroupInner(categoryGroup)
	}

	closeFormsWithStopPropagation(event: Event) {
		event.stopPropagation()

		this.closeForms()
	}

	toggleEditCategoryGroupForm(
		event: MouseEvent,
		categoryGroup: CategoryGroupsStateItem
	) {
		event.stopPropagation()

		if (this.isEditCategoryGroupFormShowed === null) {
			this.isEditCategoryGroupFormShowed = categoryGroup._id
			return
		}

		this.isEditCategoryGroupFormShowed = null
	}

	closeForms() {
		this.isEditCategoryGroupFormShowed = null
		this.isAddFormOpened = false
	}

	toggleAddForm(e: Event) {
		e.stopPropagation()

		this.isAddFormOpened = !this.isAddFormOpened
	}

	checkIsCategoryGroupChecked(categoryGroupId: string) {
		if (this.choicedCategoryGroups === null) {
			return false
		}

		return (
			this.choicedCategoryGroups.find(item => item._id === categoryGroupId) !==
			undefined
		)
	}

	editCategoryGroupInner(
		currentValue: CategoryGroupsStateItem,
		editedValue: CreateCategoryGroupData
	) {
		this.isEditCategoryGroupFormShowed = null
		this.isAddFormOpened = false

		this.editCategoryGroup.emit([currentValue, editedValue])
	}

	deleteCategoryGroupInner(category: CategoryGroupsStateItem) {
		this.deleteCategoryGroup.emit(category)
	}

	addNewCategoryGroupInner(categoryForAdd: CreateCategoryGroupData) {
		this.isAddFormOpened = false

		this.addNewCategoryGroup.emit(categoryForAdd)
	}

	drop(
		event: CdkDragDrop<
			CategoryGroupsSyncStateItem[],
			CategoryGroupsSyncStateItem[],
			CategoryGroupsSyncStateItem
		>
	) {
		if (this.categoryGroupsList === null) return
		if (event.previousIndex === event.currentIndex) return

		const categoryData = event.item.data
		const previousIndex = categoryData.order
		const currentIndex = this.categoryGroupsList[event.currentIndex].order

		this.store.dispatch(
			CategoryGroupsActions.reorder({
				props: categoryData,
				previousIndex: previousIndex,
				currentIndex: currentIndex
			})
		)
	}

	toggleEditCategoryGroupFormKeyboard(event: KeyboardEvent) {
		switch (event.code) {
			case 'Space':
			case 'Enter': {
				event.stopPropagation()

				return
			}
		}
	}

	constructor(private store: Store<RootState>) {}
}
