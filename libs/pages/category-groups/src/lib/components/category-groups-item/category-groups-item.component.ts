import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
	selector: 'la-category-groups-item',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './category-groups-item.component.html'
})
export class CategoryGroupsItemComponent {
	@Input() color = ''
	@Input() name = ''
	@Input() showDeleteButton = false

	@Output() groupDelete = new EventEmitter()

	onDeleteButtonClick(event: MouseEvent) {
		event.preventDefault()

		this.groupDelete.emit()
	}
}
