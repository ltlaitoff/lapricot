import { Component, OnDestroy, OnInit } from '@angular/core'
import {
	FormGroup,
	FormControl,
	Validators,
	ReactiveFormsModule,
	FormsModule
} from '@angular/forms'
import { Store } from '@ngrx/store'
import { formatDateToDateTimeLocalInput } from '../helpers'

import {
	RootState,
	selectCategories,
	CategoryStateItem,
	selectCategoryGroups,
	CategoryGroupsStateItem,
	StatisticActions,
	selectSeanceActive,
	SeanceActiveActions
} from '@lapricot/client-lib/store'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { SeanceActiveState } from '@lapricot/client-lib/store'
import { CommonModule } from '@angular/common'
import { CicleComponent, TimeNumberPickerComponent } from '@lapricot/ui'
import { CategorySelectComponent } from '@lapricot/pages/categories'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { StatisticLogComponent } from '@lapricot/pages/statistic'

@Component({
	selector: 'la-home-page',
	standalone: true,
	imports: [
		CommonModule,
		TimeNumberPickerComponent,
		CategorySelectComponent,
		FormsModule,
		ReactiveFormsModule,
		StatisticLogComponent,
		CicleComponent,
		AngularSvgIconModule
	],
	templateUrl: './home-page.component.html',
	styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
	addForm = new FormGroup({
		count: new FormControl<number>(0, [
			Validators.required,
			Validators.pattern('^[-+]?[0-9]*$')
		]),
		comment: new FormControl<string>('', Validators.maxLength(150)),
		category: new FormControl<string | null>(null, Validators.required),
		date: new FormControl<string>('', Validators.required)
	})

	categories: CategoryStateItem[] | null = null
	categoryGroups: CategoryGroupsStateItem[] | null = null

	additionalSettingsShow = false
	additinalOptions = {
		doNotClearComment: false,
		doNotClearCount: false,
		showDatetimePicker: false,
		doNotUpdateDateAfterSubmit: false
	}

	seanceActive: SeanceActiveState

	errorMessage: string | null = null

	private selectCategoriesSubscribe: Subscription
	private selectCategoryGroupsSubscribe: Subscription
	private selectSeanceSubscribe: Subscription
	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		this.selectSeanceSubscribe = this.store
			.select(selectSeanceActive)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.seanceActive = value

				if (value.currentSeance) {
					this.addForm.controls['category'].disable()
					this.addForm.controls['category'].setValue(
						value.currentSeance.records[value.currentRecord].category
					)
					this.nextSeanceRecord(value.currentRecord || 0, false)
				}
			})

		this.selectCategoriesSubscribe = this.store
			.select(selectCategories)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categories = value

				if (this.seanceActive && this.seanceActive.currentSeance) {
					this.nextSeanceRecord(this.seanceActive.currentRecord || 0, false)
				}
			})

		this.selectCategoryGroupsSubscribe = this.store
			.select(selectCategoryGroups)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categoryGroups = value
			})

		this.resetAddFormDate()

		this.formDataValueChangesSubscription = this.addForm.valueChanges.subscribe(
			() => {
				this.errorMessage = this.checkFormDataOnErrors()
			}
		)
	}

	ngOnDestroy() {
		this.selectSeanceSubscribe.unsubscribe()
		this.selectCategoriesSubscribe.unsubscribe()
		this.selectCategoryGroupsSubscribe.unsubscribe()
		this.formDataValueChangesSubscription.unsubscribe()
	}

	checkSeanceRecordAndApproach() {
		if (!this.seanceActive.currentSeance) return
	}

	nextSeanceRecord(nextRecordIndex?: number, isSetNext = true) {
		if (!this.seanceActive.currentSeance) return
		if (!this.categories) return

		const seanceMaxRecordIndex =
			this.seanceActive.currentSeance.records.length - 1

		const nextRecord =
			nextRecordIndex === undefined
				? this.seanceActive.currentRecord + 1
				: nextRecordIndex

		if (nextRecord === seanceMaxRecordIndex + 1) {
			return this.endCurrentSeance()
		}

		const recordInfo = this.seanceActive.currentSeance.records[nextRecord]

		const categoryTrashOrDeleted =
			this.categories.find(item => recordInfo.category === item._id) ===
			undefined

		if (
			recordInfo.count === 0 ||
			recordInfo.category === null ||
			categoryTrashOrDeleted ||
			recordInfo.trash === true
		) {
			return this.nextSeanceRecord(nextRecord + 1)
		}

		if (isSetNext) {
			this.store.dispatch(SeanceActiveActions.setRecord({ data: nextRecord }))

			this.addForm.controls['category'].setValue(
				this.seanceActive.currentSeance.records[nextRecord].category
			)

			this.store.dispatch(SeanceActiveActions.setApproach({ data: 0 }))
		}
	}

	nextSeanceApproach() {
		if (!this.seanceActive.currentSeance) return

		const maxApproachesCountInCurrentRecord =
			this.seanceActive.currentSeance.records[this.seanceActive.currentRecord]
				.count

		if (
			this.seanceActive.currentApproach + 1 ===
			maxApproachesCountInCurrentRecord
		) {
			this.nextSeanceRecord()
			return
		}

		this.store.dispatch(
			SeanceActiveActions.setApproach({
				data: this.seanceActive.currentApproach + 1
			})
		)
	}

	checkFormDataOnErrors() {
		if (this.addForm.valid) {
			return null
		}

		const count = this.addForm.get('count')

		if (count && count.invalid && (count.dirty || count.touched)) {
			if (count.errors?.['required']) {
				return 'Count is a required field'
			}

			return 'Count is invalid'
		}

		const comment = this.addForm.get('comment')

		if (comment && comment.invalid && (comment.dirty || comment.touched)) {
			if (comment.errors?.['maxlength']) {
				return 'Comment must be no more than 150 characters long'
			}
		}

		const category = this.addForm.get('category')

		if (category && category.invalid && (category.dirty || category.touched)) {
			if (category.errors?.['required']) {
				return 'Category is a required field'
			}
		}

		const date = this.addForm.get('date')

		if (date && date.invalid && (date.dirty || date.touched)) {
			if (date.errors?.['required']) {
				return 'Date is a required field'
			}
		}

		return null
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			}
		})
	}

	onSubmit() {
		if (this.addForm.invalid) {
			this.validateAllFormFields(this.addForm)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		const valueForSend = this.prepareSubmitData(this.addForm.getRawValue())

		if (valueForSend === null) {
			return
		}

		this.store.dispatch(StatisticActions.add(valueForSend))

		this.addForm.reset({
			count: this.additinalOptions.doNotClearCount ? valueForSend.count : 0,
			comment: this.additinalOptions.doNotClearComment
				? valueForSend.comment
				: '',
			category: valueForSend.category,
			date: this.additinalOptions.doNotUpdateDateAfterSubmit
				? valueForSend.datetimeLocalDate
				: this.getFormattedNowDateForDatetimeLocalInput()
		})

		this.nextSeanceApproach()
	}

	private prepareSubmitData(value: typeof this.addForm.value) {
		if (
			value.count == null ||
			value.comment == null ||
			value.category == null ||
			value.date == null
		) {
			return null
		}

		const submitDate = this.additinalOptions.showDatetimePicker
			? new Date(value.date).toISOString()
			: new Date(Date.now()).toISOString()

		const valueForSend = {
			count: value.count,
			comment: value.comment,
			category: value.category,
			date: submitDate,
			datetimeLocalDate: value.date,
			summ: 0
		}

		return valueForSend
	}

	get choicedCategory() {
		const categoryId = this.addForm.getRawValue().category

		if (categoryId === null || this.categories === null) return null

		const findedCategoty = this.categories.find(item => item._id === categoryId)

		return findedCategoty || null
	}

	private resetAddFormDate() {
		this.addForm.setValue({
			...this.addForm.getRawValue(),
			date: this.getFormattedNowDateForDatetimeLocalInput()
		})
	}

	private getFormattedNowDateForDatetimeLocalInput() {
		return formatDateToDateTimeLocalInput(new Date(Date.now()))
	}

	toggleAdditionalOptionsShow() {
		this.additionalSettingsShow = !this.additionalSettingsShow
	}

	endCurrentSeance() {
		this.store.dispatch(SeanceActiveActions.endSeance())
		this.addForm.controls['category'].enable()
		this.addForm.controls['category'].setValue(null)
	}

	constructor(private store: Store<RootState>) {}
}
