import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { firstValueFrom, map } from 'rxjs'
import {
	changeStatisticCategoryIdToCategoryObject,
	filterByTrash,
	RootState,
	StatisticActions,
	StatisticStateItemWithCategory
} from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'
import { FilteredStatisticBuilder } from '@lapricot/pages/statistic/lib/builders/filtered-statistic.builder'
import {
	InitialControls,
	INITIAL_CONTROLS
} from '@lapricot/pages/statistic/lib/statistic.config'
import { ApiService } from '@lapricot/client-lib/data-access'
import { UserProfileBase } from '@lapricot/shared/types'
import { CommonModule } from '@angular/common'
import {
	StatisticChartComponent,
	StatisticTableComponent,
	StatisticControlsComponent
} from '@lapricot/pages/statistic'

@Component({
	selector: 'la-profile-page',
	standalone: true,
	imports: [
		CommonModule,
		StatisticChartComponent,
		StatisticTableComponent,
		StatisticControlsComponent
	],
	templateUrl: './profile-page.component.html',
	styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {
	constructor(
		private route: ActivatedRoute,
		private store: Store<RootState>,
		private api: ApiService
	) {}

	nickname: string
	error = false
	loading = true
	user: UserProfileBase | null = null
	statistics: StatisticStateItemWithCategory[] | null = null
	statisticForOutput: StatisticStateItemWithCategory[] | null = null

	formData: InitialControls = structuredClone(INITIAL_CONTROLS)

	ngOnInit() {
		this.route.paramMap
			.pipe(map((params: ParamMap) => params.get('nickname')))
			.subscribe(async nickname => {
				this.error = false
				this.user = null
				this.statistics = null
				this.statisticForOutput = null

				console.log(nickname)

				if (nickname === null) return

				this.nickname = nickname

				this.loading = true
				const userData = await firstValueFrom(
					this.api.getUserDataByNickname$(nickname)
				)

				this.loading = false
				if (userData === null) {
					this.error = true
					return
				}

				this.user = userData

				const rawStatistic = userData.statistic
				const rawCategories = userData.categories

				// @ts-expect-error asd
				this.user.statistic = null
				// @ts-expect-error adad
				this.user.categories = null

				this.statistics = changeStatisticCategoryIdToCategoryObject(
					filterByTrash(rawStatistic),
					rawCategories
				)

				this.statistics.sort((a, b) => {
					return new Date(b.date).getTime() - new Date(a.date).getTime()
				})

				this.statisticForOutput = this.updateFilteredStatistic(
					this.statistics,
					this.formData
				)
			})
	}

	forceReloadStatistic(force: boolean) {
		this.store.dispatch(StatisticActions.load({ force: force }))
	}

	formDataChange(newFormData: InitialControls) {
		if (this.statistics === null) return

		this.formData = newFormData

		this.statisticForOutput = this.updateFilteredStatistic(
			this.statistics,
			newFormData
		)
	}

	private updateFilteredStatistic(
		statistics: StatisticStateItemWithCategory[],
		formData: InitialControls
	): StatisticStateItemWithCategory[] {
		return new FilteredStatisticBuilder(statistics)
			.filterComments(formData.comment)
			.filterMode(formData.mode)
			.filterDate(formData.date, formData)
			.filterCategories(formData.categories)
			.build()
	}
}
