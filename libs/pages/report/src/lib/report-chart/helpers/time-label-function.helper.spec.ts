import { timeLabelFunction } from './time-label-function.helper'

describe('report-chart helper timeLabelFunction', () => {
	it.each`
		input                  | result
		${{ parsed: 3463466 }} | ${' 02:04:26'}
		${{ parsed: 435324 }}  | ${' 00:55:24'}
		${{ parsed: 1256757 }} | ${' 13:05:57'}
		${{ parsed: 65934 }}   | ${' 18:18:54'}
		${{ parsed: 236 }}     | ${' 00:03:56'}
	`(
		'In timeLabelFunction "$result" should return on input = $input',
		({ input, result }) => {
			expect(timeLabelFunction(input)).toBe(result)
		}
	)
})
