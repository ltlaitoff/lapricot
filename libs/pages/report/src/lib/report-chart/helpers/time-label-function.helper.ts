export function timeLabelFunction(context) {
	return (
		' ' +
		new Date(context.parsed * 1000).toLocaleTimeString('en-GB', {
			hour: '2-digit',
			minute: '2-digit',
			second: '2-digit',
			timeZone: 'UTC'
		})
	)
}
