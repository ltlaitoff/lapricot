import { Component, Input, OnChanges, OnInit } from '@angular/core'
import { Chart } from 'chart.js'
import { SummStatistic } from '../types/SummStatistic.types'
import { getChartOptions } from './report-chart.config'

@Component({
	selector: 'la-report-chart',
	standalone: true,
	templateUrl: './report-chart.component.html'
})
export class ReportChartComponent implements OnInit, OnChanges {
	@Input({ required: true }) summData: SummStatistic | null = null

	private numberChart!: Chart<'doughnut', number[], unknown>
	private timeChart!: Chart<'doughnut', number[], unknown>

	ngOnInit() {
		this.numberChart = new Chart('chart-of-number', {
			type: 'doughnut',
			data: this.getChartData(),
			options: getChartOptions('number')
		})

		this.timeChart = new Chart('chart-of-time', {
			type: 'doughnut',
			data: this.getChartData('time'),
			options: getChartOptions('time')
		})
	}

	ngOnChanges() {
		if (this.numberChart) {
			this.numberChart.data = this.getChartData('number')
			this.numberChart.update()
		}

		if (this.timeChart) {
			this.timeChart.data = this.getChartData('time')
			this.timeChart.update()
		}
	}

	getChartData(mode: 'number' | 'time' = 'number') {
		if (this.summData === null) return { datasets: [] }

		const labels: string[] = []
		const datasets: Array<{
			label: string
			data: Array<number>
			backgroundColor: Array<string>
			hoverOffset: number
		}> = [
			{
				label: '',
				data: [],
				backgroundColor: [],
				hoverOffset: 4
			}
		]

		this.summData.map(item => {
			if (item.category.mode !== mode) return

			labels.push(item.category.name)
			datasets[0].data.push(item.count)
			datasets[0].backgroundColor.push(item.category.color)
		})

		return {
			labels: labels,
			datasets: datasets
		}
	}
}
