import { ChartOptions } from 'chart.js'
import { timeLabelFunction } from './helpers/time-label-function.helper'

const NUMBER_SUMM_TITLE_TEXT = 'Number summ'
const TIME_SUMM_TITLE_TEXT = 'Time summ'

const defaultOptions: ChartOptions<'doughnut'> = {
	plugins: {
		title: {
			display: true,
			text: NUMBER_SUMM_TITLE_TEXT,
			position: 'bottom',
			font: {
				size: 16,
				weight: 'lighter'
			}
		},
		legend: {
			display: false
		}
	}
}

export function getChartOptions(
	type: 'number' | 'time'
): ChartOptions<'doughnut'> {
	const copy = structuredClone(defaultOptions)

	if (type === 'number') {
		return copy
	}

	copy.plugins.title.text = TIME_SUMM_TITLE_TEXT
	copy.plugins.tooltip = {
		callbacks: {
			label: timeLabelFunction
		}
	}

	return copy
}
