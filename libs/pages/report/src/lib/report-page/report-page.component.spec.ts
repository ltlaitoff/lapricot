const mockedSelectStatistic = () => {
	return null
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import {
	RootState,
	StatisticStateItemWithCategory
} from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'
import { Subject } from 'rxjs'
import { ReportPageComponent } from './report-page.component'

global.structuredClone = (val: unknown) => JSON.parse(JSON.stringify(val))

const SELECT_STATISTIC_DATA: StatisticStateItemWithCategory[] = []

jest.mock('@lapricot/client-lib/store', () => {
	return {
		selectStatistic: mockedSelectStatistic
	}
})

const observableStatus = new Subject()

const mockedStore: Partial<Store<RootState>> = {
	// @ts-expect-error Test
	select(value) {
		if (value === mockedSelectStatistic) {
			return observableStatus
		}

		return null
	}
}

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [ReportPageComponent],
		schemas: [CUSTOM_ELEMENTS_SCHEMA],
		providers: [
			{
				provide: Store,
				useValue: mockedStore
			}
		]
	}).compileComponents()

	const fixture = TestBed.createComponent(ReportPageComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

/* 
	Component la-report-page should be
	In la-report-page la-statistic-loading should be on @Input statistic = null
	In la-report-page la-statistic-loading should be on @Input statistic.length = 0

	In la-report-page la-report-chart with Input summData should be
	In la-report-page la-statistic-controls with Input formData, Input showChartStatisticControls = false should be
	In la-report-page la-report-summ-table with Input statistic = @Input statistic should be
	In la-report-page la-statistic-table with Input statistic = statistic after filtrations, sorting = formData.sorting and showControl = false  should be
	
	In la-report-page la-statistic-table Input statistic and Input sorting should change after call (formDataChange) event on la-statistic-controls 
*/

describe('ReportPageComponent', () => {
	it('Component la-report-page should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-report-page la-statistic-loading should be on @Input statistic = null', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(By.css('la-statistic-loading'))

		expect(element).toBeTruthy()
	})

	it('In la-report-page la-statistic-loading should be on @Input statistic.length = 0', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next([])
		fixture.detectChanges()

		const element = fixture.debugElement.query(By.css('la-statistic-loading'))

		expect(element).toBeTruthy()
	})

	// it('In la-report-page la-report-chart with Input summData should be', async () => {
	// 	const { fixture } = await initializeComponent()
	// })

	// it('In la-report-page la-statistic-controls with Input formData, Input showChartStatisticControls = false should be', async () => {
	// 	const { fixture } = await initializeComponent()
	// })

	// it('In la-report-page la-report-summ-table with Input statistic = @Input statistic should be', async () => {
	// 	const { fixture } = await initializeComponent()
	// })

	// it('In la-report-page la-statistic-table with Input statistic = statistic after filtrations, sorting = formData.sorting and showControl = false  should be', async () => {
	// 	const { fixture } = await initializeComponent()
	// })

	// it('In la-report-page la-statistic-table Input statistic and Input sorting should change after call (formDataChange) event on la-statistic-controls ', async () => {
	// 	const { fixture } = await initializeComponent()
	// })
})
