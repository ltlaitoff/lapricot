import { Component, OnDestroy, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import {
	selectStatistic,
	StatisticActions,
	RootState,
	StatisticStateItemWithCategory
} from '@lapricot/client-lib/store'
import {
	InitialControls,
	INITIAL_CONTROLS
} from '@lapricot/pages/statistic/lib/statistic.config'
import { FilteredStatisticBuilder } from '@lapricot/pages/statistic/lib/builders/filtered-statistic.builder'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { SummStatistic } from '../types/SummStatistic.types'
import { CommonModule } from '@angular/common'
import { ReportChartComponent } from '../report-chart/report-chart.component'
import { StatisticControlsComponent } from '@lapricot/pages/statistic/lib/statistic-controls/statistic-controls.component'
import { StatisticTableComponent } from '@lapricot/pages/statistic/lib/statistic-table/statistic-table.component'
import { ReportSummTableComponent } from '../report-summ-table/report-summ-table.component'
import { StatisticLoadingComponent } from '@lapricot/pages/statistic/lib/statistic-loading/statistic-loading.component'

@Component({
	selector: 'la-report-page',
	standalone: true,
	imports: [
		CommonModule,
		ReportChartComponent,
		StatisticControlsComponent,
		StatisticTableComponent,
		ReportSummTableComponent,
		StatisticLoadingComponent
	],
	templateUrl: './report-page.component.html'
})
export class ReportPageComponent implements OnInit, OnDestroy {
	constructor(private store: Store<RootState>) {}

	statistics: StatisticStateItemWithCategory[] | null = null
	statisticForOutput: StatisticStateItemWithCategory[] | null = null
	summStatistic: SummStatistic | null = null

	formData: InitialControls = structuredClone(INITIAL_CONTROLS)

	private selectStatisticSubscribe: Subscription

	ngOnInit() {
		this.selectStatisticSubscribe = this.store
			.select(selectStatistic)
			.pipe(distinctUntilChanged())
			.subscribe(newStatistic => {
				this.statistics = newStatistic
				this.statisticForOutput = this.updateFilteredStatistic(
					newStatistic,
					this.formData
				)

				this.summStatistic = this.getSummStatistic(this.statisticForOutput)
			})
	}

	ngOnDestroy() {
		this.selectStatisticSubscribe.unsubscribe()
	}

	forceReloadStatistic(force: boolean) {
		this.store.dispatch(StatisticActions.load({ force: force }))
	}

	formDataChange(newFormData: InitialControls) {
		if (this.statistics === null) return
		this.formData = newFormData

		this.statisticForOutput = this.updateFilteredStatistic(
			this.statistics,
			newFormData
		)

		this.summStatistic = this.getSummStatistic(this.statisticForOutput)
	}

	getSummStatistic(statistic: StatisticStateItemWithCategory[]) {
		const result: SummStatistic = []

		statistic.forEach(item => {
			const finded = result.find(
				resultItem => resultItem.category._id === item.category._id
			)

			if (finded === undefined) {
				result.push({ category: item.category, count: item.count })
				return
			}

			finded.count += item.count
		})

		return result
	}

	private updateFilteredStatistic(
		statistics: StatisticStateItemWithCategory[],
		formData: InitialControls
	): StatisticStateItemWithCategory[] {
		return new FilteredStatisticBuilder(statistics)
			.filterComments(formData.comment)
			.filterMode(formData.mode)
			.filterDate(formData.date, formData)
			.filterCategories(formData.categories)
			.build()
	}
}
