import { TestBed } from '@angular/core/testing'
import { ReportSummTableComponent } from './report-summ-table.component'
import { By } from '@angular/platform-browser'
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core'
import { CategoryStateItem } from '@lapricot/client-lib/store'

export type SummStatisticStub = {
	category: Partial<CategoryStateItem>
	count: number
}[]

interface ComponentInputs {
	statistic: SummStatisticStub | null
}

const defaultInputs: ComponentInputs = {
	statistic: [
		{
			category: {
				color: '#f0f0f0',
				name: 'Category 1',
				dimension: 'kgs',
				mode: 'number'
			},
			count: 10
		},
		{
			category: {
				color: '#fff',
				name: 'Category 2',
				mode: 'time'
			},
			count: 6457
		},
		{
			category: {
				color: '#ff00ff',
				name: 'Category 3',
				dimension: 'min:sec',
				mode: 'time'
			},
			count: 234
		},
		{
			category: {
				color: '#f872c6',
				name: 'Category 4',
				mode: 'number'
			},
			count: 73
		}
	]
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [ReportSummTableComponent],
		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(ReportSummTableComponent)
	const component = fixture.componentInstance

	// @ts-expect-error All good
	component.statistic =
		inputs.statistic !== undefined ? inputs.statistic : defaultInputs.statistic

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('ReportSummTableComponent', () => {
	describe('basic', () => {
		it('Component la-report-summ-table should be', async () => {
			const { component } = await initializeComponent()

			expect(component).toBeTruthy()
		})

		it('In la-report-summ-table table should be', async () => {
			const { fixture } = await initializeComponent()

			const element = fixture.debugElement.query(By.css('table'))

			expect(element).toBeTruthy()
		})

		it('In la-report-summ-table first tr with th "Category" and th "Count" should be', async () => {
			const { fixture } = await initializeComponent()

			const elements = fixture.debugElement
				.queryAll(By.css('tr'))[0]
				.queryAll(By.css('th'))

			expect(elements[0].nativeElement.textContent).toContain('Category')
			expect(elements[1].nativeElement.textContent).toContain('Count')
		})
	})

	describe("Check all tr's without first for data", () => {
		let rows: DebugElement[]

		beforeAll(async () => {
			const { fixture } = await initializeComponent()

			rows = fixture.debugElement.queryAll(By.css('tr')).slice(1)
		})

		it('Should be 2 td', async () => {
			rows.map(row => {
				expect(row.queryAll(By.css('td'))).toHaveLength(2)
			})
		})

		it('In first td la-circle with color = record.category.color should be', async () => {
			rows.map((row, index) => {
				const cell = row.queryAll(By.css('td'))[0]

				const element = cell.query(By.css('la-cicle'))

				// TODO: Remove color from attr
				expect(element.nativeElement.getAttribute('color')).toBe(
					defaultInputs.statistic?.[index].category.color
				)
			})
		})

		it('In first td text = "{record.category.name}" on dimension is undefined or "{record.category.name} ({record.category.dimension})" should be on record.category.dimension is not undefined', async () => {
			rows.map((row, index) => {
				const cell = row.queryAll(By.css('td'))[0]

				const categoryData = defaultInputs.statistic?.[index].category
				const textContent = cell.nativeElement.textContent.trim()

				if (categoryData?.dimension) {
					expect(textContent).toContain(
						`${categoryData?.name}  (${categoryData?.dimension})`
					)
					return
				}

				expect(textContent).toBe(categoryData?.name)
			})
		})

		it('In second td text = "{count}" on mode = "number" or "H:mm:ss" should be on record.category.mode = "time"', async () => {
			rows.map((row, index) => {
				const cell = row.queryAll(By.css('td'))[1]

				const statisticData = defaultInputs.statistic?.[index]
				const textContent = cell.nativeElement.textContent.trim()

				if (statisticData?.category.mode === 'number') {
					expect(textContent).toBe(String(statisticData?.count))
					return
				}

				expect(textContent).toBe(
					new Date((statisticData?.count || -1) * 1000).toLocaleTimeString(
						'en-GB',
						{
							hour: 'numeric',
							minute: '2-digit',
							second: '2-digit',
							timeZone: 'UTC'
						}
					)
				)
			})
		})
	})
})
