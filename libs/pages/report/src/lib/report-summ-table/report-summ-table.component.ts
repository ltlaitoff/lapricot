import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { CicleComponent } from '@lapricot/ui'
import { SummStatistic } from '../types/SummStatistic.types'

@Component({
	selector: 'la-report-summ-table',
	standalone: true,
	imports: [CommonModule, CicleComponent],
	templateUrl: './report-summ-table.component.html'
})
export class ReportSummTableComponent {
	@Input() statistic: SummStatistic | null = null
}
