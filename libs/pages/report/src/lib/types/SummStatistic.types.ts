import { CategoryStateItem } from '@lapricot/client-lib/store'

export type SummStatisticItem = { category: CategoryStateItem; count: number }

export type SummStatistic = SummStatisticItem[]
