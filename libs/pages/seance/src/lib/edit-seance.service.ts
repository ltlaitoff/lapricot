import { Injectable } from '@angular/core'
import { SeanceStateItemWithCategory } from '@lapricot/client-lib/store'

@Injectable({
	providedIn: 'root'
})
export class EditSeanceService {
	seance: SeanceStateItemWithCategory | null = null
	status: 'edit' | null = null
}
