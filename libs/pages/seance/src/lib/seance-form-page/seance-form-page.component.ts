import { CdkDragDrop, DragDropModule } from '@angular/cdk/drag-drop'
import { CommonModule } from '@angular/common'
import { Component, OnDestroy, OnInit } from '@angular/core'
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	ReactiveFormsModule,
	Validators
} from '@angular/forms'
import { Router, RouterModule } from '@angular/router'
import { FormCloseDirective } from '@lapricot/client-lib/directives'
import {
	CategoryGroupsStateItem,
	CategoryStateItem,
	RootState,
	SeanceActions,
	SeanceStateItemWithCategory,
	selectCategories,
	selectCategoryGroups
} from '@lapricot/client-lib/store'
import { UpdateSeance } from '@lapricot/shared/types'
import {
	CicleComponent,
	ColorSelectComponent,
	PanelFormComponent,
	PanelFormItemComponent
} from '@lapricot/ui'
import { Store } from '@ngrx/store'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { EditSeanceService } from '../edit-seance.service'
import { CategorySelectComponent } from '@lapricot/pages/categories'

@Component({
	selector: 'la-seance-form-page',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
		DragDropModule,
		ReactiveFormsModule,
		FormCloseDirective,
		CicleComponent,
		ColorSelectComponent,
		PanelFormComponent,
		PanelFormItemComponent,
		CategorySelectComponent
	],
	templateUrl: './seance-form-page.component.html',
	styleUrls: ['./seance-form-page.component.scss']
})
export class SeanceFormPageComponent implements OnInit, OnDestroy {
	constructor(
		private store: Store<RootState>,
		private fb: FormBuilder,
		private router: Router,
		private editSeanceService: EditSeanceService
	) {
		let data: Omit<SeanceStateItemWithCategory, 'color'> & {
			color: null | string
		} = {
			name: '',
			comment: '',
			color: null,
			records: [],
			_id: '',
			order: -1,
			archived: false,
			trash: false,
			trash_expires: -1
		}

		if (
			this.editSeanceService.status === 'edit' &&
			this.editSeanceService.seance
		) {
			this.type = 'edit'
			data = this.editSeanceService.seance
			this.oldData = this.editSeanceService.seance

			this.editSeanceService.seance = null
			this.editSeanceService.status = null
		}

		const records = data.records.map(item => {
			return new FormGroup({
				category: new FormControl<string | null>(
					item.category._id,
					Validators.required
				),
				count: new FormControl<number>(item.count, [
					Validators.required,
					Validators.min(0),
					Validators.max(999)
				])
			})
		})

		this.formData = new FormGroup({
			name: new FormControl<string>(data.name, [
				Validators.required,
				Validators.maxLength(60)
			]),
			comment: new FormControl<string>(data.comment, [
				Validators.maxLength(180)
			]),
			color: new FormControl<string | null>(data.color, [Validators.required]),
			records: this.fb.array(records, [Validators.required])
		})
	}

	type: 'add' | 'edit' = 'add'

	categories: CategoryStateItem[] | null = null
	categoryGroups: CategoryGroupsStateItem[] | null = null
	oldData: SeanceStateItemWithCategory | null = null

	formData: FormGroup<{
		name: FormControl<string | null>
		comment: FormControl<string | null>
		color: FormControl<string | null>
		records: FormArray<
			FormGroup<{
				category: FormControl<string | null>
				count: FormControl<number | null>
			}>
		>
	}>
	pickColorMenuOpened = false

	showMenu: number | null = null

	errorMessage: string | null = null

	private selectCategoriesSubscription: Subscription
	private selectCategoryGroupsSubscription: Subscription
	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		this.selectCategoriesSubscription = this.store
			.select(selectCategories)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				if (JSON.stringify(value) !== JSON.stringify(this.categories)) {
					this.categories = value
				}
			})

		this.selectCategoryGroupsSubscription = this.store
			.select(selectCategoryGroups)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				if (JSON.stringify(value) !== JSON.stringify(this.categoryGroups)) {
					this.categoryGroups = value
				}
			})

		this.formDataValueChangesSubscription =
			this.formData.valueChanges.subscribe(() => {
				this.errorMessage = this.checkFormDataOnErrors()
			})
	}

	ngOnDestroy() {
		this.selectCategoriesSubscription.unsubscribe()
		this.selectCategoryGroupsSubscription.unsubscribe()
		this.formDataValueChangesSubscription.unsubscribe()
	}

	checkFormDataOnErrors() {
		if (this.formData.valid) {
			return null
		}

		const name = this.formData.get('name')

		if (name && name.invalid && (name.dirty || name.touched)) {
			if (name.errors?.['required']) {
				return 'Name is a required field'
			}

			if (name.errors?.['maxlength']) {
				return 'Name must be no more than 60 characters long'
			}
		}

		const comment = this.formData.get('comment')

		if (comment && comment.invalid && (comment.dirty || comment.touched)) {
			if (comment.errors?.['maxlength']) {
				return 'Comment must be no more than 180 characters long'
			}
		}

		const records = this.formData.get('records') as FormArray<
			FormGroup<{
				category: FormControl<string | null>
				count: FormControl<number | null>
			}>
		>

		if (records && records.invalid && (records.dirty || records.touched)) {
			if (records.errors?.['required']) {
				return 'Records is a required'
			}

			for (let i = 0; i < records.length; i++) {
				const element = records.at(i)

				const category = element.get('category')

				if (
					category &&
					category.invalid &&
					(category.dirty || category.touched)
				) {
					if (category && category.errors?.['required']) {
						return 'Category in all records is a required'
					}
				}

				const count = element.get('count')

				if (count && count.invalid && (count.dirty || count.touched)) {
					if (count.errors?.['required']) {
						return 'Count in all records is a required'
					}

					if (count.errors?.['max']) {
						return 'Count max is 1000'
					}

					if (count.errors?.['min']) {
						return 'Count min is 0'
					}
				}
			}
		}

		return null
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			} else if (control instanceof FormArray) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })

				for (let i = 0; i < control.length; i++) {
					this.validateAllFormFields(control.at(i) as FormGroup)
				}
			}
		})
	}

	onSubmitForm() {
		if (this.formData.invalid) {
			this.validateAllFormFields(this.formData)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		const formDataValue = structuredClone(
			this.formData.value
		) as typeof this.formData.value

		if (formDataValue.color == null) return
		if (formDataValue.name == null || formDataValue.name === '') return
		if (formDataValue.records == null || formDataValue.records.length === 0)
			return

		const recordsForSend = formDataValue.records
			.map(item => {
				if (item.category == null) return undefined
				if (item.count == null) return undefined

				return {
					category: item.category,
					count: item.count
				}
			})
			.filter(item => {
				return item != undefined
			}) as {
			category: string
			count: number
		}[]

		const resultData = {
			color: formDataValue.color,
			name: formDataValue.name,
			comment: formDataValue.comment || '',
			records: recordsForSend
		}

		if (this.type === 'add') {
			this.store.dispatch(SeanceActions.add(resultData))
		}

		if (this.type === 'edit' && this.oldData) {
			this.store.dispatch(
				SeanceActions.update({
					old: this.oldData,
					dataForUpdate: resultData as UpdateSeance
				})
			)
		}

		this.router.navigate(['/seance'])
	}

	get records() {
		return this.formData.get('records') as FormArray
	}

	addNewRecord() {
		const array = this.formData.get('records') as FormArray

		array.push(
			new FormGroup({
				category: new FormControl<string | null>(null, Validators.required),
				count: new FormControl<number>(0, Validators.required)
			})
		)
	}

	openPickColorMenu() {
		this.pickColorMenuOpened = true
	}

	closePickColorMenu() {
		this.pickColorMenuOpened = false
	}

	onControlButtonClick(index: number) {
		if (this.showMenu === index) {
			this.showMenu = null
			return
		}

		this.showMenu = index
	}

	stopPropagation(e: Event) {
		e.stopPropagation()
	}

	deleteRecord(index: number) {
		const array = this.formData.get('records') as FormArray
		array.removeAt(index)
		this.showMenu = null
	}

	onFormClickedOutside(index: number) {
		if (this.showMenu !== index) {
			return
		}

		this.showMenu = null
	}

	drop(event: CdkDragDrop<unknown>) {
		const array = this.formData.get('records') as FormArray

		const arrayValue: Array<unknown> = array.value

		arrayValue.splice(
			event.currentIndex,
			0,
			arrayValue.splice(event.previousIndex, 1)[0]
		)

		array.setValue(arrayValue)
	}
}
