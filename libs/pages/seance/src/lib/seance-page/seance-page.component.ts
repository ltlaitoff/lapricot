import { CdkDrag, CdkDragDrop, DragDropModule } from '@angular/cdk/drag-drop'
import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router, RouterModule } from '@angular/router'
import {
	LoadStatus,
	RootState,
	SeanceActions,
	SeanceStateItemWithCategory,
	selectSeance,
	selectSeanceStatus
} from '@lapricot/client-lib/store'
import { SeanceActiveActions } from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { EditSeanceService } from '../edit-seance.service'
import {
	CicleComponent,
	LoadStatusButtonComponent,
	PanelFormComponent,
	PanelFormItemComponent,
	TableControlButtonComponent
} from '@lapricot/ui'
import { CommonModule } from '@angular/common'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { FormCloseDirective } from '@lapricot/client-lib/directives'

@Component({
	selector: 'la-seance-page',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
		CicleComponent,
		AngularSvgIconModule,
		LoadStatusButtonComponent,
		DragDropModule,
		CdkDrag,
		TableControlButtonComponent,
		PanelFormComponent,
		PanelFormItemComponent,
		FormCloseDirective
	],
	templateUrl: './seance-page.component.html',
	styleUrls: ['./seance-page.component.scss']
})
export class SeancePageComponent implements OnInit, OnDestroy {
	currentStatus: LoadStatus = LoadStatus.NOT_SYNCHRONIZED
	seance: SeanceStateItemWithCategory[] | null = null

	type: 'normal' | 'archive' | 'all' = 'normal'

	showMenu: string | null = null

	private selectStatusSubscription: Subscription
	private selectSeanceSubscription: Subscription

	ngOnInit() {
		this.selectSeanceSubscription = this.store
			.select(selectSeance)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.seance = value
			})

		this.selectStatusSubscription = this.store
			.select(selectSeanceStatus)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.currentStatus = value
			})
	}

	ngOnDestroy() {
		this.selectStatusSubscription.unsubscribe()
		this.selectSeanceSubscription.unsubscribe()
	}

	forceReload() {
		this.store.dispatch(SeanceActions.load({ force: true }))
	}

	startSeance(data: SeanceStateItemWithCategory) {
		this.store.dispatch(SeanceActiveActions.startSeance(data))
		this.router.navigate(['/'])
	}

	onControlButtonClick(index: string) {
		if (this.showMenu === index) {
			this.showMenu = null
			return
		}

		this.showMenu = index
	}

	onFormClickedOutside(index: string) {
		if (this.showMenu !== index) {
			return
		}

		this.showMenu = null
	}

	stopPropagation(e: Event) {
		e.stopPropagation()
	}

	editSeanceItem(data: SeanceStateItemWithCategory) {
		this.editSeanceService.seance = data
		this.editSeanceService.status = 'edit'

		this.router.navigate(['/seance/form'])
	}

	deleteSeanceItem(data: SeanceStateItemWithCategory) {
		this.store.dispatch(SeanceActions.delete(data))
	}

	sortedByOrder<T extends { order: number }>(
		array: Array<T> | null
	): Array<T> | null {
		if (!array) return null

		return [...array].sort((a, b) => {
			return a.order > b.order ? 1 : -1
		})
	}

	getSeancesForSort() {
		if (this.seance === null) return this.seance
		if (this.type === 'all') return this.seance

		const currentCategoriesLocal = this.type !== 'normal'

		return this.seance.filter(item => {
			return item.archived === currentCategoriesLocal
		})
	}

	get sortedByOrderSeances() {
		const seancesForSort = this.getSeancesForSort()
		return this.sortedByOrder(seancesForSort)
	}

	drop(
		event: CdkDragDrop<
			SeanceStateItemWithCategory[],
			SeanceStateItemWithCategory[],
			SeanceStateItemWithCategory
		>
	) {
		if (this.sortedByOrderSeances === null) return
		if (event.previousIndex === event.currentIndex) return

		const data = event.item.data
		const previousIndex = data.order
		const currentIndex = this.sortedByOrderSeances[event.currentIndex].order

		this.store.dispatch(
			SeanceActions.reorder({
				data: data,
				previousIndex: previousIndex,
				currentIndex: currentIndex
			})
		)
	}

	checkSeanceIsCanStart(seance: SeanceStateItemWithCategory) {
		if (seance.records.length <= 0) return false

		const validRecords = seance.records.filter(
			record =>
				record.count > 0 &&
				record.category !== null &&
				record.category.trash === false
		)

		return validRecords.length > 0
	}

	constructor(
		private store: Store<RootState>,
		private editSeanceService: EditSeanceService,
		private router: Router
	) {}
}
