import { Component, OnDestroy, OnInit } from '@angular/core'
import {
	FormControl,
	FormGroup,
	ReactiveFormsModule,
	Validators
} from '@angular/forms'
import { Subscription } from 'rxjs'
import { AuthGuardService } from '@lapricot/client-lib/auth'
import { User } from '@lapricot/shared/types'
import { ApiService } from '@lapricot/client-lib/data-access'
import { Router } from '@angular/router'
import { CommonModule } from '@angular/common'

@Component({
	selector: 'la-settings-page',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule],
	templateUrl: './settings-page.component.html',
	styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent implements OnInit, OnDestroy {
	constructor(
		private authGuardService: AuthGuardService,
		private api: ApiService,
		private router: Router
	) {}

	formData = new FormGroup({
		name: new FormControl<string>('', [
			Validators.required,
			Validators.minLength(2),
			Validators.maxLength(30)
		]),
		nickname: new FormControl<string>('', [
			Validators.required,
			Validators.minLength(2),
			Validators.maxLength(25)
		])
	})

	errorMessage: string | null = null
	userData: User | null

	private userDataSubscription: Subscription
	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		this.userDataSubscription = this.authGuardService.authGuardData.subscribe(
			value => {
				if (value.authorized == false) {
					this.userData = null
					return
				}

				this.userData = value

				this.formData.setValue({
					name: value.name,
					nickname: value.nickname
				})
			}
		)

		this.formDataValueChangesSubscription =
			this.formData.valueChanges.subscribe(() => {
				this.errorMessage = this.checkFormDataOnErrors()
			})
	}

	ngOnDestroy() {
		this.userDataSubscription.unsubscribe()
		this.formDataValueChangesSubscription.unsubscribe()
	}

	checkFormDataOnErrors() {
		if (this.formData.valid) {
			return null
		}

		const name = this.formData.get('name')

		if (name && name.invalid && (name.dirty || name.touched)) {
			if (name.errors?.['required']) {
				return 'Name is a required field'
			}

			if (name.errors?.['minlength']) {
				return 'Name must be bigger than 2 characters long'
			}

			if (name.errors?.['maxlength']) {
				return 'Name must be no more than 30 characters long'
			}
		}

		const nickname = this.formData.get('nickname')

		if (nickname && nickname.invalid && (nickname.dirty || nickname.touched)) {
			if (nickname.errors?.['required']) {
				return 'Nickname is a required field'
			}

			if (nickname.errors?.['minlength']) {
				return 'Nickname must be bigger than 2 characters long'
			}

			if (nickname.errors?.['maxlength']) {
				return 'Nickname must be no more than 25 characters long'
			}
		}

		return null
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			}
		})
	}

	onSubmitAddForm() {
		if (this.formData.invalid) {
			this.validateAllFormFields(this.formData)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		const name = this.formData.value.name
		const nickname = this.formData.value.nickname

		if (!name || !nickname) return

		if (name === this.userData?.name && nickname === this.userData?.nickname) {
			this.errorMessage = 'Name and nickname is same with exists!'
			return
		}

		this.api
			.updateUserData$({
				name: name,
				nickname: nickname
			})
			.subscribe(() => {
				this.authGuardService.initialize()

				this.router.navigate([`/profile/${nickname}`])
			})
	}
}
