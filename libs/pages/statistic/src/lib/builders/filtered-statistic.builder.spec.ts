import {
	StatisticStateItemWithCategory,
	CategoryStateItem
} from '@lapricot/client-lib/store'
import { FilteredStatisticBuilder } from './filtered-statistic.builder'
import { Comment, DateControl, Mode } from '../statistic.types'

global.structuredClone = (val: unknown) => JSON.parse(JSON.stringify(val))

const CATEGORY_ONE: CategoryStateItem = {
	_id: 'first_category',
	name: 'first_category',
	mode: 'number',
	comment: 'first_category_comment',
	color: 'first_category_color',
	order: 0,
	dimension: '',
	group: [],
	archived: false,
	trash: false,
	trash_expires: -1
}

const CATEGORY_TWO: CategoryStateItem = {
	_id: 'two_category',
	name: 'two_category',
	mode: 'time',
	comment: 'two_category_comment',
	color: 'two_category_color',
	order: 0,
	dimension: '',
	group: [],
	archived: false,
	trash: false,
	trash_expires: -1
}

const DEFAULT_STATISTIC_ONE: StatisticStateItemWithCategory = {
	_id: '1',
	date: '',
	count: 1,
	comment: 'exists',
	category: CATEGORY_ONE,
	trash: false,
	trash_expires: -1
}

const DEFAULT_STATISTIC_TWO: StatisticStateItemWithCategory = {
	_id: '2',
	date: '',
	count: 2,
	comment: '',
	category: CATEGORY_TWO,
	trash: false,
	trash_expires: -1
}

type COMMENTS_TEST_ITEM = [
	StatisticStateItemWithCategory[],
	Comment,
	StatisticStateItemWithCategory[]
]

type MODE_TEST_ITEM = [
	StatisticStateItemWithCategory[],
	Mode,
	StatisticStateItemWithCategory[]
]

describe('FilteredStatisticBuilder', () => {
	it('Array sends in constructor and array returns by build should be not equals(have same link to memory)', () => {
		const array: StatisticStateItemWithCategory[] = []

		expect(new FilteredStatisticBuilder(array).build()).not.toBe(array)
	})

	it.each([
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'all',
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO]
		],
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'with',
			[DEFAULT_STATISTIC_ONE]
		],
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'without',
			[DEFAULT_STATISTIC_TWO]
		]
	] as COMMENTS_TEST_ITEM[])(
		'On statistic = %j and comment = "%s", build after filterComments should return %j',
		(statistic, comment, result) => {
			const classResult = new FilteredStatisticBuilder(statistic)
				.filterComments(comment)
				.build()

			expect(classResult).toEqual(result)
		}
	)

	it.each([
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'all',
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO]
		],
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'number',
			[DEFAULT_STATISTIC_ONE]
		],
		[
			[DEFAULT_STATISTIC_ONE, DEFAULT_STATISTIC_TWO],
			'time',
			[DEFAULT_STATISTIC_TWO]
		]
	] as MODE_TEST_ITEM[])(
		'On statistic = %j and comment = "%s", build after filterComments should return %j',
		(statistic, comment, result) => {
			const classResult = new FilteredStatisticBuilder(statistic)
				.filterMode(comment)
				.build()

			expect(classResult).toEqual(result)
		}
	)
})
