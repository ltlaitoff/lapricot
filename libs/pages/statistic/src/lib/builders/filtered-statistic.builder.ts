import { StatisticStateItemWithCategory } from '@lapricot/client-lib/store'
import { InitialControls } from '../statistic.config'
import { Comment, DateControl, Mode } from '../statistic.types'

export class FilteredStatisticBuilder {
	private statistic: StatisticStateItemWithCategory[] = []

	constructor(initialStatistic: StatisticStateItemWithCategory[]) {
		this.statistic = structuredClone(initialStatistic)
	}

	filterComments(comment: Comment) {
		if (comment === 'all') return this

		this.statistic = this.statistic.filter(item => {
			return comment === 'with'
				? item.comment.length > 0
				: item.comment.length === 0
		})

		return this
	}

	filterMode(mode: Mode) {
		if (mode === 'all') return this

		this.statistic = this.statistic.filter(item => mode === item.category.mode)

		return this
	}

	filterDate(mode: DateControl, data: InitialControls) {
		if (mode === 'all') return this

		const topDate = this.filterByDateGetTopDate(mode, data)
		const lowerDate = this.filterByDateGetLowerDate(mode, data)

		this.statistic = this.statistic.filter(item => {
			const date = new Date(item.date)

			return (
				date.getTime() > lowerDate.getTime() &&
				date.getTime() < topDate.getTime()
			)
		})

		return this
	}

	private filterByDateGetTopDate(mode: DateControl, data: InitialControls) {
		if (mode === 'to-date' && data.toDateDate) {
			return new Date(data.toDateDate)
		}

		if (mode === 'beetween-dates' && data.beetweenDatesEndDate) {
			return new Date(data.beetweenDatesEndDate)
		}

		const date = new Date(Date.now())

		date.setHours(0)
		date.setMinutes(0)
		date.setSeconds(0)

		let topDateDelay = -1

		if (mode === 'prev-month') {
			topDateDelay = 31
		}

		date.setDate(date.getDate() - topDateDelay)

		return date
	}

	private filterByDateGetLowerDate(mode: DateControl, data: InitialControls) {
		if (mode === 'from-date' && data.fromDateDate) {
			return new Date(data.fromDateDate)
		}

		if (mode === 'beetween-dates' && data.beetweenDatesStartDate) {
			return new Date(data.beetweenDatesStartDate)
		}

		const date = new Date(Date.now())

		date.setHours(0)
		date.setMinutes(0)
		date.setSeconds(0)

		let lowerDateDelay = date.getTime() / 1000 / 60 / 60 / 60

		if (mode === 'week') lowerDateDelay = 6
		if (mode === 'month') lowerDateDelay = 31
		if (mode === 'prev-month') {
			lowerDateDelay = 61
		}

		date.setDate(date.getDate() - lowerDateDelay)

		return date
	}

	filterCategories(categories: InitialControls['categories']) {
		if (categories.length === 0) return this

		this.statistic = this.statistic.filter(item => {
			return categories.includes(item.category._id)
		})

		return this
	}

	build() {
		return this.statistic
	}
}
