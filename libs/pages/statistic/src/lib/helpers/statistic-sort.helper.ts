import { StatisticStateItemWithCategory } from '@lapricot/client-lib/store'
import { StatisticSorting } from '../statistic.config'

export function sortStatistic(
	statistics: StatisticStateItemWithCategory[],
	sorting: StatisticSorting
) {
	return structuredClone(statistics).sort(getSortFunction(sorting))
}

function getSortFunction(sorting: StatisticSorting) {
	return (
		a: StatisticStateItemWithCategory,
		b: StatisticStateItemWithCategory
	): number => {
		switch (sorting) {
			case 'count_asc': {
				return asc(a.count, b.count)
			}
			case 'count_desc': {
				return desc(a.count, b.count)
			}
			case 'category_asc': {
				return asc(a.category.order, b.category.order)
			}
			case 'category_desc': {
				return desc(a.category.order, b.category.order)
			}
			case 'comment_asc': {
				return asc(a.comment.length, b.comment.length)
			}
			case 'comment_desc': {
				return desc(a.comment.length, b.comment.length)
			}
			case 'date_asc': {
				return asc(new Date(a.date).getTime(), new Date(b.date).getTime())
			}
			case 'date_desc': {
				return desc(new Date(a.date).getTime(), new Date(b.date).getTime())
			}
		}
	}
}

function asc(a: number, b: number) {
	return b - a
}

function desc(a: number, b: number) {
	return a - b
}
