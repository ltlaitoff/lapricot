import { Mode } from '../statistic.types'

export type ChartDataset = {
	id: string
	name: string
	data: Array<{ x: number; y: number }>
	colorHEX: string
	mode: Mode
}
