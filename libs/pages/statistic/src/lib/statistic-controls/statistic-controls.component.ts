import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output
} from '@angular/core'
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms'
import {
	ChartInterval,
	ChartBy,
	Mode,
	DateControl,
	Comment
} from '../statistic.types'
import {
	InitialControls,
	INITIAL_CONTROLS,
	StatisticSorting
} from '../statistic.config'
import { Store } from '@ngrx/store'
import {
	RootState,
	selectCategories,
	CategoryStateItem,
	selectCategoryGroups,
	CategoryGroupsStateItem
} from '@lapricot/client-lib/store'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { CommonModule } from '@angular/common'
import { CategorySelectMultiComponent } from '@lapricot/pages/categories'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatNativeDateModule } from '@angular/material/core'
import { MatInputModule } from '@angular/material/input'

@Component({
	selector: 'la-statistic-controls',
	standalone: true,
	imports: [
		CommonModule,
		ReactiveFormsModule,
		CategorySelectMultiComponent,
		MatNativeDateModule,
		MatInputModule,
		MatFormFieldModule,
		MatDatepickerModule
	],
	templateUrl: './statistic-controls.component.html'
})
export class StatisticControlsComponent implements OnInit, OnDestroy {
	@Input() showChartStatisticControls = true
	@Input() formData: InitialControls = structuredClone(INITIAL_CONTROLS)
	@Output() formDataChange = new EventEmitter<InitialControls>()

	form = new FormGroup({
		'chart-interval': new FormControl<ChartInterval>(
			INITIAL_CONTROLS['chart-interval']
		),
		'chart-by': new FormControl<ChartBy>(INITIAL_CONTROLS['chart-by']),
		categories: new FormControl<string[]>(
			structuredClone(INITIAL_CONTROLS.categories)
		),
		mode: new FormControl<Mode>(INITIAL_CONTROLS.mode),
		comment: new FormControl<Comment>(INITIAL_CONTROLS.comment),
		date: new FormControl<DateControl>(INITIAL_CONTROLS.date),
		fromDateDate: new FormControl<Date | null>(null),
		toDateDate: new FormControl<Date | null>(null),
		beetweenDatesStartDate: new FormControl<Date | null>(null),
		beetweenDatesEndDate: new FormControl<Date | null>(null),
		sorting: new FormControl<StatisticSorting>(INITIAL_CONTROLS.sorting)
	})

	categories: CategoryStateItem[] | null = null
	categoryGroups: CategoryGroupsStateItem[] | null = null

	private selectCategoriesSubscription: Subscription
	private selectCategoryGroupsSubscription: Subscription
	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		this.formDataChange.emit(this.form.value as InitialControls)

		this.formDataValueChangesSubscription = this.form.valueChanges.subscribe(
			() => {
				this.formDataChange.emit(this.form.value as InitialControls)
			}
		)

		this.selectCategoriesSubscription = this.store
			.select(selectCategories)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categories = value
			})

		this.selectCategoryGroupsSubscription = this.store
			.select(selectCategoryGroups)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categoryGroups = value
			})
	}

	ngOnDestroy() {
		this.selectCategoriesSubscription.unsubscribe()
		this.selectCategoryGroupsSubscription.unsubscribe()
		this.formDataValueChangesSubscription.unsubscribe()
	}

	constructor(private store: Store<RootState>) {}
}
