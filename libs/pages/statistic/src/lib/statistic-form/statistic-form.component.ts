import {
	Component,
	Input,
	Output,
	EventEmitter,
	OnInit,
	OnDestroy
} from '@angular/core'
import {
	FormControl,
	FormGroup,
	ReactiveFormsModule,
	Validators
} from '@angular/forms'
import { CommonModule } from '@angular/common'
import { Store } from '@ngrx/store'
import { CategoryGroupsStateItem, RootState } from '@lapricot/client-lib/store'
import {
	CategoryStateItem,
	selectCategories,
	selectCategoryGroups
} from '@lapricot/client-lib/store'
import { CreateStatisticDate } from '@lapricot/shared/types'
import { formatDateToDateTimeLocalInput } from '../helpers'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { TimeNumberPickerComponent } from '@lapricot/ui'
import { CategorySelectComponent } from '@lapricot/pages/categories'

@Component({
	selector: 'la-statistic-form',
	standalone: true,
	imports: [
		CommonModule,
		ReactiveFormsModule,
		TimeNumberPickerComponent,
		CategorySelectComponent
	],
	templateUrl: './statistic-form.component.html',
	styleUrls: ['./statistic-form.component.scss']
})
export class StatisticFormComponent implements OnInit, OnDestroy {
	@Input({ required: true }) initialFormData: {
		date: string
		count: number
		comment: string
		category: string
	} | null = null
	@Input({ required: true }) categoryMode: 'number' | 'time' = 'number'

	@Input() fromType: 'add' | 'edit' = 'add'

	@Output() formSubmit = new EventEmitter<CreateStatisticDate>()

	formData = new FormGroup({
		count: new FormControl<number>(0, [
			Validators.required,
			Validators.pattern('^[-+]?[0-9]*$')
		]),
		comment: new FormControl<string>('', Validators.maxLength(150)),
		category: new FormControl<string | null>(null, Validators.required),
		date: new FormControl<string>('', Validators.required)
	})

	categories: CategoryStateItem[] | null = null
	categoryGroups: CategoryGroupsStateItem[] | null = null
	localSelectCategoriesDropDownStatus = false

	errorMessage: string | null = null

	private selectCategoriesSubscription: Subscription
	private selectCategoryGroupsSubscription: Subscription
	private formDataValueChangesSubscription: Subscription

	ngOnInit() {
		this.selectCategoriesSubscription = this.store
			.select(selectCategories)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categories = value
			})

		this.selectCategoryGroupsSubscription = this.store
			.select(selectCategoryGroups)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categoryGroups = value
			})

		if (!this.initialFormData) {
			throw new Error('initialFormData on edit stastistic required!')
		}

		this.initialFormData.date = formatDateToDateTimeLocalInput(
			new Date(this.initialFormData.date)
		)

		this.formData.setValue(this.initialFormData)

		this.formDataValueChangesSubscription =
			this.formData.valueChanges.subscribe(() => {
				this.errorMessage = this.checkFormDataOnErrors()
			})
	}

	ngOnDestroy() {
		this.selectCategoriesSubscription.unsubscribe()
		this.selectCategoryGroupsSubscription.unsubscribe()
		this.formDataValueChangesSubscription.unsubscribe()
	}

	checkFormDataOnErrors() {
		if (this.formData.valid) {
			return null
		}

		const count = this.formData.get('count')

		if (count && count.invalid && (count.dirty || count.touched)) {
			if (count.errors?.['required']) {
				return 'Count is a required field'
			}

			return 'Count is invalid'
		}

		const comment = this.formData.get('comment')

		if (comment && comment.invalid && (comment.dirty || comment.touched)) {
			if (comment.errors?.['maxlength']) {
				return 'Comment must be no more than 150 characters long'
			}
		}

		const category = this.formData.get('category')

		if (category && category.invalid && (category.dirty || category.touched)) {
			if (category.errors?.['required']) {
				return 'Category is a required field'
			}
		}

		const date = this.formData.get('date')

		if (date && date.invalid && (date.dirty || date.touched)) {
			if (date.errors?.['required']) {
				return 'Date is a required field'
			}
		}

		return null
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field)

			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true })
				control.markAsDirty({ onlySelf: true })
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control)
			}
		})
	}

	onSubmitAddForm() {
		if (this.formData.invalid) {
			this.validateAllFormFields(this.formData)
			this.errorMessage = this.checkFormDataOnErrors()

			return
		}

		const valueForSend = this.prepareSubmitData(this.formData.value)

		if (valueForSend == null) {
			return
		}

		this.formSubmit.emit(valueForSend)
	}

	private prepareSubmitData(value: typeof this.formData.value) {
		if (
			value.count == null ||
			value.comment == null ||
			!value.date ||
			!value.category
		) {
			return null
		}

		const valueForSend = {
			count: value.count,
			comment: value.comment,
			category: value.category,
			date: new Date(value.date).toISOString(),
			summ: -1
		}

		return valueForSend
	}

	get formTypeIsAdd() {
		return this.fromType === 'add'
	}

	get formTypeIsEdit() {
		return this.fromType === 'edit'
	}

	stopPropagation(event: MouseEvent) {
		event.stopPropagation()
	}

	setLocalSelectCategoriesDropDownStatus(value: boolean) {
		this.localSelectCategoriesDropDownStatus = value
	}

	constructor(private store: Store<RootState>) {}
}
