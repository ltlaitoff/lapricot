const mockedSelectStatisticStatus = () => {
	return null
}

import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { RootState } from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'
import { Subject } from 'rxjs'
import { StatisticLoadingComponent } from './statistic-loading.component'
import { By } from '@angular/platform-browser'

jest.mock('@lapricot/client-lib/store', () => {
	return {
		selectStatisticStatus: mockedSelectStatisticStatus
	}
})

const observableStatus = new Subject()

const mockedStore: Partial<Store<RootState>> = {
	// @ts-expect-error Test
	select(value) {
		if (value === mockedSelectStatisticStatus) {
			return observableStatus
		}

		return null
	}
}

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [StatisticLoadingComponent],
		schemas: [NO_ERRORS_SCHEMA],
		providers: [
			{
				provide: Store,
				useValue: mockedStore
			}
		]
	}).compileComponents()

	const fixture = TestBed.createComponent(StatisticLoadingComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('StatisticLoadingComponent', () => {
	it('Component la-statistic-loading should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-statistic-loading text = "Loading..." should be on status = null', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next(null)
		fixture.detectChanges()

		expect(fixture.nativeElement.textContent.trim()).toBe('Loading...')
	})

	it('In la-statistic-loading text = "Loading..." should be on status = "synchronization"', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next('synchronization')
		fixture.detectChanges()

		expect(fixture.nativeElement.textContent.trim()).toBe('Loading...')
	})

	it('In la-statistic-loading "Statistic not found" in text should be in first div on status != null and status !== "synchronization"', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next('error')
		fixture.detectChanges()

		const element = fixture.debugElement.query(By.css('[data-testid="first"]'))
		expect(element.nativeElement.textContent.trim()).toBe('Statistic not found')
	})

	it('In la-statistic-loading "Go to the home page" in text should be in second div on status != null and status !== "synchronization"', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next('error')
		fixture.detectChanges()

		const element = fixture.debugElement.query(By.css('[data-testid="second"]'))
		expect(element.nativeElement.textContent.trim()).toContain('For add -')
	})

	it('In la-statistic-loading link with text = "Go to the home page" and with href = "/" should be on status != null and status !== "synchronization"', async () => {
		const { fixture } = await initializeComponent()

		observableStatus.next('error')
		fixture.detectChanges()

		const element = fixture.debugElement.query(By.css('[data-testid="second"]'))
		const link = element.query(By.css('a'))

		expect(link.properties.routerLink).toBe('/')
		expect(link.nativeElement.textContent.trim()).toContain(
			'Go to the home page'
		)
	})
})
