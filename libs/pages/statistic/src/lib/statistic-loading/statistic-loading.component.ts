import { CommonModule } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { RouterModule } from '@angular/router'
import {
	LoadStatus,
	RootState,
	selectStatisticStatus
} from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'

@Component({
	selector: 'la-statistic-loading',
	standalone: true,
	imports: [CommonModule, RouterModule],
	templateUrl: './statistic-loading.component.html'
})
export class StatisticLoadingComponent implements OnInit {
	constructor(private store: Store<RootState>) {}

	currentStatus: LoadStatus

	ngOnInit() {
		this.store.select(selectStatisticStatus).subscribe(value => {
			this.currentStatus = value
		})
	}
}
