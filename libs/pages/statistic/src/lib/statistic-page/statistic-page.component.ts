import { Component, OnDestroy, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import {
	selectStatistic,
	StatisticActions,
	RootState,
	StatisticStateItemWithCategory
} from '@lapricot/client-lib/store'

import { InitialControls, INITIAL_CONTROLS } from '../statistic.config'
import { FilteredStatisticBuilder } from '../builders/filtered-statistic.builder'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { CommonModule } from '@angular/common'
import { StatisticChartComponent } from '../statistic-chart/statistic-chart.component'
import { StatisticControlsComponent } from '../statistic-controls/statistic-controls.component'
import { LoadStatusButtonComponent } from '@lapricot/ui'
import { StatisticTableComponent } from '../statistic-table/statistic-table.component'
import { StatisticLoadingComponent } from '../statistic-loading/statistic-loading.component'
import { RouterModule } from '@angular/router'

@Component({
	selector: 'la-statistic-page',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
		StatisticChartComponent,
		StatisticControlsComponent,
		LoadStatusButtonComponent,
		StatisticTableComponent,
		StatisticLoadingComponent
	],
	templateUrl: './statistic-page.component.html'
})
export class StatisticComponent implements OnInit, OnDestroy {
	constructor(private store: Store<RootState>) {}

	statistics: StatisticStateItemWithCategory[] | null = null
	statisticForOutput: StatisticStateItemWithCategory[] | null = null

	formData: InitialControls = structuredClone(INITIAL_CONTROLS)

	private selectStatisticSubscription: Subscription

	ngOnInit() {
		this.selectStatisticSubscription = this.store
			.select(selectStatistic)
			.pipe(distinctUntilChanged())
			.subscribe(newStatistic => {
				this.statistics = newStatistic.sort((a, b) => {
					return new Date(b.date).getTime() - new Date(a.date).getTime()
				})

				this.statisticForOutput = this.updateFilteredStatistic(
					newStatistic,
					this.formData
				)
			})
	}

	ngOnDestroy() {
		this.selectStatisticSubscription.unsubscribe()
	}

	forceReloadStatistic(force: boolean) {
		this.store.dispatch(StatisticActions.load({ force: force }))
	}

	formDataChange(newFormData: InitialControls) {
		if (this.statistics === null) return

		this.formData = newFormData

		this.statisticForOutput = this.updateFilteredStatistic(
			this.statistics,
			newFormData
		)
	}

	private updateFilteredStatistic(
		statistics: StatisticStateItemWithCategory[],
		formData: InitialControls
	): StatisticStateItemWithCategory[] {
		return new FilteredStatisticBuilder(statistics)
			.filterComments(formData.comment)
			.filterMode(formData.mode)
			.filterDate(formData.date, formData)
			.filterCategories(formData.categories)
			.build()
	}
}
