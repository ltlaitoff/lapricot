import {
	Component,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges
} from '@angular/core'
import { CommonModule } from '@angular/common'
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator'
import { RootState } from '@lapricot/client-lib/store'
import {
	StatisticActions,
	StatisticStateItemWithCategory
} from '@lapricot/client-lib/store'
import { CreateStatisticDate } from '@lapricot/shared/types'
import { Store } from '@ngrx/store'
import { sortStatistic } from '../helpers'
import { StatisticSorting } from '../statistic.config'
import {
	INITIAL_PAGINATOR_CONFIG,
	PAGINATOR_PAGE_INDEXS
} from './statistic-table.config'
import {
	CicleComponent,
	NotSyncStatusIconComponent,
	PanelFormComponent,
	PanelFormItemComponent,
	TableControlButtonComponent
} from '@lapricot/ui'
import { StatisticFormComponent } from '../statistic-form/statistic-form.component'
import { FormCloseDirective } from '@lapricot/client-lib/directives'

@Component({
	selector: 'la-statistic-table',
	standalone: true,
	imports: [
		CommonModule,
		CicleComponent,
		MatPaginatorModule,
		NotSyncStatusIconComponent,
		TableControlButtonComponent,
		PanelFormComponent,
		PanelFormItemComponent,
		StatisticFormComponent,
		FormCloseDirective
	],
	templateUrl: './statistic-table.component.html',
	styleUrls: ['./statistic-table.component.scss']
})
export class StatisticTableComponent implements OnChanges, OnInit {
	constructor(private store: Store<RootState>) {}

	@Input({ required: true }) statistics: StatisticStateItemWithCategory[]
	@Input({ required: true }) sorting: StatisticSorting
	@Input() showControl = true

	statisticForOutputInTable: StatisticStateItemWithCategory[]
	paginatorData: PageEvent = INITIAL_PAGINATOR_CONFIG
	paginatorPageIndexs = structuredClone(PAGINATOR_PAGE_INDEXS)
	editStatisticRecordId: string | null = null
	showMenu: string | null = null

	ngOnInit() {
		this.initializePaginator(this.statistics.length)

		this.updateStatisticForOutput()
	}

	ngOnChanges(changes: SimpleChanges) {
		this.updateStatisticForOutput()

		if (changes['statistics']) {
			this.updatePaginatorLength(this.statistics.length)
		}
	}

	closeStatisticEdit() {
		if (this.editStatisticRecordId === null) return

		this.editStatisticRecordId = null
	}

	editStastisticStatus(category: StatisticStateItemWithCategory) {
		if (this.editStatisticRecordId === null) {
			this.editStatisticRecordId = category._id
			return
		}

		this.closeStatisticEdit()
	}

	editStastistic(
		currentValue: StatisticStateItemWithCategory,
		editedValue: CreateStatisticDate
	) {
		this.closeStatisticEdit()

		this.store.dispatch(
			StatisticActions.update({
				oldStatistic: currentValue,
				dataForUpdate: editedValue
			})
		)
	}

	deleteStatisticRecord(statisticRecord: StatisticStateItemWithCategory) {
		this.store.dispatch(StatisticActions.delete(statisticRecord))
	}

	initializePaginator(length: number) {
		this.paginatorData = {
			...INITIAL_PAGINATOR_CONFIG,
			length: length
		}
	}

	updatePaginatorLength(length: number) {
		this.paginatorData.length = length
		this.paginatorData.pageIndex = 0
	}

	handlePageEvent(e: PageEvent) {
		this.paginatorData = e

		if (!this.statistics) return

		this.updateStatisticForOutput()
	}

	private updateStatisticForOutput() {
		this.statisticForOutputInTable = this.paginatorFilter(
			sortStatistic(this.statistics, this.sorting),
			this.paginatorData
		)
	}

	private paginatorFilter(
		statistics: StatisticStateItemWithCategory[],
		{ pageIndex, pageSize }: PageEvent
	) {
		const startPoint = pageIndex * pageSize

		return statistics.slice(startPoint, startPoint + pageSize)
	}

	onControlButtonClick(categoryId: string) {
		if (this.showMenu === null || this.showMenu !== categoryId) {
			this.showMenu = categoryId
			return
		}

		this.showMenu = null
	}

	onFormClickedOutside(categoryId: string) {
		if (this.showMenu !== categoryId) {
			return
		}

		this.showMenu = null
		this.editStatisticRecordId = null
	}

	stopPropagation(event: Event) {
		event.stopPropagation()
	}
}
