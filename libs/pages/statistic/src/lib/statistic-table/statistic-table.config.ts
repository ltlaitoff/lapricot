import { PageEvent } from '@angular/material/paginator'

export const INITIAL_PAGINATOR_CONFIG: PageEvent = {
	pageIndex: 0,
	previousPageIndex: 0,
	pageSize: 50,
	length: 0
}

export const PAGINATOR_PAGE_INDEXS = [25, 50, 75, 100]
