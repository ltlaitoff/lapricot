import {
	ChartInterval,
	ChartBy,
	Mode,
	DateControl,
	Comment
} from './statistic.types'

export type StatisticSorting =
	| 'count_desc'
	| 'count_asc'
	| 'comment_desc'
	| 'comment_asc'
	| 'category_desc'
	| 'category_asc'
	| 'date_desc'
	| 'date_asc'

export type InitialControls = {
	'chart-interval': ChartInterval
	'chart-by': ChartBy
	categories: string[]
	mode: Mode
	comment: Comment
	date: DateControl
	fromDateDate: Date | null
	toDateDate: Date | null
	beetweenDatesStartDate: Date | null
	beetweenDatesEndDate: Date | null
	sorting: StatisticSorting
}

export const INITIAL_CONTROLS: Readonly<InitialControls> = {
	'chart-interval': 'day',
	'chart-by': 'category',
	mode: 'all',
	categories: [],
	comment: 'all',
	date: 'all',
	fromDateDate: null,
	toDateDate: null,
	beetweenDatesStartDate: null,
	beetweenDatesEndDate: null,
	sorting: 'date_desc'
}
