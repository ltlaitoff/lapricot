import { Component, OnDestroy, OnInit } from '@angular/core'
import {
	RootState,
	CategoryStateItem,
	CategoriesActions,
	selectCategoriesTrashed,
	StatisticActions,
	selectStatisticTrashed,
	StatisticStateItemWithCategory,
	SeanceStateItemWithCategory,
	selectSeanceTrashed,
	SeanceActions
} from '@lapricot/client-lib/store'
import { Store } from '@ngrx/store'
import { distinctUntilChanged, Subscription } from 'rxjs'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { FormCloseDirective } from '@lapricot/client-lib/directives'
import { CommonModule } from '@angular/common'
import {
	TableControlButtonComponent,
	PanelFormComponent,
	PanelFormItemComponent,
	CicleComponent,
	NotSyncStatusIconComponent,
	TableColorHeadItemComponent
} from '@lapricot/ui'
@Component({
	selector: 'la-trash-page',
	standalone: true,
	imports: [
		CommonModule,
		TableControlButtonComponent,
		PanelFormComponent,
		PanelFormItemComponent,
		AngularSvgIconModule,
		FormCloseDirective,
		CicleComponent,
		TableColorHeadItemComponent,
		NotSyncStatusIconComponent
	],

	templateUrl: './trash-page.component.html'
})
export class TrashPageComponent implements OnInit, OnDestroy {
	constructor(private store: Store<RootState>) {}

	categories: null | CategoryStateItem[] = null
	seance: null | SeanceStateItemWithCategory[] = null
	statistic: null | StatisticStateItemWithCategory[] = null
	categoriesStatisticCount: Record<string, number> = {}

	showMenu: string | null = null

	private selectCategoriesTrashedSubscription: Subscription
	private selectStatisticTrashedSubscription: Subscription
	private selectSeanceTrashedSubscription: Subscription

	ngOnInit() {
		this.selectSeanceTrashedSubscription = this.store
			.select(selectSeanceTrashed)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.seance = value
			})

		this.selectCategoriesTrashedSubscription = this.store
			.select(selectCategoriesTrashed)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.categories = value
			})

		this.selectStatisticTrashedSubscription = this.store
			.select(selectStatisticTrashed)
			.pipe(distinctUntilChanged())
			.subscribe(value => {
				this.statistic = value.sort(
					// TODO: Move to helpers and write tests
					(a, b) => {
						const aNumber = !a.category.trash
							? 1
							: Number(a.category.trash_expires !== a.trash_expires) - 1

						const bNumber = !b.category.trash
							? 1
							: Number(b.category.trash_expires !== b.trash_expires) - 1

						if (aNumber === bNumber) return 0
						if (aNumber > bNumber) return -1
						return 1
					}
				)

				this.categoriesStatisticCount = this.statistic.reduce((acc, item) => {
					if (!(item.category._id in acc)) {
						acc[item.category._id] = 0
					}

					if (item.category.trash_expires === item.trash_expires) {
						acc[item.category._id] += 1
					}

					return acc
				}, {} as Record<string, number>)
			})
	}

	ngOnDestroy() {
		this.selectSeanceTrashedSubscription.unsubscribe()
		this.selectCategoriesTrashedSubscription.unsubscribe()
		this.selectStatisticTrashedSubscription.unsubscribe()
	}

	get hideStatusCol() {
		return this.statistic?.reduce((acc, record) => {
			if (!record.category.trash) {
				return acc || false
			}

			return acc || true
		}, false)
	}

	onControlButtonClick(categoryId: string) {
		if (this.showMenu === null || this.showMenu !== categoryId) {
			this.showMenu = categoryId
			return
		}

		this.showMenu = null
	}

	onFormClickedOutside(categoryId: string) {
		if (this.showMenu !== categoryId) {
			return
		}

		this.showMenu = null
	}

	stopPropagation(event: Event) {
		event.stopPropagation()
	}

	deleteCategory(category: CategoryStateItem) {
		this.store.dispatch(CategoriesActions.deletePermanent(category))
	}

	restoreCategory(category: CategoryStateItem) {
		this.store.dispatch(CategoriesActions.restore(category))
	}

	deleteStatisticRecord(record: StatisticStateItemWithCategory) {
		this.store.dispatch(StatisticActions.deletePermanent(record))
	}

	restoreStatisticRecord(record: StatisticStateItemWithCategory) {
		this.store.dispatch(
			StatisticActions.update({
				oldStatistic: record,
				dataForUpdate: {
					trash: false,
					trash_expires: -1
				}
			})
		)
	}

	restoreSeance(seance: SeanceStateItemWithCategory) {
		this.store.dispatch(SeanceActions.restore(seance))
	}

	deleteSeance(seance: SeanceStateItemWithCategory) {
		this.store.dispatch(SeanceActions.deletePermanent(seance))
	}

	getStatisticRecordStatus(record: StatisticStateItemWithCategory) {
		if (!record.category.trash)
			return {
				blocked: false,
				disabled: false,
				text: ''
			}

		if (record.category.trash_expires !== record.trash_expires)
			return {
				blocked: true,
				disabled: false,
				text: 'B'
			}

		return {
			blocked: false,
			disabled: true,
			text: 'D'
		}
	}
}
