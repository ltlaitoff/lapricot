export interface Category {
	_id: string
	name: string
	mode: 'number' | 'time'
	comment: string
	color: string
	order: number
	dimension?: string
	group: string[]
	archived: boolean
	trash: boolean
	trash_expires: number
}

export type CategoriesBasicSet = Omit<
	Category,
	'order' | '_id' | 'mode' | 'archived' | 'trash' | 'trash_expires'
> &
	Partial<Pick<Category, 'mode'>>

export interface ReorderCategoryData {
	categoryId: string
	previousIndex: number
	currentIndex: number
}

export type ReorderCategoryReturnData = ReorderCategoryData[]
