export interface CategoryGroup {
	_id: string
	name: string
	order: number
	color: string
}

export type CreateCategoryGroupData = Omit<CategoryGroup, '_id' | 'order'>

export interface ReorderCategoryGroupData {
	categoryGroupId: string
	previousIndex: number
	currentIndex: number
}

export type ReorderCategoryGroupReturnData = ReorderCategoryGroupData[]
