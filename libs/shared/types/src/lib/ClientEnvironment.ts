export interface ClientEnvironment {
	version: string
	type: 'production' | 'dev'
	googleClientId: string
	API_HOST: string
}
