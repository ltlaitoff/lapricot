import { User } from './User'

export type InitializeSuccess = User
export type InitializeFailed = {
	authorized: false
}
