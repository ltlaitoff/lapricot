export interface SeanceRecord {
	category: string
	count: number
	trash: boolean
	trash_expires: number
}

export type CreateSeanceRecord = Omit<SeanceRecord, 'trash' | 'trash_expires'>

export interface Seance {
	_id: string
	name: string
	comment: string
	color: string
	order: number
	records: SeanceRecord[]
	archived: boolean
	trash: boolean
	trash_expires: number
}

export type CreateSeance = Omit<
	Seance,
	'order' | 'records' | '_id' | 'archived' | 'trash' | 'trash_expires'
> & {
	records: CreateSeanceRecord[]
}

export type UpdateSeance = Partial<Omit<Seance, '_id'>>

export interface ReorderSeanceData {
	id: string
	previousIndex: number
	currentIndex: number
}

export type ReorderSeanceReturnData = ReorderSeanceData[]
