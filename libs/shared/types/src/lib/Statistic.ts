export interface Statistic {
	_id: string
	date: string // Date as ISO string
	count: number
	comment: string
	category: string
	trash: boolean
	trash_expires: number
}

export type CreateStatisticDate = Omit<
	Statistic,
	'_id' | 'trash' | 'trash_expires'
>
export type UpdateStatisticData = Partial<Omit<Statistic, '_id'>>
