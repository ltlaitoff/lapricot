export interface UserBase {
	_id: string
	email: string
	family_name: string
	given_name: string
	name: string
	picture: string
	nickname: string
}

export interface User extends UserBase {
	authorized: boolean
	sessionId: string
}
