import { Category } from '../Category'
import { Statistic } from '../Statistic'
import { UserBase } from '../User'

export interface UserProfileBase extends UserBase {
	createdAt: string
}

export interface UserProfile extends UserProfileBase {
	categories: Category[]
	statistic: Statistic[]
}
