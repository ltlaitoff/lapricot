import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { CicleComponent } from './cicle.component'

type ComponentInputs = {
	color: string
	type: 'mini' | 'standart' | 'big'
	applyClass: string
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [CicleComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(CicleComponent)
	const component = fixture.componentInstance

	component.applyClass = inputs.applyClass || ''
	component.color = inputs.color || ''
	component.type = inputs.type || 'standart'

	fixture.detectChanges()

	return { fixture, component }
}

describe('CicleComponent', () => {
	it('should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it.each`
		color
		${'rgb(1, 35, 69)'}
		${'rgb(234, 67, 69)'}
		${'rgb(0, 0, 17)'}
	`(
		'On color = $color, div should has background = $color',
		async ({ color }) => {
			const { fixture } = await initializeComponent({
				color: color
			})

			const div = fixture.debugElement.query(By.css('div'))

			expect(div.styles.backgroundColor).toBe(color)
		}
	)

	it.each`
		applyClass
		${'class1'}
		${'class2 class3'}
		${'class4'}
	`(
		'On applyClass = $applyClass, div should has in classes $applyClass',
		async ({ applyClass }) => {
			const { fixture } = await initializeComponent({
				applyClass: applyClass
			})

			const div = fixture.debugElement.query(By.css('div'))

			const classes = applyClass.split(' ')
			classes.map(classElement => {
				expect(div.classes[classElement]).toBe(true)
			})
		}
	)

	it.todo('Write tests for circle `type` input')
})
