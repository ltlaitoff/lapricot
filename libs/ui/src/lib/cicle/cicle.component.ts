import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'

@Component({
	selector: 'la-cicle',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './cicle.component.html'
})
export class CicleComponent {
	@Input({ required: true }) color: string
	@Input() type: 'mini' | 'standart' | 'big' = 'standart'
	@Input() applyClass = ''
}
