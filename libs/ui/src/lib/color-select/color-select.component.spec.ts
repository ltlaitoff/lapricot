import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ColorSelectComponent } from './color-select.component'

describe('ColorSelectComponent', () => {
	it.todo('Create tests')
	let component: ColorSelectComponent
	let fixture: ComponentFixture<ColorSelectComponent>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ColorSelectComponent],
			imports: [ReactiveFormsModule, FormsModule]
			// schemas: [CUSTOM_ELEMENTS_SCHEMA]
		}).compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(ColorSelectComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
