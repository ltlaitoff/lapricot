import { CommonModule } from '@angular/common'
import { Component, forwardRef } from '@angular/core'
import {
	ControlValueAccessor,
	NG_VALUE_ACCESSOR,
	FormsModule,
	ReactiveFormsModule
} from '@angular/forms'
import { DEFAULT_COLOR, DEFAULT_COLORS } from './color-select.config'

@Component({
	selector: 'la-color-select',
	standalone: true,
	imports: [CommonModule, FormsModule, ReactiveFormsModule],
	templateUrl: './color-select.component.html',
	styleUrls: ['color-select.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => ColorSelectComponent),
			multi: true
		}
	]
})
export class ColorSelectComponent implements ControlValueAccessor {
	standartColors: string[] = DEFAULT_COLORS
	choicedColor: string | null = null
	customColor: string | null = null

	private onChange: ((_: string | null) => void) | null = null
	private onTouched: ((_?: unknown) => void) | null = null

	disabled = false
	touched = false

	private setFirstColorAsChoiced() {
		if (!this.onChange) return
		this.markAsTouched()

		if (this.choicedColor === null) {
			this.choicedColor = DEFAULT_COLOR
		}

		this.onChange(this.choicedColor)
	}

	checkColor(color: string, options?: 'custom' | 'first') {
		if (this.disabled) return

		this.choicedColor = color

		if (options === 'custom') {
			this.customColor = color
		}

		if (options === 'first') {
			if (!this.standartColors.includes(color)) {
				this.customColor = color
			}
		}

		if (this.onTouched) this.markAsTouched()
		if (this.onChange) this.onChange(color)
	}

	writeValue(color: string | null): void {
		if (color === null) {
			this.setFirstColorAsChoiced()

			return
		}

		this.checkColor(color, 'first')
	}

	registerOnChange(callback: (_: string | null) => void): void {
		this.onChange = callback

		this.setFirstColorAsChoiced()
	}

	registerOnTouched(callback: (_: unknown) => void): void {
		this.onTouched = callback
	}

	setDisabledState(disabled: boolean) {
		this.disabled = disabled
	}

	markAsTouched() {
		if (!this.touched && this.onTouched) {
			this.onTouched()
			this.touched = true
		}
	}
}
