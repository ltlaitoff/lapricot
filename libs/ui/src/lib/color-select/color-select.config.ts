export const DEFAULT_COLORS = [
	'#ef4444',
	'#d946ef',
	'#3b82f6',
	'#06b6d4',
	'#10b981',
	'#84cc16',
	'#6b7280',
	'#f59e0b'
]

export const DEFAULT_COLOR = DEFAULT_COLORS[0] || '#ef4444'
