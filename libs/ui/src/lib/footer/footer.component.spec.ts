import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { FooterComponent } from './footer.component'
import { ClientEnvironment } from '@lapricot/shared/types'

jest.mock('./footer.config', () => {
	const GITHUB_CREATOR_ACCOUNT_LINK = 'MOCKED_CREATOR'
	const GITHUB_CREATOR_ACCOUNT_NAME = 'MOCKED'

	const USED_SOURCE_LINKS = [
		{
			icon: 'mocked0.svg',
			link: 'mocked0.link'
		},
		{
			icon: 'mocked1.svg',
			link: 'mocked1.link'
		},
		{
			icon: 'mocked2.svg',
			link: 'mocked2.link'
		}
	]

	return {
		GITHUB_CREATOR_ACCOUNT_LINK,
		GITHUB_CREATOR_ACCOUNT_NAME,
		USED_SOURCE_LINKS
	}
})

type ComponentInputs = {
	version: string
	type: string
}

async function initializeComponent(
	inputs: ComponentInputs = { version: '-', type: '-' }
) {
	await TestBed.configureTestingModule({
		declarations: [FooterComponent],
		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(FooterComponent)
	const component = fixture.componentInstance

	component.version = inputs.version
	component.type = inputs.type as ClientEnvironment['type']

	fixture.detectChanges()

	return { fixture, component }
}

describe('FooterComponent', () => {
	it('should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it.each`
		version    | type
		${'1.0.0'} | ${'dev'}
		${'2.0.0'} | ${'prod'}
	`(
		'FooterComponent should have la-version with attr.test-version = $version and attr.test-type = $type',
		async ({ version, type }) => {
			const { fixture } = await initializeComponent({ version, type })

			const versionElement = fixture.debugElement.query(By.css('la-version'))

			expect(versionElement).not.toBe(null)
			expect(versionElement.attributes['test-version']).toBe(version)
			expect(versionElement.attributes['test-type']).toBe(type)
		}
	)

	describe('Created by', () => {
		it('FooterComponent should have `data-testid="created-by"`', async () => {
			const { fixture } = await initializeComponent()

			const element = fixture.debugElement.query(
				By.css('[data-testid="created-by"]')
			)

			expect(element).not.toBe(null)
		})

		it('Created-by element should have link with href = "MOCKED_CREATOR" and trimmed textContent = "MOCKED"', async () => {
			const { fixture } = await initializeComponent()

			const rootElement = fixture.debugElement.query(
				By.css('[data-testid="created-by"]')
			)

			const element = rootElement.query(By.css('a'))

			expect(element).not.toBe(null)
			expect(element.attributes['href']).toBe('MOCKED_CREATOR')
			expect(element.nativeElement.textContent.trim()).toBe('MOCKED')
		})
	})

	it('FooterComponent should have la-used-source-link in count 3(mocked) with attributes: icon = mocked${index}.svg and link = mocked${index}.link', async () => {
		const { fixture } = await initializeComponent()

		const usedSourceElements = fixture.debugElement.queryAll(
			By.css('la-used-source-link')
		)

		expect(usedSourceElements).not.toBe(null)
		expect(usedSourceElements.length).toBe(3)

		usedSourceElements.map((item, index) => {
			expect(item.attributes['test-icon']).toBe(`mocked${index}.svg`)
			expect(item.attributes['test-link']).toBe(`mocked${index}.link`)
		})
	})
})
