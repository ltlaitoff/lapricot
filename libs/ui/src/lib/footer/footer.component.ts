import { Component, Input } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ClientEnvironment } from '@lapricot/shared/types'
import { UsedSourceLinkComponent } from '../used-source-link/used-source-link.component'
import { VersionComponent } from '../version/version.component'

import {
	GITHUB_CREATOR_ACCOUNT_LINK,
	GITHUB_CREATOR_ACCOUNT_NAME,
	USED_SOURCE_LINKS
} from './footer.config'

@Component({
	selector: 'la-footer',
	standalone: true,
	imports: [CommonModule, VersionComponent, UsedSourceLinkComponent],
	templateUrl: './footer.component.html'
})
export class FooterComponent {
	@Input({ required: true }) version = ''
	@Input({ required: true }) type: ClientEnvironment['type'] = 'dev'

	config = {
		creatorLink: GITHUB_CREATOR_ACCOUNT_LINK,
		creatorName: GITHUB_CREATOR_ACCOUNT_NAME,
		sourceLinks: USED_SOURCE_LINKS
	}
}
