import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { HeaderComponent } from './header.component'
import { User } from '@lapricot/shared/types'

const BURGER_BUTTON_BY = By.css('[data-testid="burger-button"]')
const BURGER_MENU_BY = By.css('[data-testid="burger-menu"]')
const BURGER_MENU_CLOSE_BUTTON_BY = By.css(
	'[data-testid="burger-menu-close-button"]'
)
const BURGER_MENU_CONTENT = By.css('[data-testid="burger-menu-content"]')
const LOGO_BY = By.css('la-logo')
const MENU_BY = By.css('la-menu')
const USER_PANEL_BY = By.css('la-user-panel')

interface ComponentInputs {
	userInfo: User
}

const TEST_USER = {
	_id: 'string',
	email: 'string',
	family_name: 'string',
	given_name: 'string',
	name: 'string',
	picture: 'string',
	authorized: false,
	sessionId: 'string'
}

async function initializeComponent(
	inputs: ComponentInputs = { userInfo: TEST_USER }
) {
	await TestBed.configureTestingModule({
		declarations: [HeaderComponent],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(HeaderComponent)
	const component = fixture.componentInstance

	component.userInfo = inputs.userInfo
	fixture.detectChanges()

	const header = fixture.debugElement.query(By.css('header'))
	return {
		fixture,
		header,
		component,
		detectChanges: fixture.detectChanges
	}
}

function setWindowSize(value: number) {
	global.innerWidth = value
	global.dispatchEvent(new Event('resize'))
}

describe('LogoComponent', () => {
	it('In header component should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In header `header` tag should be', async () => {
		const { header } = await initializeComponent()

		expect(header).toBeTruthy()
	})

	describe('Header on desktop', () => {
		beforeEach(() => {
			setWindowSize(1920)
		})

		it('In header `la-logo` should be', async () => {
			const { header } = await initializeComponent()

			const element = header.query(LOGO_BY)

			expect(element).toBeTruthy()
		})

		it('In header `la-menu` should be', async () => {
			const { header } = await initializeComponent()

			const element = header.query(MENU_BY)

			expect(element).toBeTruthy()
		})

		it('In header `la-user-panel` should be', async () => {
			const { header } = await initializeComponent()

			const element = header.query(USER_PANEL_BY)

			expect(element).toBeTruthy()
		})
	})

	describe('Header on mobile', () => {
		beforeEach(() => {
			setWindowSize(750)
		})

		describe('basic', () => {
			it('In header button for open burger-menu should be in document', async () => {
				const { header } = await initializeComponent()

				const button = header.query(BURGER_BUTTON_BY)

				expect(button).toBeTruthy()
			})

			it('In header burger-menu should be closed by default', async () => {
				const { header } = await initializeComponent()

				const burgerMenu = header.query(BURGER_MENU_BY)

				expect(burgerMenu).not.toBeTruthy()
			})

			it('In header burger-menu should open after click on burger-button', async () => {
				const { fixture, header } = await initializeComponent()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).toBeTruthy()
			})

			it('In header burger-menu should close on open menu and click on burger-close-menu-button', async () => {
				const { fixture, header } = await initializeComponent()

				const button = header.query(BURGER_BUTTON_BY)

				button.triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).toBeTruthy()

				const closeButton = header.query(BURGER_MENU_CLOSE_BUTTON_BY)
				closeButton.triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()
			})

			it('In header burger-menu should close on open menu and click on burger-button second time', async () => {
				const { fixture, header } = await initializeComponent()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()

				const button = header.query(BURGER_BUTTON_BY)
				button.triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).toBeTruthy()

				button.triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()
			})

			it('In header burger-menu-content should be in the opened burger-menu ', async () => {
				const { fixture, header } = await initializeComponent()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const content = header.query(BURGER_MENU_CONTENT)

				expect(content).toBeTruthy()
			})
		})

		describe('logo', () => {
			it('In header la-logo should be in the burger-menu-content', async () => {
				const { fixture, header } = await initializeComponent()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(LOGO_BY)

				expect(element).toBeTruthy()
			})

			it('In header la-logo should has userInfo input with user info data', async () => {
				const { fixture, header } = await initializeComponent({
					userInfo: TEST_USER
				})

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(LOGO_BY)

				expect(element.properties.userInfo).toEqual(TEST_USER)
			})

			it('In header burger-menu should close on call (click) event on la-logo', async () => {
				const { fixture, header } = await initializeComponent({
					userInfo: TEST_USER
				})

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(LOGO_BY)

				element.triggerEventHandler('click')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()
			})
		})

		describe('menu', () => {
			it('In header la-menu should be in the burger-menu-content', async () => {
				const { fixture, header } = await initializeComponent()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(MENU_BY)

				expect(element).toBeTruthy()
			})

			it('In header la-menu should has userInfo input with user info data', async () => {
				const { fixture, header } = await initializeComponent({
					userInfo: TEST_USER
				})

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(MENU_BY)

				expect(element.properties.userInfo).toEqual(TEST_USER)
			})

			it('In header burger-menu should close on call (routeClick) event on la-menu', async () => {
				const { fixture, header } = await initializeComponent({
					userInfo: TEST_USER
				})

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(MENU_BY)

				element.triggerEventHandler('routeClick')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()
			})
		})

		describe('user-panel', () => {
			it('In header la-user-panel should be in the burger-menu-content', async () => {
				const { fixture, header } = await initializeComponent()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(USER_PANEL_BY)

				expect(element).toBeTruthy()
			})

			it('In header la-user-panel should has userInfo input with user info data', async () => {
				const { fixture, header } = await initializeComponent({
					userInfo: TEST_USER
				})

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(USER_PANEL_BY)

				expect(element.properties.userInfo).toEqual(TEST_USER)
			})

			it('In header burger-menu should close on call (closeBurgerMenu) event on la-user-panel', async () => {
				const { fixture, header } = await initializeComponent()

				header.query(BURGER_BUTTON_BY).triggerEventHandler('click')
				fixture.detectChanges()

				const element = header.query(BURGER_MENU_CONTENT).query(USER_PANEL_BY)

				element.triggerEventHandler('closeBurgerMenu')
				fixture.detectChanges()

				expect(header.query(BURGER_MENU_BY)).not.toBeTruthy()
			})
		})
	})
})
