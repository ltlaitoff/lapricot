import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { User } from '@lapricot/shared/types'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { LogoComponent } from '../logo/logo.component'
import { MenuComponent } from '../menu/menu.component'
import { UserPanelComponent } from '../user-panel/user-panel.component'

@Component({
	selector: 'la-header',
	standalone: true,
	imports: [
		CommonModule,
		AngularSvgIconModule,
		LogoComponent,
		MenuComponent,
		UserPanelComponent
	],
	templateUrl: './header.component.html'
})
export class HeaderComponent {
	@Input() userInfo: User | null = null

	menuOpened = false

	toggleMenuOpened() {
		this.menuOpened = !this.menuOpened
	}

	closeMenu() {
		this.menuOpened = false
	}
}
