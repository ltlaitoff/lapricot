import { AsyncPipe, CommonModule } from '@angular/common'
import { Component, Input, OnInit } from '@angular/core'
import { LoadStatus, RootState } from '@lapricot/client-lib/store'
import { Action, Store } from '@ngrx/store'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { distinctUntilChanged, Observable } from 'rxjs'
import { ICONS, STATUS_TYPES } from './load-status-button.config'

@Component({
	selector: 'la-load-status-button',
	standalone: true,
	imports: [CommonModule, AsyncPipe, AngularSvgIconModule],
	templateUrl: './load-status-button.component.html'
})
export class LoadStatusButtonComponent implements OnInit {
	@Input({ required: true }) type: keyof typeof STATUS_TYPES

	icons = ICONS

	status$: Observable<LoadStatus>
	loadAction: Action

	ngOnInit() {
		this.status$ = this.store
			.select(STATUS_TYPES[this.type].selectStatus)
			.pipe(distinctUntilChanged())
		this.loadAction = STATUS_TYPES[this.type].loadAction({ force: true })
	}

	onClick() {
		this.store.dispatch(this.loadAction)
	}

	constructor(private store: Store<RootState>) {}
}
