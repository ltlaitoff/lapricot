import {
	CategoriesActions,
	LoadStatus,
	SeanceActions,
	selectCategoriesStatus,
	selectSeanceStatus,
	selectStatisticStatus,
	StatisticActions
} from '@lapricot/client-lib/store'

export const ICONS = {
	[LoadStatus.NOT_SYNCHRONIZED]: 'assets/icons/help.svg',
	[LoadStatus.SYNCHRONIZATION]: 'assets/icons/upgrade.svg',
	[LoadStatus.ERROR]: 'assets/icons/cancel.svg',
	[LoadStatus.SYNCHRONIZED]: 'assets/icons/ok.svg'
}

export const STATUS_TYPES = {
	categories: {
		loadAction: CategoriesActions.load,
		selectStatus: selectCategoriesStatus
	},
	statistic: {
		loadAction: StatisticActions.load,
		selectStatus: selectStatisticStatus
	},
	seance: {
		loadAction: SeanceActions.load,
		selectStatus: selectSeanceStatus
	}
}
