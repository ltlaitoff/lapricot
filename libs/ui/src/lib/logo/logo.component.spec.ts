import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { RouterTestingModule } from '@angular/router/testing'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { LogoComponent } from './logo.component'
import { LogoUserInput } from './logo.interface'

interface ComponentInputs {
	userInfo: LogoUserInput
}

async function initializeComponent(
	inputs: ComponentInputs = { userInfo: null }
) {
	await TestBed.configureTestingModule({
		declarations: [LogoComponent],
		imports: [RouterTestingModule],
		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(LogoComponent)
	const component = fixture.componentInstance

	component.userInfo = inputs.userInfo

	fixture.detectChanges()

	return { fixture, component }
}

describe('LogoComponent', () => {
	it('should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('LogoComponent should have link', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(By.css('a'))

		expect(element).toBeTruthy()
	})

	it('should have link with href = undefined on userInfo = null', async () => {
		const { fixture } = await initializeComponent({
			userInfo: null
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['href']).toBe(undefined)
	})

	it('should have link with href = undefined on userInfo.authorized = "false"', async () => {
		const { fixture } = await initializeComponent({
			userInfo: { authorized: false }
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['href']).toBe(undefined)
	})

	it('should have link with href = "/" on userInfo.authorized = "true"', async () => {
		const { fixture } = await initializeComponent({
			userInfo: { authorized: true }
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['href']).toBe('/')
	})

	it('LogoComponent should have link with attribute aria-disabled = "false" on userInfo.authorized = "true"', async () => {
		const { fixture } = await initializeComponent({
			userInfo: { authorized: true }
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['aria-disabled']).toBe('false')
	})

	it('LogoComponent should have link with attribute aria-disabled = "true" on userInfo = null', async () => {
		const { fixture } = await initializeComponent({
			userInfo: null
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['aria-disabled']).toBe('true')
	})

	it('LogoComponent should have link with attribute aria-disabled = "true" on userInfo.authorized = false', async () => {
		const { fixture } = await initializeComponent({
			userInfo: { authorized: false }
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['aria-disabled']).toBe('true')
	})

	it('Link should have svg', async () => {
		const { fixture } = await initializeComponent()

		const linkElement = fixture.debugElement.query(By.css('a'))
		const element = linkElement.query(By.css('svg-icon'))

		expect(element).toBeTruthy()
	})

	it('Link should have h1 with trimmed textContent "Lapricot"', async () => {
		const { fixture } = await initializeComponent()

		const linkElement = fixture.debugElement.query(By.css('a'))
		const element = linkElement.query(By.css('h1'))

		expect(element).toBeTruthy()
		expect(element.nativeElement.textContent.trim()).toBe('Lapricot')
	})
})
