import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { RouterModule } from '@angular/router'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { LogoUserInput } from './logo.interface'

@Component({
	selector: 'la-logo',
	standalone: true,
	imports: [CommonModule, AngularSvgIconModule, RouterModule],
	templateUrl: './logo.component.html',
	styleUrls: ['./logo.component.scss']
})
export class LogoComponent {
	@Input() userInfo: LogoUserInput | null = null
}
