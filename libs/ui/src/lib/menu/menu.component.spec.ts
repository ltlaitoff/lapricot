const mockMyMethod = jest
	.fn()
	.mockImplementation((menuItems: MenuItem[]): AnimationOptionsWithId[] => {
		return menuItems.map(item => ({
			name: String(item.id),
			id: item.id,
			path: `./${item.icon}.json`
		}))
	})

import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { MenuComponent } from './menu.component'
import { AnimationOptionsWithId, MenuItem, MenuUserInput } from './menu.types'
import { RouterTestingModule } from '@angular/router/testing'
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core'
import { first } from 'rxjs'
import { provideRouter, Router } from '@angular/router'

const mockMenuItems = [
	{
		id: 0,
		name: 'MOCKED-name-0',
		path: 'MOCKED-path-0',
		icon: 'MOCKED-icon-0'
	},
	{
		id: 1,
		name: 'MOCKED-name-1',
		path: 'MOCKED-path-1',
		icon: 'MOCKED-icon-1'
	},
	{
		id: 2,
		name: 'MOCKED-name-2',
		path: 'MOCKED-path-2',
		icon: 'MOCKED-icon-2'
	}
]

jest.mock('./menu.helper', () => ({
	getAnimationOptions: mockMyMethod
}))

jest.mock('./menu.config', () => ({
	get MENU_ITEMS() {
		return mockMenuItems
	}
}))

type ComponentInputs = Partial<{
	mini: boolean
	userInfo: MenuUserInput | null
}>

async function initializeComponent(
	inputs: ComponentInputs = { mini: false, userInfo: { authorized: true } }
) {
	await TestBed.configureTestingModule({
		declarations: [MenuComponent],
		imports: [RouterTestingModule],
		schemas: [CUSTOM_ELEMENTS_SCHEMA],
		providers: [provideRouter([{ path: '**', component: MenuComponent }])]
	}).compileComponents()

	TestBed.inject(Router)

	const fixture = TestBed.createComponent(MenuComponent)
	const component = fixture.componentInstance

	if (inputs.mini) component.mini = inputs.mini
	if (inputs.userInfo) component.userInfo = inputs.userInfo

	fixture.detectChanges()

	return { fixture, component }
}

/** Button events to pass to `DebugElement.triggerEventHandler` for RouterLink event handler */
export const ButtonClickEvents = {
	left: { button: 0 },
	right: { button: 2 }
}

/** Simulate element click. Defaults to mouse left-button click event. */
export function click(
	el: DebugElement | HTMLElement,
	eventObj: unknown = ButtonClickEvents.left
): void {
	if (el instanceof HTMLElement) {
		el.click()
	} else {
		el.triggerEventHandler('click', eventObj)
	}
}

describe('MenuComponent', () => {
	describe('basic', () => {
		it(`Menu should create component`, async () => {
			const { component } = await initializeComponent()

			expect(component).toBeTruthy()
		})

		it(`In menu nav tag should be`, async () => {
			const { fixture } = await initializeComponent()

			const element = fixture.debugElement.query(By.css('nav'))

			expect(element).toBeTruthy()
		})

		it(`In menu ul with li x ${mockMenuItems.length} should be`, async () => {
			const { fixture } = await initializeComponent()

			const elements = fixture.debugElement.queryAll(By.css('li'))

			expect(elements.length).toBe(mockMenuItems.length)
		})

		it(`In menu link(<a>) should be in every li`, async () => {
			const { fixture } = await initializeComponent()

			const elements = fixture.debugElement.queryAll(By.css('li'))

			expect(elements.length).toBe(mockMenuItems.length)

			elements.map(element => {
				const link = element.query(By.css('a'))

				expect(link).toBeTruthy()
			})
		})
	})

	describe('disabled', () => {
		it(`In menu links should be disabled on input userInfo = null`, async () => {
			const { fixture } = await initializeComponent({ userInfo: null })

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				expect(link.attributes.disabled).toBe('true')
			})
		})

		it(`In menu links should be disabled on input userInfo.authorized = false `, async () => {
			const { fixture } = await initializeComponent({
				userInfo: { authorized: false }
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				expect(link.attributes.disabled).toBe('true')
			})
		})

		it(`In menu links should not be disabled on input userInfo.authorized = true`, async () => {
			const { fixture } = await initializeComponent({
				userInfo: { authorized: true }
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				expect(link.attributes.disabled).toBe('false')
			})
		})
	})

	describe('routeClick', () => {
		it(`In menu routeClick event should be called on click on link`, async () => {
			const { component, fixture } = await initializeComponent({
				userInfo: { authorized: true }
			})

			fixture.debugElement.queryAll(By.css('a')).map(link => {
				let routeClickCalled = false

				component.routeClick
					.pipe(first())
					.subscribe(() => (routeClickCalled = true))

				click(link)
				fixture.detectChanges()

				expect(link.attributes.disabled).toBe('false')
				expect(routeClickCalled).toBe(true)
			})
		})

		it(`In menu routeClick event should not be called on click on disabled link`, async () => {
			const { component, fixture } = await initializeComponent({
				userInfo: { authorized: false }
			})

			fixture.debugElement.queryAll(By.css('a')).map(link => {
				let routeClickCalled = false

				component.routeClick
					.pipe(first())
					.subscribe(() => (routeClickCalled = true))

				click(link)
				fixture.detectChanges()

				expect(link.attributes.disabled).toBe('true')
				expect(routeClickCalled).toBe(false)
			})
		})
	})

	describe('href and text', () => {
		it(`In menu href attr should be = undefined on input userInfo = null`, async () => {
			const { fixture } = await initializeComponent({
				userInfo: null
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				expect(link.attributes.href).toBe(undefined)
			})
		})

		it(`In menu href attr should be = undefined on input userInfo.authorized = false `, async () => {
			const { fixture } = await initializeComponent({
				userInfo: { authorized: false }
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				expect(link.attributes.href).toBe(undefined)
			})
		})

		it(`In menu href attr should be = menuItem.path on input userInfo.authorized = true`, async () => {
			const { fixture } = await initializeComponent({
				userInfo: { authorized: true }
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				const id = Number(
					link.attributes['data-testid']?.replace('menu-link-', '')
				)

				expect(link.attributes.href).toBe('/' + mockMenuItems[id].path)
			})
		})

		it(`In menu menuItem.name text should be in link`, async () => {
			const { fixture } = await initializeComponent({
				userInfo: { authorized: true }
			})

			const links = fixture.debugElement.queryAll(By.css('a'))

			links.map(link => {
				const id = Number(
					link.attributes['data-testid']?.replace('menu-link-', '')
				)

				expect(link.nativeElement.textContent.trim()).toBe(
					mockMenuItems[id].name
				)
			})
		})
	})

	describe('animation', () => {
		it(`In menu every link element should play animation on mouse enter on userInfo valid`, async () => {
			const { fixture } = await initializeComponent()

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map(link => {
				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(1)
				expect(newItem.stop).toBeCalledTimes(0)
			})
		})

		it(`In menu every link element should stop animation on mouse enter and after mouse leave on userInfo valid`, async () => {
			const { fixture } = await initializeComponent()

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map(link => {
				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(1)
				expect(newItem.stop).toBeCalledTimes(0)

				link.triggerEventHandler('mouseleave')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(1)
				expect(newItem.stop).toBeCalledTimes(1)
			})
		})

		it(`In menu every link element should not play animation on mouse enter on userInfo not valid`, async () => {
			const { fixture } = await initializeComponent({ userInfo: null })

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map(link => {
				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(0)
				expect(newItem.stop).toBeCalledTimes(0)
			})
		})

		it(`In menu every link element should not play and stop animation on mouse enter and after mouse leave on userInfo not valid`, async () => {
			const { fixture } = await initializeComponent({ userInfo: null })

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map(link => {
				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(0)
				expect(newItem.stop).toBeCalledTimes(0)

				link.triggerEventHandler('mouseleave')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(0)
				expect(newItem.stop).toBeCalledTimes(0)
			})
		})
	})

	describe('errors', () => {
		beforeAll(() => {
			jest.spyOn(console, 'error').mockImplementation(() => ({}))
		})

		afterAll(() => {
			;(console.error as jest.Mock).mockRestore()
		})

		afterEach(() => {
			;(console.error as jest.Mock).mockClear()
		})

		it('In menu component should call console.error on error in get animation options', async () => {
			mockMyMethod.mockImplementation(
				(menuItems: MenuItem[]): AnimationOptionsWithId[] => {
					return menuItems.map(item => ({
						name: String(item.id),
						id: -1,
						path: `./${item.icon}.json`
					}))
				}
			)

			await initializeComponent()

			expect(console.error).toHaveBeenCalled()
			;(console.error as jest.Mock).mock.calls.map((text: string) => {
				expect(
					text[0].includes('Not fount animateOptions for id = ')
				).toBeTruthy()
			})
		})

		it('In menu component should call console.error on error in start animation', async () => {
			mockMyMethod.mockImplementation(
				(menuItems: MenuItem[]): AnimationOptionsWithId[] => {
					return menuItems.map(item => ({
						name: String(item.id),
						id: -1,
						path: `./${item.icon}.json`
					}))
				}
			)

			const { fixture } = await initializeComponent()

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map((link, index) => {
				;(console.error as jest.Mock).mockClear()

				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(0)
				expect(newItem.stop).toBeCalledTimes(0)
				expect(console.error).toHaveBeenCalled()

				const error = (console.error as jest.Mock).mock.calls.find(
					(text: string) => {
						return text[0].includes('No find animation with id = ')
					}
				)

				expect(error[0]).toContain(String(index))
			})
		})

		it('In menu component should call console.error on error in start animation and end', async () => {
			mockMyMethod.mockImplementation(
				(menuItems: MenuItem[]): AnimationOptionsWithId[] => {
					return menuItems.map(item => ({
						name: String(item.id),
						id: -1,
						path: `./${item.icon}.json`
					}))
				}
			)

			const { fixture } = await initializeComponent()

			const allLinks = fixture.debugElement.queryAll(By.css('a'))

			allLinks.map((link, index) => {
				;(console.error as jest.Mock).mockClear()

				const ngLottie = link.query(By.css('ng-lottie'))

				const newItem = {
					name: String(ngLottie.properties.options.id),
					play: jest.fn(),
					stop: jest.fn()
				}

				ngLottie.triggerEventHandler('animationCreated', newItem)
				fixture.detectChanges()

				link.triggerEventHandler('mouseenter')
				fixture.detectChanges()

				expect(newItem.play).toBeCalledTimes(0)
				expect(newItem.stop).toBeCalledTimes(0)
				expect(console.error).toHaveBeenCalled()

				const errorStart = (console.error as jest.Mock).mock.calls.find(
					(text: string) => {
						return text[0].includes('No find animation with id = ')
					}
				)

				expect(errorStart[0]).toContain(String(index))
				;(console.error as jest.Mock).mockClear()

				link.triggerEventHandler('mouseleave')
				fixture.detectChanges()

				const errorEnd = (console.error as jest.Mock).mock.calls.find(
					(text: string) => {
						return text[0].includes('No find animation with id = ')
					}
				)

				expect(errorEnd[0]).toContain(String(index))
			})
		})
	})
})
