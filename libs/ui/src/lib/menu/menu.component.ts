import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	Output
} from '@angular/core'
import { AnimationItem } from 'lottie-web'
import { AUTHORIZATION_MENU_ITEMS, MENU_ITEMS } from './menu.config'
import {
	AnimationOptionsWithId,
	AnimationSetStatusTypes,
	MenuItem,
	MenuUserInput
} from './menu.types'
import { getAnimationOptions } from './menu.helper'
import { CommonModule } from '@angular/common'
import { NavigationEnd, Router, RouterModule } from '@angular/router'
import { LottieModule } from 'ngx-lottie'
import { filter, firstValueFrom } from 'rxjs'
// import { LottieModule } from 'ngx-lottie'

// Factory funtion needed ngx-lottie
export function playerFactory() {
	return import(/* webpackChunkName: 'lottie-web' */ 'lottie-web')
}

@Component({
	selector: 'la-menu',
	standalone: true,
	imports: [CommonModule, RouterModule, LottieModule],
	templateUrl: './menu.component.html'
})
export class MenuComponent implements OnChanges {
	@Input() mini = false
	@Input() userInfo: MenuUserInput | null = null

	@Output() routeClick = new EventEmitter()

	menuItems: MenuItem[]
	activeMenuItemsForce = false

	private animateOptions: AnimationOptionsWithId[]
	private animationItems: AnimationItem[]

	constructor(private router: Router) {
		this.menuItems = MENU_ITEMS
		this.animateOptions = getAnimationOptions(this.menuItems)
		this.animationItems = []
	}

	async ngOnChanges() {
		const routerEvent: NavigationEnd = (await firstValueFrom(
			this.router.events.pipe(filter(event => event instanceof NavigationEnd))
		)) as NavigationEnd

		this.activeMenuItemsForce = false

		if (this.userInfo === null && routerEvent.url.includes('/profile')) {
			this.activeMenuItemsForce = true
			this.menuItems = AUTHORIZATION_MENU_ITEMS
		} else {
			this.menuItems = MENU_ITEMS
		}

		this.animateOptions = getAnimationOptions(this.menuItems)
		this.animationItems = []
	}

	getAnimateOptions(id: number) {
		const animateOptions = this.animateOptions.find(item => item.id === id)

		if (animateOptions == undefined) {
			console.error(`Not fount animateOptions for id = ${id}`)
			return {}
		}

		return animateOptions
	}

	onAnimate(animationItem: AnimationItem): void {
		this.animationItems.push(animationItem)
	}

	private getAnimationItemById(id: number) {
		return this.animationItems.find(item => item.name === String(id))
	}

	private animationSetStatus(id: number, type: AnimationSetStatusTypes) {
		const animationItem = this.getAnimationItemById(id)

		if (animationItem == undefined) {
			console.error(`No find animation with id = ${id}`)
			return
		}

		switch (type) {
			case AnimationSetStatusTypes.PLAY:
				animationItem.play()
				return

			case AnimationSetStatusTypes.STOP:
				animationItem.stop()
				return
		}
	}

	mouseLinkEnter(id: number) {
		if (!this.userInfo || !this.userInfo.authorized) return

		this.animationSetStatus(id, AnimationSetStatusTypes.PLAY)
	}

	mouseLinkLeave(id: number) {
		if (!this.userInfo || !this.userInfo.authorized) return

		this.animationSetStatus(id, AnimationSetStatusTypes.STOP)
	}

	onRouteClickInner() {
		if (!this.userInfo || !this.userInfo.authorized) return

		this.routeClick.emit()
	}
}
