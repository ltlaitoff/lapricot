import { getAnimationOptions } from './menu.helper'
import { MenuItem } from './menu.types'

const mockDefaultAnimateOptions = { defaultProp: 1 }
const mockIconPath = 'mockIconPath'

jest.mock('./menu.config', () => {
	return {
		get defaultAnimateOptions() {
			return mockDefaultAnimateOptions
		},
		get ICONS_PATH() {
			return mockIconPath
		}
	}
})

type TestItemResult = {
	name: string
	id: number
	defaultProp: number
	path: string
}

const TESTS: Array<[Array<MenuItem>, Array<TestItemResult>]> = [
	[
		[{ id: 0, icon: 'icon-0-mock', name: '', path: '' }],
		[
			{
				name: '0',
				id: 0,
				defaultProp: 1,
				path: mockIconPath + '/icon-0-mock.json'
			}
		]
	],
	[
		[
			{ id: 123125, icon: 'icon-0-mock', name: '', path: '' },
			{ id: 1, icon: 'icon-12123-mock', name: '', path: '' }
		],
		[
			{
				name: '123125',
				id: 123125,
				defaultProp: 1,
				path: mockIconPath + '/icon-0-mock.json'
			},
			{
				name: '1',
				id: 1,
				defaultProp: 1,
				path: mockIconPath + '/icon-12123-mock.json'
			}
		]
	]
]

describe('menu.helper', () => {
	it.each(TESTS)(
		'In menu helper getAnimationOptions should return %j on menuItems = %j',
		(input, result) => {
			expect(getAnimationOptions(input)).toEqual(result)
		}
	)
})
