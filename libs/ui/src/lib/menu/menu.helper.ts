import { defaultAnimateOptions, ICONS_PATH } from './menu.config'
import { AnimationOptionsWithId, MenuItem } from './menu.types'

export const getAnimationOptions = (
	menuItems: MenuItem[]
): AnimationOptionsWithId[] => {
	return menuItems.map(item => ({
		name: String(item.id),
		...defaultAnimateOptions,
		id: item.id,
		path: `${ICONS_PATH}/${item.icon}.json`
	}))
}
