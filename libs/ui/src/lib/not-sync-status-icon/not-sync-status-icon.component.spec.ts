import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { NotSyncStatus } from '@lapricot/client-lib/store'

import { NotSyncStatusIconComponent } from './not-sync-status-icon.component'

type ComponentInputs = Partial<{
	status: NotSyncStatus | undefined
}>

async function initializeComponent(
	inputs: ComponentInputs = { status: undefined }
) {
	await TestBed.configureTestingModule({
		declarations: [NotSyncStatusIconComponent],

		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(NotSyncStatusIconComponent)
	const component = fixture.componentInstance

	if (inputs.status) component.status = inputs.status

	component.ngOnChanges()
	fixture.detectChanges()

	return { fixture, component }
}

describe('NotSyncStatusIconComponent', () => {
	it('In NotSyncStatusIcon component should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it(`In NotSyncStatusIcon svg-icon should be on status !== undefined`, async () => {
		const { fixture } = await initializeComponent({
			status: NotSyncStatus.SYNCHRONIZATION
		})

		const svgIcon = fixture.debugElement.query(By.css('svg-icon'))

		expect(svgIcon).toBeTruthy()
	})

	it(`In NotSyncStatusIcon svg-icon should not be on status === undefined`, async () => {
		const { fixture } = await initializeComponent({
			status: undefined
		})

		const svgIcon = fixture.debugElement.query(By.css('svg-icon'))

		expect(svgIcon).not.toBeTruthy()
	})

	it(`In NotSyncStatusIcon svg-icon should be on status === undefined and after has been changed to !== undefined`, async () => {
		const { component, fixture } = await initializeComponent({
			status: undefined
		})

		expect(fixture.debugElement.query(By.css('svg-icon'))).not.toBeTruthy()

		component.status = NotSyncStatus.NOT_SYNCHRONIZED
		component.ngOnChanges()
		fixture.detectChanges()

		expect(fixture.debugElement.query(By.css('svg-icon'))).toBeTruthy()
	})

	it(`In NotSyncStatusIcon svg-icon should has data-status attribute = 'NOT_SYNCHRONIZED' on status = NotSyncStatus.NOT_SYNCHRONIZED`, async () => {
		const { fixture } = await initializeComponent({
			status: NotSyncStatus.NOT_SYNCHRONIZED
		})

		const svgIcon = fixture.debugElement.query(By.css('svg-icon'))

		expect(svgIcon.properties['data-status']).toBe('NOT_SYNCHRONIZED')
	})

	it(`In NotSyncStatusIcon svg-icon should has data-status attribute = 'SYNCHRONIZATION' on status = NotSyncStatus.SYNCHRONIZATION`, async () => {
		const { fixture } = await initializeComponent({
			status: NotSyncStatus.SYNCHRONIZATION
		})

		const svgIcon = fixture.debugElement.query(By.css('svg-icon'))

		expect(svgIcon.properties['data-status']).toBe('SYNCHRONIZATION')
	})

	it(`In NotSyncStatusIcon svg-icon should has data-status attribute = 'ERROR' on status = NotSyncStatus.ERROR`, async () => {
		const { fixture } = await initializeComponent({
			status: NotSyncStatus.ERROR
		})

		const svgIcon = fixture.debugElement.query(By.css('svg-icon'))

		expect(svgIcon.properties['data-status']).toBe('ERROR')
	})
})
