import { CommonModule } from '@angular/common'
import { Component, Input, NO_ERRORS_SCHEMA, OnChanges } from '@angular/core'
import { NotSyncStatus } from '@lapricot/client-lib/store'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-not-sync-status-icon',
	standalone: true,
	imports: [CommonModule, AngularSvgIconModule],
	schemas: [NO_ERRORS_SCHEMA],
	templateUrl: './not-sync-status-icon.component.html'
})
export class NotSyncStatusIconComponent implements OnChanges {
	@Input({ required: true }) status: NotSyncStatus | undefined

	innerStatus: 'NOT_SYNCHRONIZED' | 'SYNCHRONIZATION' | 'ERROR' | '' = ''

	ngOnChanges() {
		this.innerStatus = this.status ? this.getStatusType(this.status) : ''
	}

	getStatusType(status: NotSyncStatus) {
		if (status === NotSyncStatus.NOT_SYNCHRONIZED) {
			return 'NOT_SYNCHRONIZED'
		}

		if (status === NotSyncStatus.SYNCHRONIZATION) {
			return 'SYNCHRONIZATION'
		}

		return 'ERROR'
	}
}
