import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'

import { PanelFormDividerComponent } from './panel-form-divider.component'

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [PanelFormDividerComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(PanelFormDividerComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return {
		fixture,

		component,
		detectChanges: fixture.detectChanges
	}
}

describe('PanelFormDividerComponent', () => {
	it('Component la-panel-form-divider should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-panel-form-divider should be div', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(By.css('div'))

		expect(element).toBeTruthy()
	})
})
