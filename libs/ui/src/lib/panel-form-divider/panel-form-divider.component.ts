import { Component } from '@angular/core'

@Component({
	selector: 'la-panel-form-divider',
	standalone: true,
	templateUrl: './panel-form-divider.component.html'
})
export class PanelFormDividerComponent {}
