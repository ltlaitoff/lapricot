import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { first } from 'rxjs'
import { PanelFormItemBaseComponent } from './panel-form-item-base.component'

interface ComponentInputs {
	text: string
	imageSrc: string
	type: 'button' | 'link'
	linkHref: string | null
	disabled: boolean
}

const defaultInputs: ComponentInputs = {
	text: '',
	imageSrc: '',
	type: 'button',
	linkHref: null,
	disabled: false
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [PanelFormItemBaseComponent],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(PanelFormItemBaseComponent)
	const component = fixture.componentInstance

	component.text = inputs.text !== undefined ? inputs.text : defaultInputs.text
	component.imageSrc =
		inputs.imageSrc !== undefined ? inputs.imageSrc : defaultInputs.imageSrc
	component.type = inputs.type !== undefined ? inputs.type : defaultInputs.type
	component.linkHref =
		inputs.linkHref !== undefined ? inputs.linkHref : defaultInputs.linkHref
	component.disabled =
		inputs.disabled !== undefined ? inputs.disabled : defaultInputs.disabled

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('PanelFormItemBaseComponent', () => {
	describe('basic', () => {
		it('Component la-panel-form-item-base should be', async () => {
			const { component } = await initializeComponent()

			expect(component).toBeTruthy()
		})

		it('In la-panel-form-item-base link should be on Input type = "link"	', async () => {
			const { fixture } = await initializeComponent({ type: 'link' })

			const element = fixture.debugElement.query(By.css('a'))

			expect(element).toBeTruthy()
		})

		it('In la-panel-form-item-base button should be on Input type = "button"	', async () => {
			const { fixture } = await initializeComponent({ type: 'button' })

			const element = fixture.debugElement.query(By.css('button'))

			expect(element).toBeTruthy()
		})
	})

	describe('button', () => {
		it('In la-panel-form-item-base should has text = "somethingText" on Input text = "somethingText"', async () => {
			const { fixture } = await initializeComponent({
				type: 'button',
				text: 'somethingText'
			})

			const element = fixture.debugElement.query(By.css('button'))

			expect(element.nativeElement.textContent.trim()).toEqual('somethingText')
		})

		it('In la-panel-form-item-base should has svg-icon src param = "somethingImage" on Input imageSrc = "somethingImage"', async () => {
			const { fixture } = await initializeComponent({
				type: 'button',
				imageSrc: 'somethingImage'
			})

			const element = fixture.debugElement.query(By.css('button'))
			const svgIcon = element.query(By.css('svg-icon'))

			expect(svgIcon.properties.src).toEqual('somethingImage')
		})

		it('In la-panel-form-item-base "itemClick" Output should called onClick on button on Input disabled = false', async () => {
			const { component, fixture } = await initializeComponent({
				type: 'button',
				disabled: false
			})

			let isClicked = false

			component.itemClick.pipe(first()).subscribe(() => (isClicked = true))

			const element = fixture.debugElement.query(By.css('button'))

			element.triggerEventHandler('click')
			fixture.detectChanges()

			expect(isClicked).toBe(true)
		})

		it('In la-panel-form-item-base "itemClick" Output should not called onClick on button on Input disabled = true', async () => {
			const { component, fixture } = await initializeComponent({
				type: 'button',
				disabled: true
			})

			let isClicked = false

			component.itemClick.pipe(first()).subscribe(() => (isClicked = true))

			const element = fixture.debugElement.query(By.css('button'))

			element.triggerEventHandler('click')
			fixture.detectChanges()

			expect(isClicked).toBe(false)
		})
	})

	describe('link', () => {
		it('In la-panel-form-item-base should has text = "somethingText" on Input text = "somethingText"', async () => {
			const { fixture } = await initializeComponent({
				type: 'link',
				text: 'somethingText'
			})

			const element = fixture.debugElement.query(By.css('a'))

			expect(element.nativeElement.textContent.trim()).toEqual('somethingText')
		})

		it('In la-panel-form-item-base should has svg-icon src param = "somethingImage" on Input imageSrc = "somethingImage"', async () => {
			const { fixture } = await initializeComponent({
				type: 'link',
				imageSrc: 'somethingImage'
			})

			const element = fixture.debugElement.query(By.css('a'))
			const svgIcon = element.query(By.css('svg-icon'))

			expect(svgIcon.properties.src).toEqual('somethingImage')
		})

		it('In la-panel-form-item-base "itemClick" Output should called onClick on link on Input disabled = false', async () => {
			const { component, fixture } = await initializeComponent({
				type: 'link',
				disabled: false
			})

			let isClicked = false

			component.itemClick.pipe(first()).subscribe(() => (isClicked = true))

			const element = fixture.debugElement.query(By.css('a'))

			element.triggerEventHandler('click')
			fixture.detectChanges()

			expect(isClicked).toBe(true)
		})

		it('In la-panel-form-item-base "itemClick" Output should not called onClick on link on Input disabled = true', async () => {
			const { component, fixture } = await initializeComponent({
				type: 'link',
				disabled: true
			})

			let isClicked = false

			component.itemClick.pipe(first()).subscribe(() => (isClicked = true))

			const element = fixture.debugElement.query(By.css('a'))

			element.triggerEventHandler('click')
			fixture.detectChanges()

			expect(isClicked).toBe(false)
		})

		it('In la-panel-form-item-base link should has routerLink param = "path" on Input linkHref = "path"', async () => {
			const { fixture } = await initializeComponent({
				type: 'link',
				linkHref: 'path'
			})

			const element = fixture.debugElement.query(By.css('a'))

			expect(element.properties.routerLink).toBe('path')
		})
	})
})
