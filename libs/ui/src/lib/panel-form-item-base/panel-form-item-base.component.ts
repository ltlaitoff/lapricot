import { CommonModule } from '@angular/common'
import { Component, Input, EventEmitter, Output } from '@angular/core'
import { RouterModule } from '@angular/router'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-panel-form-item-base',
	standalone: true,
	imports: [CommonModule, RouterModule, AngularSvgIconModule],
	templateUrl: './panel-form-item-base.component.html'
})
export class PanelFormItemBaseComponent {
	@Input() text = ''
	@Input() imageSrc = ''
	@Input() type: 'button' | 'link' = 'button'
	@Input() linkHref: string | null = null
	@Input() disabled = false

	@Output() itemClick = new EventEmitter()

	onButtonClick() {
		if (this.disabled) return

		this.itemClick.emit()
	}
}
