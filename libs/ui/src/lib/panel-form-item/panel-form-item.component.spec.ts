import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { first } from 'rxjs'
import { PanelFormItemComponent } from './panel-form-item.component'

jest.mock('./panel-form-item.config.ts', () => ({
	PANEL_FORM_ITEMS: {
		c1: {
			imageSrc: 'component1imageSrc',
			text: 'component1Text'
		},
		c2: {
			imageSrc: 'component2imageSrc',
			text: 'component2Text'
		}
	}
}))

interface ComponentInputs {
	type: 'c1' | 'c2'
	itemType: 'button' | 'link'
	linkHref: string | null
	disabled: boolean
}

const defaultInputs: ComponentInputs = {
	type: 'c1',
	itemType: 'button',
	linkHref: null,
	disabled: false
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [PanelFormItemComponent],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(PanelFormItemComponent)
	const component = fixture.componentInstance

	// @ts-expect-error Test
	component.type = inputs.type !== undefined ? inputs.type : defaultInputs.type
	component.itemType =
		inputs.itemType !== undefined ? inputs.itemType : defaultInputs.itemType
	component.linkHref =
		inputs.linkHref !== undefined ? inputs.linkHref : defaultInputs.linkHref
	component.disabled =
		inputs.disabled !== undefined ? inputs.disabled : defaultInputs.disabled

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('PanelFormItemComponent', () => {
	describe('base', () => {
		it('Component panel-form-item should  be', async () => {
			const { component } = await initializeComponent()

			expect(component).toBeTruthy()
		})

		it('In panel-form-item la-panel-form-item-base should be on valid Input "type" ', async () => {
			const { fixture } = await initializeComponent({
				type: 'c1'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element).toBeTruthy()
		})

		it('In panel-form-item la-panel-form-item-base should not be on not valid Input "type"', async () => {
			const { fixture } = await initializeComponent({
				// @ts-expect-error Test
				type: null
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element).not.toBeTruthy()
		})
	})

	describe('inputs', () => {
		it('In panel-form-item la-panel-form-item-base should has param "disabled" = false on Input "disabled" = false', async () => {
			const { fixture } = await initializeComponent({
				disabled: true
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.disabled).toBe(true)
		})

		it('In panel-form-item la-panel-form-item-base should has param "disabled" = true on Input "disabled" = true', async () => {
			const { fixture } = await initializeComponent({
				disabled: false
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.disabled).toBe(false)
		})

		it('In panel-form-item la-panel-form-item-base should has param "type" = "button" on Input "itemType" = "button"', async () => {
			const { fixture } = await initializeComponent({
				itemType: 'button'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.type).toBe('button')
		})

		it('In panel-form-item la-panel-form-item-base should has param "type" = "link" on Input "itemType" = "link"', async () => {
			const { fixture } = await initializeComponent({
				itemType: 'link'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.type).toBe('link')
		})

		it('In panel-form-item la-panel-form-item-base should has param "linkHref" = null on Input "linkHref" = null', async () => {
			const { fixture } = await initializeComponent({
				linkHref: null
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.linkHref).toBe(null)
		})

		it('In panel-form-item la-panel-form-item-base should has param "linkHref" = "something" on Input "linkHref" = "something"', async () => {
			const { fixture } = await initializeComponent({
				linkHref: 'something'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.linkHref).toBe('something')
		})

		it('In panel-form-item la-panel-form-item-base should has param "text" = "component1Text" on Input "type" = "c1"', async () => {
			const { fixture } = await initializeComponent({
				type: 'c1'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.text).toBe('component1Text')
		})

		it('In panel-form-item la-panel-form-item-base should has param "text" = "component2Text" on Input "type" = "c2"', async () => {
			const { fixture } = await initializeComponent({
				type: 'c2'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.text).toBe('component2Text')
		})

		it('In panel-form-item la-panel-form-item-base should has param "imageSrc" = "component1imageSrc" on Input "type" = "c1"', async () => {
			const { fixture } = await initializeComponent({
				type: 'c1'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.imageSrc).toBe('component1imageSrc')
		})

		it('In panel-form-item la-panel-form-item-base should has param "imageSrc" = "component2imageSrc" on Input "type" = "c2"', async () => {
			const { fixture } = await initializeComponent({
				type: 'c2'
			})

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			expect(element.properties.imageSrc).toBe('component2imageSrc')
		})
	})

	describe('outputs', () => {
		it('In panel-form-item itemClick Output should called on call (click) event on la-panel-form-item-base', async () => {
			const { component, fixture } = await initializeComponent()

			let itemClickCalled = false

			component.itemClick
				.pipe(first())
				.subscribe(() => (itemClickCalled = true))

			const element = fixture.debugElement.query(
				By.css('la-panel-form-item-base')
			)

			element.triggerEventHandler('click')
			fixture.detectChanges()

			expect(itemClickCalled).toBe(true)
		})
	})
})
