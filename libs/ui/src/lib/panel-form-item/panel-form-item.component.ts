import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core'
import { CommonModule } from '@angular/common'
import { PanelFormItemBaseComponent } from '../panel-form-item-base/panel-form-item-base.component'
import { PANEL_FORM_ITEMS } from './panel-form-item.config'
import {
	PanelFormItemTypes,
	PanelFormItemValue
} from './panel-form-item.interfaces'

@Component({
	selector: 'la-panel-form-item',
	standalone: true,
	imports: [CommonModule, PanelFormItemBaseComponent],
	templateUrl: './panel-form-item.component.html'
})
export class PanelFormItemComponent implements OnInit {
	@Input({ required: true }) type: PanelFormItemTypes = 'edit'
	@Input() itemType: 'button' | 'link' = 'button'
	@Input() linkHref: string | null = null
	@Input() disabled = false

	@Output() itemClick = new EventEmitter()

	itemValue: PanelFormItemValue | null = null

	ngOnInit() {
		this.itemValue = PANEL_FORM_ITEMS[this.type]
	}

	onButtonClick() {
		this.itemClick.emit()
	}
}
