import {
	PanelFormItemTypes,
	PanelFormItemValue
} from './panel-form-item.interfaces'

export const PANEL_FORM_ITEMS: Record<PanelFormItemTypes, PanelFormItemValue> =
	{
		edit: {
			imageSrc: 'assets/icons/pencil.svg',
			text: 'Edit'
		},
		delete: {
			imageSrc: 'assets/icons/trash-can.svg',
			text: 'Delete'
		},
		archive: {
			imageSrc: 'assets/icons/archive.svg',
			text: 'Archive'
		},
		restore: {
			imageSrc: 'assets/icons/restore.svg',
			text: 'Restore'
		},
		profile: {
			imageSrc: 'assets/icons/profile.svg',
			text: 'Profile'
		},
		trash: {
			imageSrc: 'assets/icons/trash-can.svg',
			text: 'Trash'
		},
		sessions: {
			imageSrc: 'assets/icons/sessions.svg',
			text: 'Sessions'
		},
		exit: {
			imageSrc: 'assets/icons/exit.svg',
			text: 'Exit'
		},
		settings: {
			imageSrc: 'assets/icons/settings.svg',
			text: 'Settings'
		}
	}
