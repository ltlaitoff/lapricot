export type PanelFormItemTypes =
	| 'edit'
	| 'delete'
	| 'archive'
	| 'profile'
	| 'trash'
	| 'sessions'
	| 'exit'
	| 'restore'
	| 'settings'

export type PanelFormItemValue = {
	imageSrc: string
	text: string
}
