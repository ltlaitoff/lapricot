import { Component } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'

import { PanelFormComponent } from './panel-form.component'

@Component({
	selector: 'la-test-component',
	template: `
		<la-panel-form>Hello World</la-panel-form>
	`
})
class TestNgContentComponent {}

interface ComponentInputs {
	additionalClasses: string
}

const defaultInputs: ComponentInputs = {
	additionalClasses: ''
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [PanelFormComponent, TestNgContentComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(PanelFormComponent)
	const component = fixture.componentInstance
	const fixtureTop = TestBed.createComponent(TestNgContentComponent)

	component.additionalClasses =
		inputs.additionalClasses !== undefined
			? inputs.additionalClasses
			: defaultInputs.additionalClasses

	fixture.detectChanges()
	fixtureTop.detectChanges()

	return {
		fixture,
		fixtureTop,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('PanelFormComponent', () => {
	it('Component la-panel-form should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-panel-form div should be', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(By.css('div'))

		expect(element).toBeTruthy()
	})

	it('In la-panel-form div should has classes from additionalClasses Input', async () => {
		const { fixture } = await initializeComponent({
			additionalClasses: 'textClass1 testClass2'
		})

		const element = fixture.debugElement.query(By.css('div'))

		expect(element.classes['textClass1']).toBeTruthy()
		expect(element.classes['testClass2']).toBeTruthy()
	})

	it('In la-panel-form div should hav ng-content', async () => {
		const { fixtureTop } = await initializeComponent()

		const element = fixtureTop.debugElement.query(By.css('div'))

		expect(element.nativeElement.textContent).toContain('Hello World')
	})
})
