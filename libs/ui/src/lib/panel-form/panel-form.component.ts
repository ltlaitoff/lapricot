import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'

@Component({
	selector: 'la-panel-form',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './panel-form.component.html'
})
export class PanelFormComponent {
	@Input() additionalClasses = ''
}
