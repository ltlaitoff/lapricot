import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'

import { TableColorHeadItemComponent } from './table-color-head-item.component'

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [TableColorHeadItemComponent],
		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(TableColorHeadItemComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return { fixture, component }
}

describe('TableColorHeadItemComponent', () => {
	it('In table-color-head-item component should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In table-color-head-item ng-lottie should be in the document', async () => {
		const { fixture } = await initializeComponent()

		const ngLottie = fixture.debugElement.query(By.css('ng-lottie'))

		expect(ngLottie).toBeTruthy()
	})

	it('In table-color-head-item ng-lottie should has options param with autoplay: false and path string with `.json` at end', async () => {
		const { fixture } = await initializeComponent()

		const ngLottie = fixture.debugElement.query(By.css('ng-lottie'))

		expect(ngLottie.properties.options.autoplay).toBe(false)
		expect(ngLottie.properties.options.path).toContain('.json')
	})

	it('In table-color-head-item animation should start on mouseenter on data-testid="div"', async () => {
		const { fixture } = await initializeComponent()

		const div = fixture.debugElement.query(By.css('[data-testid="div"]'))
		const ngLottie = div.query(By.css('ng-lottie'))

		const newItem = {
			play: jest.fn(),
			stop: jest.fn()
		}

		ngLottie.triggerEventHandler('animationCreated', newItem)
		fixture.detectChanges()

		// Check on not change element in memory
		ngLottie.triggerEventHandler('animationCreated', {
			play: jest.fn(),
			stop: jest.fn()
		})
		fixture.detectChanges()

		div.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(1)
		expect(newItem.stop).toBeCalledTimes(0)
	})

	it('In table-color-head-item animation should stop on mouseenter and after mouseleave on data-testid="div"', async () => {
		const { fixture } = await initializeComponent()

		const div = fixture.debugElement.query(By.css('[data-testid="div"]'))
		const ngLottie = div.query(By.css('ng-lottie'))

		const newItem = {
			play: jest.fn(),
			stop: jest.fn()
		}

		ngLottie.triggerEventHandler('animationCreated', newItem)
		fixture.detectChanges()

		div.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(1)
		expect(newItem.stop).toBeCalledTimes(0)

		div.triggerEventHandler('mouseleave')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(1)
		expect(newItem.stop).toBeCalledTimes(1)
	})

	it('In table-color-head-item animation should not start on mouseenter on data-testid="div" if element has not initialized', async () => {
		const { fixture } = await initializeComponent()

		const div = fixture.debugElement.query(By.css('[data-testid="div"]'))

		const newItem = {
			play: jest.fn(),
			stop: jest.fn()
		}

		div.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(0)
		expect(newItem.stop).toBeCalledTimes(0)
	})

	it('In table-color-head-item animation should stop on mouseenter and after mouseleave on data-testid="div" if element has not initialized', async () => {
		const { fixture } = await initializeComponent()

		const div = fixture.debugElement.query(By.css('[data-testid="div"]'))

		const newItem = {
			play: jest.fn(),
			stop: jest.fn()
		}

		div.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(0)
		expect(newItem.stop).toBeCalledTimes(0)

		div.triggerEventHandler('mouseleave')
		fixture.detectChanges()

		expect(newItem.play).toBeCalledTimes(0)
		expect(newItem.stop).toBeCalledTimes(0)
	})
})
