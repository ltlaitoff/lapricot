import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { AnimationItem } from 'lottie-web'
import { LottieModule } from 'ngx-lottie'

@Component({
	selector: 'la-table-color-head-item',
	standalone: true,
	imports: [CommonModule, LottieModule],
	templateUrl: './table-color-head-item.component.html'
})
export class TableColorHeadItemComponent {
	animationItem: AnimationItem | null = null

	onAnimate(newAnimationItem: AnimationItem): void {
		if (this.animationItem !== null) return

		this.animationItem = newAnimationItem
	}

	mouseEnter() {
		this.animationItem?.play()
	}

	mouseLeave() {
		this.animationItem?.stop()
	}
}
