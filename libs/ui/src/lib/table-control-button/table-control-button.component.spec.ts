import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { first } from 'rxjs'

import { TableControlButtonComponent } from './table-control-button.component'

@Component({
	selector: 'la-test-component',
	template: `
		<la-table-control-button>Hello World</la-table-control-button>
	`
})
class TestNgContentComponent {}

type ComponentInputs = Partial<{
	allowShowHint: boolean
}>

async function initializeComponent(
	inputs: ComponentInputs = { allowShowHint: true }
) {
	await TestBed.configureTestingModule({
		declarations: [TestNgContentComponent, TableControlButtonComponent],

		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixtureTop = TestBed.createComponent(TestNgContentComponent)
	fixtureTop.detectChanges()

	const fixture = TestBed.createComponent(TableControlButtonComponent)
	const component = fixture.componentInstance

	if (inputs.allowShowHint !== undefined) {
		component.allowShowHint = inputs.allowShowHint
	}

	fixture.detectChanges()

	return { fixtureTop, fixture, component }
}

describe('TableControlButtonComponent', () => {
	it('In table-control-button component should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In table-control-button button should be', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		expect(button).toBeTruthy()
	})

	it('In table-control-button svg-icon should be in the button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))
		const svgIcon = button.query(By.css('svg-icon'))

		expect(svgIcon).toBeTruthy()
	})

	it('In table-control-button hint should not be in the document by default', async () => {
		const { fixture } = await initializeComponent()

		const hint = fixture.debugElement.query(By.css('[data-testid="hint"]'))

		expect(hint).not.toBeTruthy()
	})

	it('In table-control-button buttonClick event should be called on click on button', async () => {
		const { component, fixture } = await initializeComponent()

		let buttonClickEventCalled = false

		component.buttonClick
			.pipe(first())
			.subscribe(() => (buttonClickEventCalled = true))

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('click')
		fixture.detectChanges()

		expect(buttonClickEventCalled).toBeTruthy()
	})

	it('In table-control-button hint should show on mouseenter on button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		const hint = fixture.debugElement.query(By.css('[data-testid="hint"]'))

		expect(hint).toBeTruthy()
	})

	it('In table-control-button hint should not show on mouseenter on button with Input allowShowHint = false', async () => {
		const { fixture } = await initializeComponent({ allowShowHint: false })

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).not.toBeTruthy()
	})

	it('In table-control-button hint should hide on mouseenter and after mousedown on button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).toBeTruthy()

		button.triggerEventHandler('mousedown')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).not.toBeTruthy()
	})

	it('In table-control-button hint should hide on mouseenter and after mouseleave on button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).toBeTruthy()

		button.triggerEventHandler('mouseleave')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).not.toBeTruthy()
	})

	it('In table-control-button hint should hide on mouseenter and after click on button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).toBeTruthy()

		button.triggerEventHandler('click')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('[data-testid="hint"]'))
		).not.toBeTruthy()
	})

	it('In table-control-button `Drag to move` text should be in the hint text', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		const hint = fixture.debugElement.query(By.css('[data-testid="hint"]'))

		expect(hint.nativeElement.textContent).toContain('Drag to move')
	})

	it('In table-control-button `Click to open menu` text should be in the hint text', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('mouseenter')
		fixture.detectChanges()

		const hint = fixture.debugElement.query(By.css('[data-testid="hint"]'))

		expect(hint.nativeElement.textContent).toContain('Click to open menu')
	})

	it('In table-control-button ng-content should be', async () => {
		const { fixtureTop } = await initializeComponent()

		expect(fixtureTop.nativeElement.textContent).toEqual('Hello World')
	})
})
