import { Component, Input, EventEmitter, Output } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-table-control-button',
	standalone: true,
	imports: [CommonModule, AngularSvgIconModule],
	templateUrl: './table-control-button.component.html'
})
export class TableControlButtonComponent {
	@Input() allowShowHint = true
	@Output() buttonClick = new EventEmitter()

	showHintStatus = false

	hideHint() {
		if (this.showHintStatus !== true) return

		this.showHintStatus = false
	}

	showHint() {
		if (this.showHintStatus) return

		this.showHintStatus = true
	}

	onButtonClick() {
		this.buttonClick.emit()

		this.hideHint()
	}
}
