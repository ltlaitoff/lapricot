import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { TimeNumberPickerComponent } from './time-number-picker.component'

describe('TimeNumberPickerComponent', () => {
	it.todo('Create tests')
	let component: TimeNumberPickerComponent
	let fixture: ComponentFixture<TimeNumberPickerComponent>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [TimeNumberPickerComponent],
			imports: [FormsModule, ReactiveFormsModule],
			schemas: [CUSTOM_ELEMENTS_SCHEMA]
		}).compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(TimeNumberPickerComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
