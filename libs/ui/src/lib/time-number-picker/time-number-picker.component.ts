import { CommonModule } from '@angular/common'
import {
	ChangeDetectionStrategy,
	Component,
	forwardRef,
	Input,
	OnChanges,
	SimpleChanges
} from '@angular/core'
import {
	ControlValueAccessor,
	FormsModule,
	NG_VALUE_ACCESSOR
} from '@angular/forms'

@Component({
	selector: 'la-time-number-picker',
	standalone: true,
	imports: [CommonModule, FormsModule],
	templateUrl: './time-number-picker.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TimeNumberPickerComponent),
			multi: true
		}
	],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeNumberPickerComponent
	implements ControlValueAccessor, OnChanges
{
	@Input() inputClass = ''
	@Input() inputId = ''
	@Input() inputTimeShow = false
	@Input() inputNumberShow = true
	// `value` is stub for sync formControlName work in adaptive
	@Input() value: number | null | undefined = null

	private onChange: null | ((value: number | null) => void) = null
	private onTouched: null | ((_?: unknown) => void) = null

	numberValue: number | null = null

	ngOnChanges(changes: SimpleChanges) {
		if (changes['value'] && !changes['value'].firstChange) {
			this.numberValue = changes['value'].currentValue
		}
	}

	onNumberValueChange() {
		if (!this.onChange) return

		this.markAsTouched()

		if (this.disabled) return

		this.onChange(this.numberValue)
	}

	get timeValue() {
		// TODO: Func transform seconds to time-string
		if (this.numberValue === null) return '00:00:00 AM'

		const absNumberValueInSeconds = Math.abs(this.numberValue)

		const output = new Date(absNumberValueInSeconds * 1000)

		return output.toLocaleTimeString('en-GB', { timeZone: 'UTC' })
	}

	set timeValue(newTime: string | null) {
		if (this.disabled) return
		if (newTime === null) return

		this.numberValue = this.tranformTimeToSeconds(newTime)
		this.onNumberValueChange()
	}

	private tranformTimeToSeconds(newTime: string) {
		const [hours, minutes, seconds] = newTime.split(':').map(Number)

		return hours * 60 * 60 + minutes * 60 + seconds
	}

	disabled = false
	touched = false

	writeValue(newValue: number | null): void {
		this.numberValue = newValue
	}

	registerOnChange(callback: (_: unknown) => void): void {
		this.onChange = callback
	}

	registerOnTouched(callback: (_: unknown) => void): void {
		this.onTouched = callback
	}

	setDisabledState(disabled: boolean) {
		this.disabled = disabled
	}

	markAsTouched() {
		if (!this.touched && this.onTouched) {
			this.onTouched()
			this.touched = true
		}
	}
}
