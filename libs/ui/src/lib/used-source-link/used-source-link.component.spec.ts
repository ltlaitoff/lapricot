import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { UsedSourceLinkComponent } from './used-source-link.component'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'

type ComponentInputs = {
	link: string
	icon: string
}

async function initializeComponent(
	inputs: ComponentInputs = { link: '-', icon: '-' }
) {
	await TestBed.configureTestingModule({
		declarations: [UsedSourceLinkComponent],
		schemas: [CUSTOM_ELEMENTS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(UsedSourceLinkComponent)
	const component = fixture.componentInstance

	component.link = inputs.link
	component.icon = inputs.icon

	fixture.detectChanges()

	return { fixture, component }
}

describe('UsedSourceLinkComponent', () => {
	it('should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	describe.each`
		link      | icon
		${'link'} | ${'icon'}
		${'TeSt'} | ${'TeSt'}
		${''}     | ${''}
		${'-1'}   | ${'-1'}
	`('On link = "$link" and icon = "$icon"', ({ link, icon }) => {
		it(`Link should be in the document`, async () => {
			const { fixture } = await initializeComponent({ link, icon })

			const element = fixture.debugElement.query(By.css('a'))

			expect(element).toBeTruthy()
		})

		it(`Link href should be "${link}"`, async () => {
			const { fixture } = await initializeComponent({ link, icon })

			const element = fixture.debugElement.query(By.css('a'))

			expect(element.attributes['href']).toBe(link)
		})

		it(`Icon should be in the link`, async () => {
			const { fixture } = await initializeComponent({ link, icon })

			const element = fixture.debugElement
				.query(By.css('a'))
				.query(By.css('svg-icon'))

			expect(element).toBeTruthy()
		})

		it(`Icon src should be "${icon}"`, async () => {
			const { fixture } = await initializeComponent({ link, icon })

			const element = fixture.debugElement
				.query(By.css('a'))
				.query(By.css('svg-icon'))

			expect(element.attributes['src']).toBe(icon)
		})
	})

	it(`Link should have target attr = '_blank'`, async () => {
		const { fixture } = await initializeComponent({
			link: 'test',
			icon: 'test'
		})

		const element = fixture.debugElement.query(By.css('a'))

		expect(element.attributes['target']).toBe('_blank')
	})
})
