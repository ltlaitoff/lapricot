import { Meta } from '@storybook/angular'
import { UsedSourceLinkComponent } from './used-source-link.component'

export default {
	title: 'UsedSourceLinkComponent',
	component: UsedSourceLinkComponent
} as Meta<UsedSourceLinkComponent>

export const Primary = {
	render: (args: UsedSourceLinkComponent) => ({
		props: args
	}),
	args: {
		link: '',
		icon: ''
	}
}
