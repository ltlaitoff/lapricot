import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { AngularSvgIconModule } from 'angular-svg-icon'

@Component({
	selector: 'la-used-source-link',
	standalone: true,
	imports: [CommonModule, AngularSvgIconModule],
	templateUrl: './used-source-link.component.html'
})
export class UsedSourceLinkComponent {
	@Input({ required: true }) link: string
	@Input({ required: true }) icon: string
}
