import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { first } from 'rxjs'

import { UserPanelFormComponent } from './user-panel-form.component'

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [UserPanelFormComponent],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(UserPanelFormComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('UserPanelFormComponent', () => {
	it('Component la-user-panel-form should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-user-panel-form la-panel-form should be', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))

		expect(form).toBeTruthy()
	})

	it('In la-user-panel-form should be la-panel-form-divider', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const element = form.query(By.css('la-panel-form-divider'))

		expect(element).toBeTruthy()
	})

	it('In la-user-panel-form la-panel-form-item with type = "profile" and itemType="link" with disabled = true and linkHref = undefined should be', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'profile')

		expect(element).toBeTruthy()
		if (!element) return

		expect(element.attributes['itemType']).toBe('link')
		expect(element.properties['disabled']).toBeTruthy()
		expect(element.attributes['linkHref']).toBe(undefined)
	})

	it('In la-user-panel-form la-panel-form-item with type = "trash" and itemType="link" and linkHref="trash" should be', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'trash')

		expect(element).toBeTruthy()
		if (!element) return

		expect(element.attributes['itemType']).toBe('link')
		expect(element.attributes['linkHref']).toBe('trash')
	})

	it('In la-user-panel-form la-panel-form-item with type = "sessions" and itemType="link" and linkHref="sessions" should be', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'sessions')

		expect(element).toBeTruthy()
		if (!element) return

		expect(element.attributes['itemType']).toBe('link')
		expect(element.attributes['linkHref']).toBe('sessions')
	})

	it('In la-user-panel-form la-panel-form-item with type = "exit" should be', async () => {
		const { fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'profile')
		expect(element).toBeTruthy()
	})

	it('In la-user-panel-form itemClick Output should not called on click on la-panel-form-item with type = "profile"', async () => {
		const { component, fixture } = await initializeComponent()

		let itemClickCalled = false

		component.itemClick.pipe(first()).subscribe(() => (itemClickCalled = true))

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'profile')
		expect(element).toBeTruthy()

		if (!element) return

		element.triggerEventHandler('itemClick')

		expect(itemClickCalled).toBe(false)
	})

	it('In la-user-panel-form itemClick Output should called on click on la-panel-form-item with type = "trash"', async () => {
		const { component, fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'trash')
		expect(element).toBeTruthy()
		if (!element) return

		let itemClickCalled = false
		component.itemClick.pipe(first()).subscribe(() => (itemClickCalled = true))

		element.triggerEventHandler('itemClick')

		expect(itemClickCalled).toBe(true)
	})

	it('In la-user-panel-form itemClick Output should called on click on la-panel-form-item with type = "sessions"', async () => {
		const { component, fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'sessions')
		expect(element).toBeTruthy()
		if (!element) return

		let itemClickCalled = false
		component.itemClick.pipe(first()).subscribe(() => (itemClickCalled = true))

		element.triggerEventHandler('itemClick')

		expect(itemClickCalled).toBe(true)
	})

	it('In la-user-panel-form itemClick Output should called with "exit" param on click on la-panel-form-item with type = "exit"', async () => {
		const { component, fixture } = await initializeComponent()

		const form = fixture.debugElement.query(By.css('la-panel-form'))
		const elements = form.queryAll(By.css('la-panel-form-item'))

		const element = elements.find(el => el.attributes['type'] === 'exit')
		expect(element).toBeTruthy()
		if (!element) return

		let itemClickCalled = false
		let itemClickCalledValue: string | null = null

		component.itemClick.pipe(first()).subscribe(value => {
			itemClickCalled = true
			itemClickCalledValue = value
		})

		element.triggerEventHandler('itemClick')

		expect(itemClickCalled).toBe(true)
		expect(itemClickCalledValue).toBe('exit')
	})
})
