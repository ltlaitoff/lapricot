import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { User } from '@lapricot/shared/types'
import { PanelFormDividerComponent } from '../../panel-form-divider/panel-form-divider.component'
import { PanelFormItemComponent } from '../../panel-form-item/panel-form-item.component'
import { PanelFormComponent } from '../../panel-form/panel-form.component'

@Component({
	selector: 'la-user-panel-form',
	standalone: true,
	imports: [
		CommonModule,
		PanelFormComponent,
		PanelFormItemComponent,
		PanelFormDividerComponent
	],
	templateUrl: './user-panel-form.component.html'
})
export class UserPanelFormComponent {
	@Input() userInfo: User | null = null

	@Output() itemClick = new EventEmitter<'exit' | null>()

	onClickOutput(type: 'exit' | null = null) {
		this.itemClick.emit(type)
	}
}
