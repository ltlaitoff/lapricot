import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { User } from '@lapricot/shared/types'
import { first } from 'rxjs'

import { UserPanelOpenButtonComponent } from './user-panel-open-button.component'

interface ComponentInputs {
	userInfo: User | null
	isOpened: boolean
}

const defaultInputs: ComponentInputs = {
	userInfo: {
		_id: 'id',
		email: 'email',
		family_name: 'family_name',
		given_name: 'given_name',
		name: 'name',
		picture: 'picture',
		authorized: true,
		sessionId: 'sessionId'
	},
	isOpened: false
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [UserPanelOpenButtonComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(UserPanelOpenButtonComponent)
	const component = fixture.componentInstance

	component.userInfo =
		inputs.userInfo !== undefined ? inputs.userInfo : defaultInputs.userInfo
	component.isOpened =
		inputs.isOpened !== undefined ? inputs.isOpened : defaultInputs.isOpened

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('UserPanelOpenButtonComponent', () => {
	it('Component la-user-panel-open-button should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-user-panel-open-button button should be on userInfo Input is not null', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		expect(button).toBeTruthy()
	})

	it('In la-user-panel-open-button button should not be on userInfo Input is null', async () => {
		const { fixture } = await initializeComponent({ userInfo: null })

		const button = fixture.debugElement.query(By.css('button'))

		expect(button).not.toBeTruthy()
	})

	it('In la-user-panel-open-button userInfo.name should be as text in button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))

		expect(button.nativeElement.textContent).toContain('name')
	})

	it('In la-user-panel-open-button img should be in button with src = userInfo.picture', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(By.css('button'))
		const image = button.query(By.css('img'))

		expect(image).toBeTruthy()
		expect(image.attributes['src']).toBe('picture')
	})

	it('In la-user-panel-open-button buttonClick Output should called on click on button', async () => {
		const { component, fixture } = await initializeComponent()

		let buttonClickCalled = false

		component.buttonClick
			.pipe(first())
			.subscribe(() => (buttonClickCalled = true))

		const button = fixture.debugElement.query(By.css('button'))

		button.triggerEventHandler('click')

		expect(buttonClickCalled).toBe(true)
	})
})
