import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { User } from '@lapricot/shared/types'

@Component({
	selector: 'la-user-panel-open-button',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './user-panel-open-button.component.html'
})
export class UserPanelOpenButtonComponent {
	@Input() userInfo: User | null = null
	@Input() isOpened = false

	@Output() buttonClick = new EventEmitter()

	onButtonClick() {
		this.buttonClick.emit()
	}
}
