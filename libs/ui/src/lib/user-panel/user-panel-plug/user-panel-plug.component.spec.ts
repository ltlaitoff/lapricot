import { TestBed } from '@angular/core/testing'
import { UserPanelPlugComponent } from './user-panel-plug.component'

async function initializeComponent() {
	await TestBed.configureTestingModule({
		declarations: [UserPanelPlugComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(UserPanelPlugComponent)
	const component = fixture.componentInstance

	fixture.detectChanges()

	return {
		fixture,

		component,
		detectChanges: fixture.detectChanges
	}
}

describe('UserPanelPlugComponent', () => {
	it('Component la-user-panel-plug should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-user-panel-plug should be text = "Not authorized"', async () => {
		const { fixture } = await initializeComponent()

		const text = fixture.nativeElement.textContent

		expect(text).toContain('Not authorized')
	})
})
