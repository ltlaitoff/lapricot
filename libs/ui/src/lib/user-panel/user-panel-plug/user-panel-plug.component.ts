import { Component } from '@angular/core'

@Component({
	selector: 'la-user-panel-plug',
	templateUrl: './user-panel-plug.component.html',
	standalone: true
})
export class UserPanelPlugComponent {}
