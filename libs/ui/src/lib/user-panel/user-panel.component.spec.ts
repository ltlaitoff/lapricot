import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { AuthGuardService } from '@lapricot/client-lib/auth'
import { User } from '@lapricot/shared/types'
import { first } from 'rxjs'

import { UserPanelComponent } from './user-panel.component'

interface ComponentInputs {
	userInfo: User | null
}

const defaultInputs: ComponentInputs = {
	userInfo: {
		_id: 'id',
		email: 'email',
		family_name: 'family_name',
		given_name: 'given_name',
		name: 'name',
		picture: 'picture',
		authorized: true,
		sessionId: 'sessionId'
	}
}

const authGuardServiceStubUnauthorize = jest.fn()

const authGuardServiceStub: Partial<AuthGuardService> = {
	unauthorize: authGuardServiceStubUnauthorize
}

async function initializeComponent(inputs: Partial<ComponentInputs> = {}) {
	await TestBed.configureTestingModule({
		declarations: [UserPanelComponent],
		providers: [{ provide: AuthGuardService, useValue: authGuardServiceStub }],
		schemas: [NO_ERRORS_SCHEMA]
	}).compileComponents()

	const fixture = TestBed.createComponent(UserPanelComponent)
	const component = fixture.componentInstance

	component.userInfo =
		inputs.userInfo !== undefined ? inputs.userInfo : defaultInputs.userInfo

	fixture.detectChanges()

	return {
		fixture,
		component,
		detectChanges: fixture.detectChanges
	}
}

describe('UserPanelComponent', () => {
	it('Component la-user-panel should be', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it('In la-user-panel la-user-panel-plug should be on Input userInfo = null', async () => {
		const { fixture } = await initializeComponent({ userInfo: null })

		const element = fixture.debugElement.query(By.css('la-user-panel-plug'))

		expect(element).toBeTruthy()
	})

	it('In la-user-panel la-user-panel-open-button should be on Input userInfo != null', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		expect(element).toBeTruthy()
	})

	it('In la-user-panel la-user-panel-open-button should has userInfo and isOpened params', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		expect(element.properties['userInfo']).toEqual(defaultInputs.userInfo)
		expect(element.properties['isOpened']).toBe(false)
	})

	it('In la-user-panel isOpened param on la-user-panel-open-button should be true after call buttonClick event on la-user-panel-open-button', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		expect(element.properties['isOpened']).toBe(false)

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		expect(element.properties['isOpened']).toBe(true)
	})

	it('In la-user-panel isOpened param on la-user-panel-open-button should be false after 2 calls buttonClick event on la-user-panel-open-button', async () => {
		const { fixture } = await initializeComponent()

		const element = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		expect(element.properties['isOpened']).toBe(false)

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		expect(element.properties['isOpened']).toBe(true)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		expect(element.properties['isOpened']).toBe(false)
	})

	it('In la-user-panel la-user-panel-form should be in document after call buttonClick event on la-user-panel-open-button', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		const form = fixture.debugElement.query(By.css('la-user-panel-form'))
		expect(form).toBeTruthy()
	})

	it('In la-user-panel authGuard.unauthorize should call on call (itemClick) event on la-user-panel-form with "exit" param', async () => {
		const { fixture } = await initializeComponent()

		authGuardServiceStubUnauthorize.mockClear()

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		const form = fixture.debugElement.query(By.css('la-user-panel-form'))
		expect(form).toBeTruthy()

		form.triggerEventHandler('itemClick', 'exit')
		fixture.detectChanges()

		expect(authGuardServiceStubUnauthorize).toBeCalled()
	})

	it('In la-user-panel la-user-panel-form should close after call (itemClick) event on la-user-panel-form', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		const form = fixture.debugElement.query(By.css('la-user-panel-form'))
		expect(form).toBeTruthy()

		form.triggerEventHandler('itemClick')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()
	})

	it('In la-user-panel la-user-panel-form should not changed after call (formClose) event on [data-testid="user-panel-main"] with opened form and Input userInfo = null', async () => {
		const { fixture } = await initializeComponent({ userInfo: null })

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()

		const a = fixture.debugElement.query(
			By.css('[data-testid="user-panel-main"]')
		)
		a.triggerEventHandler('formClose')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()
	})

	it('In la-user-panel la-user-panel-form should not changed after call (formClose) event on [data-testid="user-panel-main"] with closed form', async () => {
		const { fixture } = await initializeComponent()

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()

		const a = fixture.debugElement.query(
			By.css('[data-testid="user-panel-main"]')
		)
		a.triggerEventHandler('formClose')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()
	})

	it('In la-user-panel la-user-panel-form should close after call (formClose) event on [data-testid="user-panel-main"] with opened form', async () => {
		const { fixture } = await initializeComponent()

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		const form = fixture.debugElement.query(By.css('la-user-panel-form'))
		expect(form).toBeTruthy()

		const a = fixture.debugElement.query(
			By.css('[data-testid="user-panel-main"]')
		)
		a.triggerEventHandler('formClose')
		fixture.detectChanges()

		expect(
			fixture.debugElement.query(By.css('la-user-panel-form'))
		).not.toBeTruthy()
	})

	it('In la-user-panel Output closeBurgerMenu should called after call (formClose) event on [data-testid="user-panel-main"]', async () => {
		const { component, fixture } = await initializeComponent()

		let closeBurgerMenuCalled = false

		component.closeBurgerMenu
			.pipe(first())
			.subscribe(() => (closeBurgerMenuCalled = true))

		const button = fixture.debugElement.query(
			By.css('la-user-panel-open-button')
		)

		button.triggerEventHandler('buttonClick')
		fixture.detectChanges()

		const a = fixture.debugElement.query(
			By.css('[data-testid="user-panel-main"]')
		)
		a.triggerEventHandler('formClose')
		fixture.detectChanges()

		expect(closeBurgerMenuCalled).toBe(true)
	})
})
