import { Component, EventEmitter, Input, Output } from '@angular/core'
import { User } from '@lapricot/shared/types'

import { AuthGuardService } from '@lapricot/client-lib/auth'
import { UserPanelPlugComponent } from './user-panel-plug/user-panel-plug.component'
import { UserPanelFormComponent } from './user-panel-form/user-panel-form.component'
import { UserPanelOpenButtonComponent } from './user-panel-open-button/user-panel-open-button.component'
import { FormCloseDirective } from '@lapricot/client-lib/directives'
import { CommonModule } from '@angular/common'

@Component({
	selector: 'la-user-panel',
	standalone: true,
	imports: [
		CommonModule,
		UserPanelPlugComponent,
		UserPanelFormComponent,
		UserPanelOpenButtonComponent,
		FormCloseDirective
	],
	templateUrl: './user-panel.component.html'
})
export class UserPanelComponent {
	constructor(private authGuard: AuthGuardService) {}

	@Input() userInfo: User | null = null
	@Output() closeBurgerMenu = new EventEmitter()

	isOpened = false

	private toggleDropdownOpened() {
		this.isOpened = !this.isOpened
	}

	private closeDropdown() {
		this.isOpened = false
		this.closeBurgerMenu.emit()
	}

	onBadgeClick() {
		this.toggleDropdownOpened()
	}

	onClick(value: 'exit' | null = null) {
		if (value === 'exit') {
			this.authGuard.unauthorize()
		}

		this.closeDropdown()
	}

	closeWithChecks() {
		if (this.userInfo === null) return
		if (this.isOpened === false) return

		this.closeDropdown()
	}
}
