import { TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { VersionComponent } from './version.component'
import { ClientEnvironment } from '@lapricot/shared/types'

const mockVersionDefault = 'MOCKED_VERSION'

jest.mock('./version.config', () => ({
	get VERSION_DEFAULT() {
		return mockVersionDefault
	}
}))

type ComponentInputs = {
	version?: string
	type?: ClientEnvironment['type']
	showType?: boolean
}

async function initializeComponent(inputs: ComponentInputs = {}) {
	await TestBed.configureTestingModule({
		declarations: [VersionComponent]
	}).compileComponents()

	const fixture = TestBed.createComponent(VersionComponent)
	const component = fixture.componentInstance

	if (inputs.version) component.version = inputs.version
	if (inputs.type) component.type = inputs.type
	if (inputs.showType !== undefined) {
		component.showType = inputs.showType
	}

	fixture.detectChanges()

	return { fixture, component }
}

describe('VersionComponent', () => {
	it('should create', async () => {
		const { component } = await initializeComponent()

		expect(component).toBeTruthy()
	})

	it.each`
		version    | versionResult
		${'1.0.0'} | ${'v1.0.0'}
		${'2.0.0'} | ${'v2.0.0'}
		${'test'}  | ${'vtest'}
		${'ABC'}   | ${'vABC'}
		${''}      | ${`v${mockVersionDefault}`}
	`(
		'On version = "$version", version on page should be "$versionResult"',
		async ({ version, versionResult }) => {
			const { fixture } = await initializeComponent({
				version: version
			})

			const element = fixture.debugElement.query(
				By.css('[data-testid="version"]')
			)

			expect(element.nativeElement.textContent).toBe(versionResult)
		}
	)

	it.each`
		type            | showType | resultType
		${'dev'}        | ${true}  | ${'dev'}
		${'dev'}        | ${false} | ${undefined}
		${'next'}       | ${true}  | ${'next'}
		${'next'}       | ${false} | ${undefined}
		${'production'} | ${true}  | ${undefined}
		${'production'} | ${false} | ${undefined}
		${''}           | ${true}  | ${'dev'}
		${''}           | ${false} | ${undefined}
	`(
		'On type = "$type" and showType = "$showType", type on page should be "$resultType"',
		async ({ type, showType, resultType }) => {
			const { fixture } = await initializeComponent({
				type: type,
				showType: showType
			})

			const element = fixture.debugElement.query(By.css('[data-testid="type"]'))

			if (showType === false && resultType === undefined) {
				expect(element).toBe(null)
				return
			}

			expect(element?.nativeElement.textContent.trim()).toBe(resultType)
		}
	)
})
