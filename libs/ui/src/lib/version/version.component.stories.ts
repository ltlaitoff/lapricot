import { Meta } from '@storybook/angular'
import { VersionComponent } from './version.component'

export default {
	title: 'VersionComponent',
	component: VersionComponent
} as Meta<VersionComponent>

export const Primary = {
	render: (args: VersionComponent) => ({
		props: args
	}),
	args: {}
}
