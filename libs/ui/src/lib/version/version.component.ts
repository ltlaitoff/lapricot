import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { ClientEnvironment } from '@lapricot/shared/types'
import { VERSION_DEFAULT } from './version.config'

@Component({
	selector: 'la-version',
	templateUrl: './version.component.html',
	imports: [CommonModule],
	standalone: true
})
export class VersionComponent {
	@Input({ required: true }) version = VERSION_DEFAULT
	@Input() type: ClientEnvironment['type'] = 'dev'
	@Input() showType = true
}
